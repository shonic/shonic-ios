//
//  EmptyProfileViewController.swift
//  Shonic
//
//  Created by MEKARI on 08/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class EmptyProfileViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func navigationLogin(_ sender: Any) {
        self.navigationController?.pushViewController(LoginViewController(), animated: true)
    }
    @IBAction func registerAction(_ sender: Any) {
        self.navigationController?.pushViewController(RegisterViewController(), animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
