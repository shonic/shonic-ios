//
//  HomeInteractor.swift
//  Shonic
//
//  Created by Gramedia on 22/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

protocol HomeInteractorInput{
    func getAllLastestProduct()
    func getAllBrandProduct()
}

protocol HomeInteractorOutput{
    func getAllLatestProduct(data:GetAllLastestProductResponse)
    func getAllBrandProduct(data:GetAllBrandResponse)
    func getMessage(error:String)
}

class HomeInteractor{
    var worker:HomeWorker
    var presenter:HomeInteractorOutput?
    init(presenter:HomeInteractorOutput,worker:HomeWorker){
        self.presenter = presenter
        self.worker = worker
    }
}

extension HomeInteractor:HomeInteractorInput{
    func getAllLastestProduct() {
        self.worker.getAllByBrand{callback in
            switch callback{
            case .success(let data):
                self.presenter?.getAllBrandProduct(data: data)
            case .failure( let error):
                self.presenter?.getMessage(error: error.localizedDescription)
            }
        }
    }
    
    func getAllBrandProduct() {
        self.worker.getAllByBrand{callback in
            switch callback{
            case .success(let data):
                self.presenter?.getAllBrandProduct(data: data)
            case .failure(let error):
                self.presenter?.getMessage(error: error.localizedDescription)
            }
        }
    }
    
    
}
