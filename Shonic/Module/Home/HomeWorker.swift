//
//  HomeWorker.swift
//  Shonic
//
//  Created by Gramedia on 22/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation
import Alamofire

protocol HomeSceneLogic{
    func getAllLastestProduct(callback:@escaping(Result<GetAllLastestProductResponse,AFError>)->Void)
    func getAllProductSales()
    func getAllByBrand(callback:@escaping(Result<GetAllBrandResponse,AFError>)->Void)
    
}

class HomeWorker{
    var service = ApiClient.shared
    var baseUrl = "http://shonic-test.herokuapp.com"
}

extension HomeWorker:HomeSceneLogic{
    func getAllByBrand(callback: @escaping (Result<GetAllBrandResponse, AFError>) -> Void) {
        service.requestApi(urlString: "\(baseUrl)/api/v1/brand/getAllBrand", method: .get, header: nil){(response:Result<GetAllBrandResponse, AFError>) in
            switch response {
            case .success(let success):
                callback(.success(success))
            case .failure(let failure):
                callback(.failure(failure))
            }
        }
    }
    
    func getAllLastestProduct(callback:@escaping(Result<GetAllLastestProductResponse,AFError>)->Void) {
        service.requestApi(urlString: "\(baseUrl)/api/v1/product/getAllLatestProduct", method: .get, header: nil){(response:Result<GetAllLastestProductResponse,AFError>) in
            switch response {
            case .success(let success):
                callback(.success(success))
            case .failure(let failure):
                callback(.failure(failure))
            }
        }
    }
    
    func getAllProductSales() {
        
    }
    
    
}
