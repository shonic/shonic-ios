//
//  HomePresenter.swift
//  Shonic
//
//  Created by Gramedia on 22/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

protocol HomePresenterOutput{
    func getAllLastestProduct(data:GetAllLastestProductResponse)
    func getAllBrandProduct(data:GetAllBrandResponse)
    func getMessage(error:String)
}

class HomePresenter:HomeInteractorOutput{
    func getMessage(error: String) {
        self.getMessage(error: error)
    }
    
    func getAllLatestProduct(data: GetAllLastestProductResponse) {
        self.getAllLatestProduct(data: data)
    }
    
    func getAllBrandProduct(data: GetAllBrandResponse) {
        self.getAllBrandProduct(data: data)
    }
    var viewController:HomePresenterOutput?
    init(viewController:HomePresenterOutput) {
        self.viewController = viewController
    }
    
}
