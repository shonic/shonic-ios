//
//  HomeHeaderView.swift
//  Shonic
//
//  Created by Dzaki Izza on 20/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class HomeHeaderView: UIView {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var seeAllButton: UIButton!
    @IBOutlet weak var countdownView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
      }

      required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadNib()
      }

      func loadNib() {
        Bundle.main.loadNibNamed("HomeHeaderView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
      }
}
