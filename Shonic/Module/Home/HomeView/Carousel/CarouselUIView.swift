//
//  CarouselUIView.swift
//  Shonic
//
//  Created by MEKARI on 14/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit


struct ImageData{
    var image:String
}
class CarouselUIView: UIView {
    
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
   
    private lazy var carouselCollectionView: UICollectionView = {
        let collection = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collection.showsHorizontalScrollIndicator = false
        collection.isPagingEnabled = true
        collection.dataSource = self
        collection.delegate = self
        collection.register(CarouselCollectionViewCell.self, forCellWithReuseIdentifier: CarouselCollectionViewCell.cellId)
        collection.backgroundColor = .clear
        return collection
     }()

      private lazy var pageControl: UIPageControl = {
         let pageControl = UIPageControl()
         pageControl.pageIndicatorTintColor = .gray
         pageControl.currentPageIndicatorTintColor = .white
         return pageControl
      }()
    
    
    
      // MARK: - Properties
      private var carouselData = [ImageData]()
      
    private var currentPage = 0 {
        didSet {
            pageControl.currentPage = currentPage
        }
    }

}

extension CarouselUIView:UICollectionViewDataSource,UICollectionViewDelegate{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return carouselData.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CarouselCollectionViewCell.cellId, for: indexPath) as? CarouselCollectionViewCell else { return UICollectionViewCell() }
        
        let image = UIImage(named: carouselData[indexPath.row].image)!
        print(image)
        cell.configure(image: image)
        return cell
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
            currentPage = getCurrentPage()
        }
        
        func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
            currentPage = getCurrentPage()
        }
        
        func scrollViewDidScroll(_ scrollView: UIScrollView) {
            currentPage = getCurrentPage()
        }
    
}

extension CarouselUIView {
    
    public func configureView(with data: [ImageData]) {
        let cellPadding = (frame.width - 300) / 2
                let carouselLayout = UICollectionViewFlowLayout()
                carouselLayout.scrollDirection = .horizontal
                carouselLayout.itemSize = .init(width: 300, height: 400)
                carouselLayout.sectionInset = .init(top: 0, left: cellPadding, bottom: 0, right: cellPadding)
                carouselLayout.minimumLineSpacing = frame.width - 300
                carouselCollectionView.collectionViewLayout = carouselLayout
                
                carouselData = data
                carouselCollectionView.reloadData()
    }
}
private extension CarouselUIView {
    func getCurrentPage() -> Int {
            
            let visibleRect = CGRect(origin: carouselCollectionView.contentOffset, size: carouselCollectionView.bounds.size)
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            if let visibleIndexPath = carouselCollectionView.indexPathForItem(at: visiblePoint) {
                return visibleIndexPath.row
            }
            
        return currentPage
        
    }
}
