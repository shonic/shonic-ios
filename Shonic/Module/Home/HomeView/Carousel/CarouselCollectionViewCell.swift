//
//  CollectionViewCell.swift
//  Shonic
//
//  Created by MEKARI on 14/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class CarouselCollectionViewCell: UICollectionViewCell {
    private var imageView = UIImageView()
    static let cellId = "CarouselCell"
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
}

private extension CarouselCollectionViewCell{
    func setup(){
        backgroundColor = .clear
        addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        imageView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        imageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 300).isActive = true
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 24
    }
}

extension CarouselCollectionViewCell{
    public func configure(image:UIImage){
        imageView.image = UIImage(named: "Carousel")
    }
}
