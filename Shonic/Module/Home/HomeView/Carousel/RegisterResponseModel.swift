//
//  RegisterResponseModel.swift
//  Shonic
//
//  Created by MEKARI on 14/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation


struct RegisterResponseModel:Decodable{
    var token:String?
    var status:Int
    var message:String
    var error:String?
    var path:String?
    var data: ResponseData?
}

struct ResponseData: Decodable {
    var fullName: String?
    var id: String?
    var email : String?
}

struct RegisterParameter:Encodable{
    var email:String
    var password:String
    var fullName: String
    
    init(email:String,password:String, fullName: String) {
        self.email = email
        self.password = password
        self.fullName = fullName
    }
}

struct RegisterFailedResponseModel:Decodable{
    var message:String
    var status:Int
    var timeStamp:String
    var error:String
}
