//
//  BrandCollectionViewCell.swift
//  Shonic
//
//  Created by Dzaki Izza on 21/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class BrandCollectionViewCell: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    class func nib ()-> UINib {UINib(nibName: "BrandCollectionViewCell", bundle: nil)}

}
