//
//  CarousellTableViewCell.swift
//  Shonic
//
//  Created by MEKARI on 18/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class CarousellTableViewCell: UITableViewCell {

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var CollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupCollectionView()
        // Initialization code
    }
    func setupCollectionView(){
        self.CollectionView.delegate = self
        self.CollectionView.dataSource = self
        self.CollectionView.register(CarousellCollectionViewCell.nib(), forCellWithReuseIdentifier: "CarousellCollectionCell")
        self.pageControl.currentPageIndicatorTintColor = UIColor.blue
    }
    
    private var currentPage:Int = 0{
        didSet{
            pageControl.currentPage = currentPage
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    class func nib()->UINib{
        UINib(nibName: "CarousellTableViewCell", bundle:nil)
    }
    
}

extension CarousellTableViewCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CarousellCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CarousellCollectionCell", for: indexPath) as! CarousellCollectionViewCell
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      let width = (collectionView.frame.size.width)
        let height = (183.0 / 375) * width
      return CGSize(width: width, height: height)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        currentPage = getCurrentPage()
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        currentPage = getCurrentPage()
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        currentPage = getCurrentPage()
    }
    func getCurrentPage() -> Int {
            
        let visibleRect = CGRect(origin: CollectionView.contentOffset, size: CollectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        if let visibleIndexPath = CollectionView.indexPathForItem(at: visiblePoint) {
            return visibleIndexPath.row
        }
            
        return currentPage
    }
    
    
}
