//
//  CarousellCollectionViewCell.swift
//  Shonic
//
//  Created by MEKARI on 15/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit
import SDWebImage

class CarousellCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    var imageUrl:String? {
        didSet {
            if let image = imageUrl{
                setupItem(image: image)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setupItem(image:String){
        let url = URL(string: image)
        var image = UIImage(named: "Banner")
        imageView = UIImageView(image: image)
        //imageView.sd_setImage(with: url,placeholderImage:UIImage(systemName: "gearshape.fill"))
    }
    class func nib()->UINib{
        UINib(nibName: "CarousellCollectionViewCell", bundle:nil)
    }

}
