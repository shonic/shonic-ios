//
//  HomeViewController.swift
//  Shonic
//
//  Created by MEKARI on 03/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var homeTableView: UITableView!
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupNavigationBar()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
    }
    
    private func setupNavigationBar(){
        
    }

    private func setupTableView(){
        homeTableView.delegate = self
        homeTableView.dataSource = self
        homeTableView.register(CarousellTableViewCell.nib(), forCellReuseIdentifier: "CarousellCell")
        homeTableView.register(CategoriesTableViewCell.nib(), forCellReuseIdentifier: "CategoriesTableViewCell")
        homeTableView.register(RecommendationTableViewCell.nib(), forCellReuseIdentifier: "RecommendationCell")
        homeTableView.register(BrandTableViewCell.nib(), forCellReuseIdentifier: "BrandCell")
        homeTableView.register(DiscountTableViewCell.nib(), forCellReuseIdentifier: "DiscountCell")

        self.homeTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension HomeViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section{
        case 1:
            let view = HomeHeaderView()
            view.titleLabel.text = "KATEGORI"
            return view
        case 2:
            let view = HomeHeaderView()
            view.countdownView.isHidden = false
            view.titleLabel.text = "DISKON SPESIAL"
            return view
        case 3:
            let view = HomeHeaderView()
            view.titleLabel.text = "BRAND PILIHAN"
            return view
        case 4:
            let view = HomeHeaderView()
            view.titleLabel.text = "REKOMENDASI PRODUK"
            view.seeAllButton.isHidden = false 
            return view
        default:
            return nil
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        5
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section{
        case 0: return 0.000001
        case 1: return 45
        case 2: return 45
        case 3: return 45
        case 4: return 45
        default: return 0.001
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section{
        case 0 : return 1
        case 1 : return 1
        case 2 : return 1
        case 3 : return 1
        case 4 : return 1
        default : return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section{
            case 0 :
            let cell:CarousellTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CarousellCell", for: indexPath) as! CarousellTableViewCell
            return cell
            case 1 :
            let cell:CategoriesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CategoriesTableViewCell") as! CategoriesTableViewCell
            return cell
            case 2:
            let cell:DiscountTableViewCell = tableView.dequeueReusableCell(withIdentifier: "DiscountCell") as! DiscountTableViewCell
            return cell
            case 3:
            let cell:BrandTableViewCell = tableView.dequeueReusableCell(withIdentifier: "BrandCell") as! BrandTableViewCell
            return cell
            case 4:
            let cell:RecommendationTableViewCell = tableView.dequeueReusableCell(withIdentifier: "RecommendationCell") as! RecommendationTableViewCell
            return cell
            
        default:
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section{
        case 0:
            return (187 / 375) * tableView.frame.size.width
        case 1 :
            return (94 / 375) * tableView.frame.size.width
        case 2 :
            return ((295 / 375) * tableView.frame.size.width) + 10
        case 3 :
            return ((123 / 375) * tableView.frame.size.width) + 10

        default : return tableView.frame.height - 40
        }

    }
    
    
    
}
