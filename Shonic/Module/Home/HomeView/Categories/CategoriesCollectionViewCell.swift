//
//  CategoriesCollectionViewCell.swift
//  Shonic
//
//  Created by MEKARI on 18/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class CategoriesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelView: UILabel!
    
    var category:Category?{
        didSet{
            if let category = category{
                setupCategory(category: category)
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupCategory(category:Category){
        imageView.image = UIImage(named: category.image)
        labelView.text = category.title
    }
    class func nib ()-> UINib {UINib(nibName: "CategoriesCollectionViewCell", bundle: nil)}

}
