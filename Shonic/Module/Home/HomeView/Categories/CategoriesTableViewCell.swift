//
//  CategoriesTableViewCell.swift
//  Shonic
//
//  Created by MEKARI on 18/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class CategoriesTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    var dataArray:[Category] = [
        Category(title: "Laptop", image: "category1"),
        Category(title: "PC/Komputer", image: "category2"),
        Category(title: "Smartphone", image: "category3"),
        Category(title: "Audio/Speaker", image: "category4"),
        Category(title: "Komponen", image: "category5")

    ]
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupCollectionView()
        // Initialization code
    }
    
    func setupCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(CategoriesCollectionViewCell.nib(), forCellWithReuseIdentifier: "CategoryCollectionViewCell")
    }
    
    class func nib() -> UINib{ UINib(nibName: "CategoriesTableViewCell", bundle: nil)}

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension CategoriesTableViewCell:UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = 64.0
        let height = 94.0
        return CGSize(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:CategoriesCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionViewCell", for: indexPath) as! CategoriesCollectionViewCell
        cell.category = dataArray[indexPath.row]
        return cell
    }
}
