//
//  Category.swift
//  Shonic
//
//  Created by MEKARI on 18/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

struct Category{
    var title:String
    var image:String
    init(title:String,image:String) {
        self.title = title
        self.image = image
    }
}
