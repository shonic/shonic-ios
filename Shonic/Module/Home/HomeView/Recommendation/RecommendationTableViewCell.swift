//
//  RecommendationTableViewCell.swift
//  Shonic
//
//  Created by Dzaki Izza on 20/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class RecommendationTableViewCell: UITableViewCell {

    @IBOutlet weak var heightCollectionViewConst: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.collectionView.register(RecommendationCollectionViewCell.nib(), forCellWithReuseIdentifier: "RecomendationCollectionCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
    class func nib() -> UINib{ UINib(nibName: "RecommendationTableViewCell", bundle: nil)}


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension RecommendationTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: RecommendationCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecomendationCollectionCell", for: indexPath) as! RecommendationCollectionViewCell
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let width = (UIScreen.main.bounds.size.width - 16)  / 2
        let height = (307.0 / 181) * width
      return CGSize(width: width, height: height)
    }

    
}
