//
//  RecommendationCollectionViewCell.swift
//  Shonic
//
//  Created by Dzaki Izza on 20/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit
import SDWebImage
class RecommendationCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var recommendationCardView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameProduct: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    var product:Product?{
        didSet{
            if let product = product{
                self.configureUI(image: product.image ?? "", title: product.name ?? "", price: product.price ?? 0)
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        recommendationCardView.shadow(alpha: 0.5, opacity: 0.3, shadowOffset: CGSize(width: 0.5, height: 0.5), shadowRadius: 5.0, cornerRadius: 5.0)
    }
    
    func configureUI(image:String,title:String,price:Int){
        var url = URL(string: image)
        imageView.sd_setImage(with: url, placeholderImage: UIImage(systemName: "gearshape.fill"))
        priceLabel.text = "Rp \(price)"
        nameProduct.text = title
    }
    
    class func nib ()-> UINib {UINib(nibName: "RecommendationCollectionViewCell", bundle: nil)}

}
