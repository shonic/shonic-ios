//
//  DiscountCollectionViewCell.swift
//  Shonic
//
//  Created by Dzaki Izza on 21/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class DiscountCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var card: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        card.shadow(alpha: 0.5, opacity: 0.3, shadowOffset: CGSize(width: 0.5, height: 0.5), shadowRadius: 5.0, cornerRadius: 5.0)
    }
    class func nib ()-> UINib {UINib(nibName: "DiscountCollectionViewCell", bundle: nil)}


}
