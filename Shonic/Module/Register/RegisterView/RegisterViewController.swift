//
//  RegisterViewController.swift
//  Shonic
//
//  Created by Dzaki Izza on 01/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit
import GoogleSignIn
import NotificationBannerSwift

class RegisterViewController: UIViewController {

    @IBOutlet weak var googleBtn: UIButton!
    @IBOutlet weak var policyTextView: UITextView!
    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var bottomRegisterConstraint: NSLayoutConstraint!
    @IBOutlet weak var errorMessage: UILabel!
    let signInConfig = GIDConfiguration(clientID: "801858909785-jjj9k9704l9g65ijdf9mut3fma1aevi9.apps.googleusercontent.com")
    var interactor:RegisterInteractor?
    
    init() {
        super.init(nibName: "RegisterViewController", bundle: nil)
        RegisterConfigurator.configure(viewcontroller: self)
        self.hidesBottomBarWhenPushed = true
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        googleBtnSetup()
        policySetup()
        textFieldSetup()
        registerBtnSetup()
        emailForm()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Daftar"
        
    }
    
    @IBAction func emailOnChange(_ sender: Any) {
        if let email = emailTextField.text
        {
            if let error = invalidEmail(email)
            {
                errorMessage.text = error
                errorMessage.isHidden = false
                emailTextField.layer.borderColor = UIColor.systemRed.cgColor

            }
            else
            {
                errorMessage.isHidden = true
                emailTextField.layer.borderColor = UIColor.systemGray5.cgColor
            }
        }
        checkForValidForm()
    }
    @IBAction func registerButtonTapped(_ sender: Any) {
        if emailTextField.text == "shonic@gmail.com" {
            showAlert()
        }
        self.interactor?.sendOTPRegister(email: emailTextField.text!)
    }
    @IBAction func googleBtnTapped(_ sender: Any) {
        GIDSignIn.sharedInstance.signIn(with: signInConfig, presenting: self) { user, error in
          guard error == nil else { return }

        }
    }
    
    func showAlert(){
        let alert = UIAlertController(title: "Email sudah terdaftar", message: "Lanjut masuk menggunakan email shonic@gmail.com?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ubah Email", style: .cancel, handler: {action in
            print("Ubah Email...")
        }))
        alert.addAction(UIAlertAction(title: "Ya, Masuk", style: .default, handler: {action in
            let vc = VerificationEmailViewController()
            vc.email = self.emailTextField.text!
            self.navigationController?.pushViewController(vc, animated: true)
        }))
        
        present(alert, animated: true)
    }
    
    
    func googleBtnSetup(){
        googleBtn.configuration?.imagePadding = 8
        googleBtn.backgroundColor = .clear
        googleBtn.layer.cornerRadius = 8
        googleBtn.layer.borderWidth = 1
        googleBtn.layer.borderColor = UIColor.systemGray5.cgColor
    }
    
    func policySetup(){
        policyTextView.addHyperLinksToText(originalText: "Dengan mendaftar, saya menyetujui Syarat dan Ketentuan serta Kebijakan Privasi", hyperLinks: ["Syarat dan Ketentuan": "www.google.com", "Kebijakan Privasi": "www.google.com"])
    }
    
    func registerBtnSetup(){
        registerBtn.layer.cornerRadius = 8
        registerBtn.layer.masksToBounds = true
        registerBtn.tintColor = UIColor(named: "PrimaryFocus")
        registerBtn.isUserInteractionEnabled = false
    }
    
    func emailForm(){
        errorMessage.isHidden = true
        errorMessage.text = ""
        emailTextField.text = ""
    }
    
    func invalidEmail(_ value: String) -> String?
    {
        let reqularExpression = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let predicate = NSPredicate(format: "SELF MATCHES %@", reqularExpression)
        if !predicate.evaluate(with: value)
        {
            return "Format email tidak valid, contoh: shonic@gmail.com"
        }
        
        return nil
    }
    
    func checkForValidForm()
    {
        if errorMessage.isHidden
        {
            registerBtn.isUserInteractionEnabled = true
            registerBtn.tintColor = UIColor(named: "PrimaryBlue")
            
        }
        else
        {
            registerBtn.isUserInteractionEnabled = false
            registerBtn.tintColor = UIColor(named: "PrimaryFocus")
        }
    }
    
    func textFieldSetup(){
        emailTextField.addTarget(self, action: #selector(touchTextField), for: .editingDidBegin)
        emailTextField.layer.borderColor = UIColor.systemGray5.cgColor
        emailTextField.layer.borderWidth = 1
        emailTextField.layer.cornerRadius = 8
        emailTextField.layer.masksToBounds = true
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyBoard)))
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func touchTextField() {
        emailTextField.layer.borderColor = UIColor.systemBlue.cgColor
    }
    
    @objc private func hideKeyBoard(){
        self.view.endEditing(true)
    }
    
    @objc private func keyboardWillHide(){
        bottomRegisterConstraint.constant = 16
    }
    
    @objc private func keyboardWillShow(notification: NSNotification){
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as?
            NSValue {
            let keyboardHeight = keyboardFrame.cgRectValue.height
            let bottomSpace = self.view.frame.height - (registerBtn.frame.origin.y + registerBtn.frame.height)
            bottomRegisterConstraint.constant = keyboardHeight - bottomSpace + 36
        }
    }
}

extension RegisterViewController:RegisterScenePresenterOutput{
    func success(response: GenerateTokenReponseModel) {
        if response.status >= 400 {
            let banner = FloatingNotificationBanner(title:"Gagal pengiriman kode",subtitle:"Email sudah terdaftar, silahkan menggunakan akun lain",style: .danger)
            banner.show()
        }else{
            let data = self.emailTextField.text
            let vc = VerificationEmailViewController()
            vc.email = data!
            vc.status = "register"
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func failure(message: String) {
        let banner = FloatingNotificationBanner(title:"Gagal pengiriman kode",subtitle:"Silahkan check email dan koneksi",style: .danger)
        banner.show()
    }
    
    
}
