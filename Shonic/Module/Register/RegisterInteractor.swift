//
//  RegisterInteractor.swift
//  Shonic
//
//  Created by MEKARI on 15/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

protocol RegisterInteractorInput:AnyObject{
    func sendOTPRegister(email:String)
}

protocol RegisterInteractorOutput: AnyObject {
    func successSendOTPRegister(response:GenerateTokenReponseModel)
    func failed(message:String)
}

class RegisterInteractor{
    init(presenter:RegisterInteractorOutput , worker: RegisterAuthWorker){
        self.presenter = presenter
        self.worker = worker
    }
    var presenter:RegisterInteractorOutput?
    var worker:RegisterAuthWorker?
}
extension RegisterInteractor:RegisterInteractorInput{
    func sendOTPRegister(email: String) {
        self.worker?.sendRegisterOTP(email: email){callback in
            DispatchQueue.main.async {
                switch callback{
                    
                case .success(let data):
                    self.presenter?.successSendOTPRegister(response: data)
                case .failure(_):
                    self.presenter?.failed(message: "Please check your email")
                }
            }
        }
    }
}

