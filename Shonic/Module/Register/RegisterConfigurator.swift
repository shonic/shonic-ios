//
//  RegisterConfigurator.swift
//  Shonic
//
//  Created by MEKARI on 15/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

enum RegisterConfigurator{
    static func configure(viewcontroller:RegisterViewController){
        let presenter = RegisterPresenter(viewController: viewcontroller)
        let worker = RegisterAuthWorker()
        let interactor = RegisterInteractor(presenter: presenter, worker: worker)
        viewcontroller.interactor = interactor
    }
}
