//
//  RegisterPresenter.swift
//  Shonic
//
//  Created by MEKARI on 15/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

protocol RegisterScenePresenterOutput: Any{
    func success(response:GenerateTokenReponseModel)
    func failure(message:String)
}

class RegisterPresenter:RegisterInteractorOutput{
    func successSendOTPRegister(response:GenerateTokenReponseModel) {
        viewController?.success(response: response)
    }
    
    func failed(message: String) {
        viewController?.failure(message: message)
    }
    init(viewController: RegisterScenePresenterOutput){
        self.viewController = viewController
    }
    //weak var viewController = LoginPresenterOutput?
    var viewController:RegisterScenePresenterOutput?
}
