//
//  RegisterWorker.swift
//  Shonic
//
//  Created by MEKARI on 15/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation
import Combine
import Alamofire
import RxSwift

protocol RegisterAuthLogic{
    func sendRegisterOTP(email:String,completion:@escaping(Result<GenerateTokenReponseModel,AFError>)->Void)
}
final class RegisterAuthWorker{
    private let service:ApiClient = ApiClient.shared
    private var bag = Set<AnyCancellable>()
    
    
    enum VerificationSceneAuthWorkerError:Error{
        case failedSendOTP(String)
        case unauthorized
    }
    
}

extension RegisterAuthWorker:RegisterAuthLogic{
    func sendRegisterOTP(email: String, completion: @escaping (Result<GenerateTokenReponseModel, AFError>) -> Void) {
        service.requestApi(urlString: "http://shonic-test.herokuapp.com/api/v1/otp/send", method: .post, parameters: ["email":"\(email)"], header: nil){ (callback:Result<GenerateTokenReponseModel, AFError>) in
            switch callback{
            case .success(let result):
                completion(.success(result))
                break
                
            case .failure(let error):
                completion(.failure(error))
                break
            }
        }
    }
}
