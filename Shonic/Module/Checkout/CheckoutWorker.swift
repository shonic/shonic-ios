//
//  CheckoutWorker.swift
//  Shonic
//
//  Created by MEKARI on 17/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation
import Alamofire

protocol CheckoutSceneLogic{
    func postCheckoutCost(by body:CheckoutCostBody,completion:@escaping(Result<CheckoutResponse,AFError>)->Void)
    func getAllAccountBank(completion:@escaping(Result<GetAllAccountBankDataResponse,AFError>)->Void)
    func createCheckout(body:CheckoutCreateBody,completion:@escaping(Result<GetCheckoutDataResponse,AFError>)->Void)
    func getAllMyAddress(completion:@escaping(Result<GetMyAddress,AFError>)->Void)
}

class CheckoutWorker{
    var baseUrl = "http://shonic-test.herokuapp.com"
    let service:ApiClient = ApiClient.shared
    let userDefault = UserDefaults.standard
}

extension CheckoutWorker:CheckoutSceneLogic{
    func getAllMyAddress(completion: @escaping(Result<GetMyAddress,AFError>)->Void) {
        if let token = userDefault.string(forKey: "token"){
            let authorization = "Bearer \(token)"
            let headers:HTTPHeaders = [
                "Authorization":authorization
            ]
            service.requestApi(urlString: "\(baseUrl)/api/v1/address/myaddress", method: .get, header: headers){(callback:Result<GetMyAddress,AFError>) in
                switch callback{
                case .success(let data):
                    print(data)
                    completion(.success(data))
                case .failure(let message):
                    completion(.failure(message))
                }
            }
        }else{
            completion(.failure(AFError.explicitlyCancelled))
        }
    }
    
    func postCheckoutCost(by body: CheckoutCostBody, completion: @escaping (Result<CheckoutResponse, AFError>) -> Void) {
        if let token = userDefault.string(forKey: "token"){
            let authorization = "Bearer \(token)"
            let headers:HTTPHeaders = [
                "Authorization":authorization
            ]
            service.requestApi(urlString: "\(baseUrl)/api/v1/checkout/cost", method: .post, parameters: body, header: headers){(callback:Result<CheckoutResponse, AFError>) in
                switch callback{
                case .success(let data):
                    completion(.success(data))
                case .failure(let message):
                    completion(.failure(message))
                }
            }
        }else{
            completion(.failure(AFError.explicitlyCancelled))
        }
        
    }
    
    func getAllAccountBank(completion: @escaping (Result<GetAllAccountBankDataResponse, AFError>) -> Void) {
        if let token = userDefault.string(forKey: "token"){
            let authorization = "Bearer \(token)"
            let headers:HTTPHeaders = [
                "Authorization":authorization
            ]
            service.requestApi(urlString: "\(baseUrl)/api/v1/account/all", method: .get, header: headers){(callback:Result<GetAllAccountBankDataResponse, AFError>) in
                switch callback{
                case .success(let data):
                    completion(.success(data))
                case .failure(let message):
                    completion(.failure(message))
                }
            }
        }else{
            completion(.failure(AFError.explicitlyCancelled))
        }
        
        
    }
    
    func createCheckout(body: CheckoutCreateBody, completion: @escaping (Result<GetCheckoutDataResponse, AFError>) -> Void) {
        let authorization = "Bearer \(userDefault.string(forKey: "token")!)"
        let headers:HTTPHeaders = [
            "Authorization":authorization
        ]
        service.requestApi(urlString: "\(baseUrl)/api/v1/checkout/create", method: .post, parameters: body, header: headers){ (callback:Result<GetCheckoutDataResponse, AFError>) in
            switch callback{
            case .success(let data):
                completion(.success(data))
            case .failure(let message):
                completion(.failure(message))
            }
        }
    }
    
    
}
