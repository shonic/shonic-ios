//
//  AddressHeaderCollectionReusableView.swift
//  Shonic
//
//  Created by Gramedia on 04/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class AddressHeaderCollectionReusableView: UICollectionReusableView {

    @IBOutlet weak var titleHeader: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    var handler:(()->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func navigateAddAddress(_ sender: Any) {
    handler!()
    }
    
    class func nib ()-> UINib{
        UINib(nibName: "AddressHeaderCollectionReusableView", bundle: nil)
    }
    
}
