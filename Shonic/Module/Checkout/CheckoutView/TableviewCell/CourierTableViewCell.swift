//
//  CourierTableViewCell.swift
//  Shonic
//
//  Created by MEKARI on 17/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class CourierTableViewCell: UITableViewCell {

    @IBOutlet weak var serviceLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    class func nib()-> UINib{
        UINib(nibName: "CourierTableViewCell", bundle: nil)
    }
    
}
