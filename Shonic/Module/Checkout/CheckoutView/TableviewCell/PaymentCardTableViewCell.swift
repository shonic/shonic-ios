//
//  PaymentCardTableViewCell.swift
//  Shonic
//
//  Created by Gramedia on 18/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class PaymentCardTableViewCell: UITableViewCell {

    @IBOutlet weak var labelBank: UILabel!
    @IBOutlet weak var logoBank: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    class func nib()->UINib{
        UINib(nibName: "PaymentCardTableViewCell", bundle: nil)
    }
    
}
