//
//  CheckoutViewController.swift
//  Shonic
//
//  Created by Gramedia on 03/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit
import FloatingPanel
import NotificationBannerSwift

class CheckoutViewController: UIViewController, FloatingPanelControllerDelegate,FloatingPanelBehavior {

    @IBOutlet weak var collectionView: UICollectionView!
    var fpc:FloatingPanelController!
    var checkoutData:CheckoutResponse?
    var getAllPaymentBank:GetAllAccountBankDataResponse?
    var getMyAddress:GetMyAddress?
    var interactor:CheckoutInteractor?
    var bodyCheckout:CheckoutCostBody?
    var courierCheckout:CourierCheckout?
    var qty:Int = 0
    var id:String = ""
    var price:Int = 0
    var courier:Int = 0
    init() {
        super.init(nibName: "CheckoutViewController", bundle: nil)
        CheckoutConfigurator.configure(viewController: self)
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    override func viewWillAppear(_ animated: Bool) {
        if let body = bodyCheckout{
            self.interactor?.getAllCourier(body: body)
            self.interactor?.getAllAccountBank()
            self.interactor?.getMyAddress()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Checkout"
        self.navigationItem.backButtonTitle = "Kembali"
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(NoteProductCollectionViewCell.nib(), forCellWithReuseIdentifier: "NoteProductCollectionViewCell")
        collectionView.register(AddressCollectionViewCell.nib(), forCellWithReuseIdentifier: "AddressCollectionViewCell")
        collectionView.register(CourierServiceCollectionViewCell.nib(), forCellWithReuseIdentifier: "CourierServiceCollectionViewCell")
        collectionView.register(PaymentCollectionViewCell.nib(), forCellWithReuseIdentifier: "PaymentCollectionViewCell")
        collectionView.register(DetailShoppingCollectionViewCell.nib(), forCellWithReuseIdentifier: "DetailShoppingCollectionViewCell")
        collectionView.register(LabelCollectionReusableView.nib(), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "LabelCollectionReusableView")
        collectionView.register(AddressHeaderCollectionReusableView.nib(), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "AddressHeaderCollectionReusableView")
        fpc = FloatingPanelController(delegate: self)
        fpc.layout = HidePickerCourier()
        fpc.contentMode = .static
        let checkout = CheckoutPriceViewController()
        checkout.price = self.price + self.courier
        checkout.handler = {
            if self.courierCheckout?.code ==  nil, self.courierCheckout?.service ==  nil, self.bodyCheckout?.products == nil{
                let banner = FloatingNotificationBanner(title:"Eror dalam melakukan checkout",subtitle: "Silahkan periksa data2 yang belum diinputkan")
                banner.show()
            }else{
                let checkout = CourierCheckout(code: self.courierCheckout?.code ?? "", service: self.courierCheckout?.service ?? "")
                let body =  CheckoutCreateBody(courier: checkout, note: "", paymentAccountId: 1, products: ProductCheckout(productId: self.bodyCheckout?.products[0].productId ?? "", qty: self.bodyCheckout?.products[0].qty ?? 0))
                self.interactor?.checkout(body: body)
            }
            
        }
        fpc.set(contentViewController: checkout)
        fpc.show()
        fpc.delegate = self
        fpc.panGestureRecognizer.isEnabled = false
        fpc.addPanel(toParent: self)
        // Do any additional setup after loading the view.
    }
    func shouldProjectMomentum(_ fpc: FloatingPanelController, to proposedTargetPosition: FloatingPanelPosition) -> Bool {
            return false
        }
    func allowsRubberBanding(for edge: UIRectEdge) -> Bool {
            return false
        }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CheckoutViewController:CheckoutPresenterOutput{
    func getMyAddress(data: GetMyAddress) {
        if data.status! >= 400{
            let banner = FloatingNotificationBanner(title:"Silahkan menambahkan alamat",subtitle: "Silahkan masukkan alamat anda untuk menentukan harga kurir",style: .info)
            banner.show()
        }else{
            self.getMyAddress = data
            collectionView.reloadData()
        }
        
    }
    
    func getAllAccountBank(data: GetAllAccountBankDataResponse) {
        if data.status! >= 400{
            let banner = FloatingNotificationBanner(title:"Ooops, ada error",subtitle: "Silahkan periksa data yang anda masukkan",style: .danger)
            banner.show()
        } else if data.status! >= 500{
            let banner = FloatingNotificationBanner(title:"Server error",subtitle: "Ada perbaikan, silahkan akses kembali nanti",style: .danger)
            banner.show()
        }else{
            self.getAllPaymentBank = data
            self.collectionView.reloadData()
        }
        
    }
    
    func getAllCourier(data: CheckoutResponse) {
        self.checkoutData = data
        self.collectionView.reloadData()
    }
    
    func checkout(data: GetCheckoutDataResponse) {
        print(data)
        if data.status! >= 400{
            let banner = FloatingNotificationBanner(title:"Error membuat order",subtitle: "Silahkan cek pesanan anda",style: .danger)
            banner.show()
        }else{
            let viewController = PaymentViewController()
            if let myNumber = NumberFormatter().number(from: data.data?.order_id ?? "") {
                let myInt = myNumber.intValue
                viewController.id = myInt
                self.navigationController?.pushViewController(viewController, animated: true)
                // do what you need to do with myInt
            } else {
                // what ever error code you need to write
            }

            
        }
        
    }
    
    func error(message: String) {
        
    }
    
    
}
extension CheckoutViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        switch section{
        case 0:
            return CGSize(width: 0, height: 0)
        default:
            let width = UIScreen.main.bounds.size.width
            let height = 32.0
            return CGSize(width: width, height: height)
        }
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch indexPath.section{
        case 1:
            let header:AddressHeaderCollectionReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "AddressHeaderCollectionReusableView", for: indexPath) as! AddressHeaderCollectionReusableView
            header.handler = {
                let vc = AddressCourierViewController()
                vc.actionHandler = {
                    self.interactor?.getAllCourier(body: self.bodyCheckout!)
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }
            return header
        case 2:
            let header:LabelCollectionReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LabelCollectionReusableView", for: indexPath) as! LabelCollectionReusableView
            header.labelHeader.text = "Jasa Pengiriman"
            header.labelHeader.font  =  UIFont.boldSystemFont(ofSize: 16)
            return header
        case 3:
            let header:LabelCollectionReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LabelCollectionReusableView", for: indexPath) as! LabelCollectionReusableView
            header.labelHeader.text = "Pembayaran"
            header.labelHeader.font  =  UIFont.boldSystemFont(ofSize: 16)
            return header
        case 4:
            let header:LabelCollectionReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LabelCollectionReusableView", for: indexPath) as! LabelCollectionReusableView
            header.labelHeader.text = "Rincian Belanja"
            header.labelHeader.font  =  UIFont.boldSystemFont(ofSize: 16)
            return header
            
        default:
            let header:LabelCollectionReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LabelCollectionReusableView", for: indexPath) as! LabelCollectionReusableView
            header.labelHeader.text = ""
            return header
        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        5
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section{
        case 0:
            let cell:NoteProductCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "NoteProductCollectionViewCell", for: indexPath) as! NoteProductCollectionViewCell
            return cell
        case 1:
            let cell:AddressCollectionViewCell =
            collectionView.dequeueReusableCell(withReuseIdentifier: "AddressCollectionViewCell", for: indexPath) as! AddressCollectionViewCell
            if let address = getMyAddress{
                cell.nameLabel.text  = "\(address.data?.name ?? "")"
                cell.phoneLabel.text = "\(address.data?.phone ?? "")"
                cell.descriptionLabel.text =  "\(address.data?.detailAddress ?? "") \(address.data?.kecamatan ?? "")\n\(address.data?.city?.name ?? ""), \(address.data?.province?.name ?? ""), \(address.data?.postalCode ?? "")"
                return cell
            }else{
                return cell
            }
            
        case 2:
            let cell:CourierServiceCollectionViewCell =
            collectionView.dequeueReusableCell(withReuseIdentifier: "CourierServiceCollectionViewCell", for: indexPath) as! CourierServiceCollectionViewCell
            cell.clickDropdow = {
                if self.checkoutData?.data == nil{
                    let banner = FloatingNotificationBanner(title:"Error menampilkan kurir",subtitle: "Silahkan masukkan alamat anda untuk menentukan harga kurir")
                    banner.show()
                }else{
                    self.fpc.layout = ShownPickerCourier()
                    self.fpc.contentMode = .fitToBounds
                    let chooseCourier = ChooseCourierViewController()
                    chooseCourier.dataCheckout = self.checkoutData!
                    chooseCourier.actionChooseDone = { value,price,service,code  in
                        self.fpc.layout = HidePickerCourier()
                        self.courier = price
                        cell.titleCourier.text = value
                        self.collectionView.reloadData()
                        let checkout = CheckoutPriceViewController()
                        checkout.price = self.price + self.courier
                        checkout.handler = {
                            if self.courierCheckout?.code ==  nil, self.courierCheckout?.service ==  nil, self.bodyCheckout?.products == nil{
                                let banner = FloatingNotificationBanner(title:"Eror dalam melakukan checkout",subtitle: "Silahkan periksa data2 yang belum diinputkan")
                                banner.show()
                            } else {
                                let checkout = CourierCheckout(code: code, service: service)
                                
                                let body =  CheckoutCreateBody(courier: checkout, note: "", paymentAccountId: 1, products: ProductCheckout(productId: self.bodyCheckout?.products[0].productId ?? "", qty: self.bodyCheckout?.products[0].qty ?? 0))
                                self.interactor?.checkout(body: body)
                            }
                        }
                        self.fpc.set(contentViewController: checkout)
                        self.fpc.show()
                    }
                    chooseCourier.actionChooseClose = {
                        self.fpc.hide()
                    }
                    self.fpc.set(contentViewController: chooseCourier)
                    self.fpc.show()
                }
            }
            return cell
        case 3:
            let cell:PaymentCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PaymentCollectionViewCell", for: indexPath) as! PaymentCollectionViewCell
            cell.showPayment = {
                if self.getAllPaymentBank == nil{
                    let banner = FloatingNotificationBanner(title:"Error menampilkan bank",subtitle: "Silahkan masukkan alamat anda untuk menentukan harga kurir")
                    banner.show()
                } else {
                    self.fpc.layout = ShownPickerCourier()
                    self.fpc.contentMode = .fitToBounds
                    let vc = PaymentGatewayViewController()
                    vc.getAllAccountBank = self.getAllPaymentBank
                    vc.actionHandler = {  id,name in
                    }
                    self.fpc.set(contentViewController: vc)
                    self.fpc.show()
                }
            }
            return cell
        case 4:
            let cell:DetailShoppingCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DetailShoppingCollectionViewCell", for: indexPath) as! DetailShoppingCollectionViewCell
            cell.labelPrice.text = "Rp \(self.price)"
            cell.ongkirPrice.text = "Rp \(self.courier)"
            cell.totalPrice.text = "Rp\(self.price + self.courier)"
            return cell
        default:
            let cell:NoteProductCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "NoteProductCollectionViewCell", for: indexPath) as! NoteProductCollectionViewCell
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.getMyAddress == nil{
            switch section{
            case 1: return 0
            default : return 1
            }
        }else{
            return 1
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.section{
        case 0:
            let width = UIScreen.main.bounds.size.width
            let height = (175 / 375) * width
            return CGSize(width: width, height: height)
        case 1:
            let width = UIScreen.main.bounds.size.width
            let height =  (80 / 375) * width
            return CGSize(width: width, height: height)
        case 2:
            let width = UIScreen.main.bounds.size.width
            let height =  (100 / 375) * width
            return CGSize(width: width, height: height)
        case 3:
            let width = UIScreen.main.bounds.size.width
            let height =  (100 / 375) * width
            return CGSize(width: width, height: height)
        case 4:
            let width = UIScreen.main.bounds.size.width
            let height =  (175 / 375) * width
            return CGSize(width: width, height: height)
        default:
            let width = UIScreen.main.bounds.size.width
            let height = width
            return CGSize(width: width, height: height)
        }
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        switch
//    }
}


class HidePickerCourier:FloatingPanelLayout{
    let position: FloatingPanelPosition = .bottom
    let initialState: FloatingPanelState = .hidden
    var anchors: [FloatingPanelState : FloatingPanelLayoutAnchoring]{
        return [

            .tip: FloatingPanelLayoutAnchor(absoluteInset: 120, edge: .bottom, referenceGuide: .safeArea),
            .hidden: FloatingPanelLayoutAnchor(absoluteInset: 72, edge: .bottom, referenceGuide: .safeArea)
        ]
    }
}

class ShownPickerCourier:FloatingPanelLayout{
    let position: FloatingPanelPosition = .bottom
    let initialState: FloatingPanelState = .tip
    var anchors: [FloatingPanelState : FloatingPanelLayoutAnchoring]{
        return [

            .tip: FloatingPanelLayoutAnchor(absoluteInset: UIScreen.main.bounds.size.height * 0.4, edge: .bottom, referenceGuide: .safeArea),
            .hidden: FloatingPanelLayoutAnchor(absoluteInset: 100, edge: .bottom, referenceGuide: .safeArea)
        ]
    }
}
