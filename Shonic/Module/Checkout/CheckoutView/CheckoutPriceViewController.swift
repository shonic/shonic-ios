//
//  CheckoutPriceViewController.swift
//  Shonic
//
//  Created by Gramedia on 04/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class CheckoutPriceViewController: UIViewController {
    
    var handler:(()->Void)?
    var price:Int?
    @IBOutlet weak var priceLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if let price = price {
            priceLabel.text = "Rp \(price)"
        }
    }
    
    @IBAction func navigation(_ sender: Any) {
        handler!()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
