//
//  NoteProductCollectionViewCell.swift
//  Shonic
//
//  Created by Gramedia on 03/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class NoteProductCollectionViewCell: UICollectionViewCell {
    var increment:(()->Void)?
    var decrement:(()->Void)?
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelProduct: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var massaOfItemLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func incrementAction(_ sender: Any) {
        increment!()
    }
    @IBAction func decrementAction(_ sender: Any) {
        decrement!()
    }
    
    class func nib()-> UINib{
        UINib(nibName: "NoteProductCollectionViewCell", bundle: nil)
    }

}
