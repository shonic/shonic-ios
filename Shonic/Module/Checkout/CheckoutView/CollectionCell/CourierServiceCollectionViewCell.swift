//
//  CourierServiceCollectionViewCell.swift
//  Shonic
//
//  Created by Gramedia on 03/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class CourierServiceCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var dropdownUI: UIView!
    @IBOutlet weak var titleCourier: UILabel!
    var clickDropdow:(()->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        self.dropdownUI.dropDownUI(cornerRadius: 4, borderWidth: 0.8, borderColor: .black)
        
        self.dropdownUI.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickShowPicker)))
        // Initialization code
    }
    
    @objc
    func clickShowPicker(){
        clickDropdow!()
    }
    
    class func nib()-> UINib{
        UINib(nibName: "CourierServiceCollectionViewCell", bundle: nil)
    }

}
