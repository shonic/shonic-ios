//
//  PaymentCollectionViewCell.swift
//  Shonic
//
//  Created by Gramedia on 03/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class PaymentCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var paymentUIView: UIView!
    @IBOutlet weak var labelName: UIView!
    var showPayment:(()->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        paymentUIView.dropDownUI(cornerRadius: 4, borderWidth: 0.8, borderColor: .black)
        paymentUIView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showPicker)))
        // Initialization code
    }
    
    @objc
    func showPicker(){
        showPayment!()
    }
    
    class func nib ()-> UINib{
        UINib(nibName: "PaymentCollectionViewCell", bundle: nil)
    }

}
