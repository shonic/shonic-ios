//
//  SenderAddressCollectionViewCell.swift
//  Shonic
//
//  Created by Gramedia on 04/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class SenderAddressCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var provinceTextfield: UITextField!
    @IBOutlet weak var cityTextfield: UITextField!
    @IBOutlet weak var kecamatanTextfield: UITextField!
    @IBOutlet weak var streetTextfield: UITextField!
    @IBOutlet weak var codePossTextfield: UITextField!
    var provincePicker = UIPickerView()
    var cityPicker = UIPickerView()
    var listProvince:[Province]?
    var listDistrict:[District]?
    var actionIdCities:((Int)->Void)?
    var saveHandler:((String,String,String,Int,Int)->Void)?
    var idProvinces:Int = 0
    var idCity:Int = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        provincePicker.delegate = self
        provincePicker.dataSource = self
        cityPicker.dataSource = self
        cityPicker.delegate = self
        provinceTextfield.inputView = provincePicker
        cityTextfield.inputView = cityPicker
    }
    @IBAction func saveAction(_ sender: Any) {
        saveHandler!(kecamatanTextfield.text!,streetTextfield.text!,codePossTextfield.text!,idProvinces,idCity)
    }
    
    class func nib()-> UINib{
        UINib(nibName: "SenderAddressCollectionViewCell", bundle: nil)
    }

}

extension SenderAddressCollectionViewCell:UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView{
        case cityPicker:
            return listDistrict?.count ?? 0
        case provincePicker:
            return listProvince?.count ?? 0
        default:
            return 0
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView{
        case cityPicker:
            return listDistrict![row].name!
        case provincePicker:
            return listProvince![row].name!
        default:
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView{
        case cityPicker:
            cityTextfield.text = listDistrict![row].name!
            idCity = listDistrict![row].id!
            
        case provincePicker:
            provinceTextfield.text = listProvince![row].name!
            idProvinces =  listProvince![row].id!
            actionIdCities!(listProvince![row].id!)
        default:
            break
        }
    }
    
    
}
