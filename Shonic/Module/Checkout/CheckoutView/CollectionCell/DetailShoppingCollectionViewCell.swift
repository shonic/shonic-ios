//
//  DetailShoppingCollectionViewCell.swift
//  Shonic
//
//  Created by Gramedia on 03/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class DetailShoppingCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var ongkirPrice: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    class func nib ()-> UINib{
        UINib(nibName: "DetailShoppingCollectionViewCell", bundle: nil)
    }

}
