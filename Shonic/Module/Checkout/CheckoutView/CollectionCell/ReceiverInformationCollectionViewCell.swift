//
//  ReceiverInformationCollectionViewCell.swift
//  Shonic
//
//  Created by Gramedia on 04/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class ReceiverInformationCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var nameLabel: UITextField!
    @IBOutlet weak var phoneLabel: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    class func nib()->UINib{
        UINib(nibName: "ReceiverInformationCollectionViewCell", bundle: nil)
    }

}
