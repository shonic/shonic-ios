//
//  PaymentGatewayViewController.swift
//  Shonic
//
//  Created by Gramedia on 18/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class PaymentGatewayViewController: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    
    var getAllAccountBank:GetAllAccountBankDataResponse?
    var actionHandler:((Int,String)->Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.delegate = self
        tableview.dataSource = self
        tableview.register(PaymentCardTableViewCell.nib(), forCellReuseIdentifier: "PaymentCardTableViewCell")
        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PaymentGatewayViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = getAllAccountBank?.data![indexPath.row]
        actionHandler!(data?.id  ?? 0, data?.name ?? "")
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:PaymentCardTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PaymentCardTableViewCell") as! PaymentCardTableViewCell
        let data = getAllAccountBank?.data![indexPath.row]
        cell.labelBank.text = data?.name ?? ""
        if let url = URL(string: data?.image ?? ""){
            cell.logoBank.sd_setImage(with: url, placeholderImage: UIImage(systemName: "gear"))
            return cell
        }
        return UITableViewCell()
    }
    
    
}
