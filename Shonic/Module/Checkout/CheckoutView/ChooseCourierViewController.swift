//
//  ChooseCourierViewController.swift
//  Shonic
//
//  Created by Gramedia on 04/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class ChooseCourierViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var dataCheckout:CheckoutResponse?
    var stringValueChoose:String = ""
    var price:Int = 0
    var actionChooseDone:((String,Int,String,String)->Void)?
    var actionChooseClose:(()->Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(CourierTableViewCell.nib(), forCellReuseIdentifier: "CourierTableViewCell")
        tableView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height * 0.33)
        // Do any additional setup after loading the view.
    }

    @IBAction func actionDone(_ sender: Any) {
        actionChooseDone!(stringValueChoose, price,"","")
    }
    @IBAction func actionClose(_ sender: Any) {
        actionChooseClose!()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ChooseCourierViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return dataCheckout?.data?[section].name ?? ""
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataCheckout?.data?[section].costs?.count ?? 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataCheckout?.data?.count ?? 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = dataCheckout?.data![indexPath.section]
        self.price = data?.costs![indexPath.row].cost![0].value ?? 0
        actionChooseDone!("\(data?.code?.uppercased() ?? "") \(data?.costs![indexPath.row].service ?? "") Rp \(data?.costs![indexPath.row].cost![0].value ?? 0)\nEstimasi \(data?.costs![indexPath.row].cost![0].etd ?? "") hari",data?.costs![indexPath.row].cost![0].value ?? 0,"\(data?.costs![indexPath.row].service ?? "")", "\(data?.code ?? "")")
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CourierTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CourierTableViewCell", for: indexPath) as! CourierTableViewCell
        cell.serviceLabel.text = dataCheckout?.data?[indexPath.section].costs?[indexPath.row].service ?? ""
        cell.priceLabel.text = "Rp \(dataCheckout?.data?[indexPath.section].costs?[indexPath.row].cost?[0].value ?? 0)"
        cell.durationLabel.text = "Estimasi Pengiriman \(dataCheckout?.data?[indexPath.section].costs?[indexPath.row].cost?[0].etd ?? "") hari"
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (63 / 328) * UIScreen.main.bounds.size.width
    }
    
}
