//
//  CheckoutPresenter.swift
//  Shonic
//
//  Created by MEKARI on 17/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

protocol CheckoutPresenterOutput:AnyObject{
    func getAllAccountBank(data:GetAllAccountBankDataResponse)
    func getAllCourier(data:CheckoutResponse)
    func checkout(data:GetCheckoutDataResponse)
    func getMyAddress(data:GetMyAddress)
    func error(message:String)
}

class CheckoutPresenter:CheckoutInteractorOutput{
    func getMyAddress(data: GetMyAddress) {
        self.viewcontroller?.getMyAddress(data: data)
    }
    
    var viewcontroller:CheckoutPresenterOutput?
    init(viewController:CheckoutPresenterOutput) {
        self.viewcontroller = viewController
    }
    func getAllAccountBank(data: GetAllAccountBankDataResponse) {
        self.viewcontroller?.getAllAccountBank(data: data)
    }
    
    func getAllCourier(data: CheckoutResponse) {
        self.viewcontroller?.getAllCourier(data: data)
    }
    
    func checkout(data: GetCheckoutDataResponse) {
        self.viewcontroller?.checkout(data: data)
    }
    
    func error(message: String) {
        self.viewcontroller?.error(message: message)
    }
    
    
    
}
