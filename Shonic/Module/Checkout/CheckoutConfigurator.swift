//
//  CheckoutConfigurator.swift
//  Shonic
//
//  Created by MEKARI on 17/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

enum CheckoutConfigurator{
    static func configure(viewController:CheckoutViewController){
        let presenter = CheckoutPresenter(viewController: viewController)
        let worker = CheckoutWorker()
        let interactor = CheckoutInteractor(worker: worker, presenter: presenter)
        viewController.interactor = interactor
    }
}
