//
//  CheckoutInteractor.swift
//  Shonic
//
//  Created by Gramedia on 03/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

protocol CheckoutInteractorInput{
    func getAllAccountBank()
    func getAllCourier(body:CheckoutCostBody)
    func checkout(body:CheckoutCreateBody)
    func getMyAddress()
}

protocol CheckoutInteractorOutput{
    func getAllAccountBank(data:GetAllAccountBankDataResponse)
    func getAllCourier(data:CheckoutResponse)
    func checkout(data:GetCheckoutDataResponse)
    func getMyAddress(data:GetMyAddress)
    func error(message:String)
}

class CheckoutInteractor{
    var presenter:CheckoutInteractorOutput?
    var worker:CheckoutWorker?
    init(worker:CheckoutWorker,presenter:CheckoutInteractorOutput) {
        self.worker = worker
        self.presenter = presenter
    }
}

extension CheckoutInteractor:CheckoutInteractorInput{
    func getMyAddress() {
        worker?.getAllMyAddress{completion in
            DispatchQueue.main.async {
                switch completion{
                case .success(let data):
                    self.presenter?.getMyAddress(data: data)
                case .failure(let message):
                    if message.isSessionTaskError{
                        self.presenter?.error(message: "Network Error")
                    } else {
                        self.presenter?.error(message: "Uknown Error")
                    }
                }
            }
        }
    }
    
    func getAllAccountBank() {
        worker?.getAllAccountBank{completion in
            DispatchQueue.main.async {
                switch completion{
                case .success(let data):
                    self.presenter?.getAllAccountBank(data: data)
                case .failure(let message):
                    if message.isSessionTaskError{
                        self.presenter?.error(message: "Network Error")
                    } else {
                        self.presenter?.error(message: "Uknown Error")
                    }
                }
            }
        }
    }
    
    func getAllCourier(body: CheckoutCostBody) {
        worker?.postCheckoutCost(by: body){
            completion in
                DispatchQueue.main.async {
                    switch completion{
                    case .success(let data):
                        self.presenter?.getAllCourier(data: data)
                    case .failure(let message):
                        if message.isSessionTaskError{
                            self.presenter?.error(message: "Network Error")
                        } else {
                            self.presenter?.error(message: "Uknown Error")
                        }
                    }
                }
        }
    }
    
    func checkout(body: CheckoutCreateBody) {
        worker?.createCheckout(body: body){
            completion in
                DispatchQueue.main.async {
                    switch completion{
                    case .success(let data):
                        self.presenter?.checkout(data: data)
                    case .failure(let message):
                        if message.isSessionTaskError{
                            self.presenter?.error(message: "Network Error")
                        } else {
                            self.presenter?.error(message: "Uknown Error")
                        }
                    }
                }
        }
    }
    
    
}
