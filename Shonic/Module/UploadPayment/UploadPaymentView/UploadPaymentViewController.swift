//
//  UploadPaymentViewController.swift
//  Shonic
//
//  Created by Gramedia on 24/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class UploadPaymentViewController: UIViewController, UINavigationControllerDelegate {

    @IBOutlet weak var tableview: UITableView!
    var imageData:UIImage?
    var interactor:UploadPaymentInteractor?
    var urlImage:NSURL?
    var bodyPayment:UploadBody?
    var id:Int?
    
    init() {
        super.init(nibName: "UploadPaymentViewController", bundle: nil)
        UploadPaymentConfigurator.configure(viewCotroller: self)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.delegate = self
        tableview.dataSource = self
        tableview.register(UploadImageTableViewCell.nib(), forCellReuseIdentifier: "UploadImageTableViewCell")
        tableview.register(AddInformationPaymentTableViewCell.nib(), forCellReuseIdentifier: "AddInformationPaymentTableViewCell")

        // Do any additional setup after loading the view.
    }
    
    func imagePicker(sourceType:UIImagePickerController.SourceType)->UIImagePickerController{
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = sourceType
        imagePicker.allowsEditing = true
        return imagePicker
    }

    @IBAction func sendAction(_ sender: Any) {
        
        let body = UploadBody(fileUrl: self.urlImage!.absoluteString ?? "", bank_name: bodyPayment!.bank_name, total_transfer: bodyPayment!.total_transfer, bank_number: bodyPayment!.bank_number, name: bodyPayment!.name)
        self.interactor?.pay(body: body, filePath: self.urlImage!, id: "\(id ?? 0)")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UploadPaymentViewController:UploadPaymentPresenterOutput{
    func payResponse(data: GetPaymentResponse) {
        print(data)
    }
    
    func error(message: String) {
        print(message)
    }
    
    
}

extension UploadPaymentViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row{
        case 0  :
            return UIScreen.main.bounds.height * 0.2
        case 1 :
            return UIScreen.main.bounds.height * 0.55
        default:
            return 20
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row{
        case 0:
            let cell:UploadImageTableViewCell = tableView.dequeueReusableCell(withIdentifier: "UploadImageTableViewCell") as! UploadImageTableViewCell
            cell.handler = {
                self.showPickerOptions()
            }
            return cell
        case 1:
            let cell:AddInformationPaymentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AddInformationPaymentTableViewCell") as! AddInformationPaymentTableViewCell
            self.bodyPayment = UploadBody(fileUrl: urlImage?.absoluteString ?? "", bank_name: cell.bankNameTextField.text!, total_transfer:Int(cell.totalTransferTextFeild.text ?? "0") ?? 0, bank_number: cell.numberLicenseTextField.text!, name: cell.customerTextField.text!)
            
            return cell
        default:
            return UITableViewCell()
        }
    }
    func showPickerOptions(){
        let alert = UIAlertController(title: "Pick photo", message: "Choose a picture for submit transacntion", preferredStyle: .actionSheet)
        let  cameraAction  = UIAlertAction(title: "Camera", style: .default){[weak self] _  in
            guard let self  = self else{
                return
            }
            let cameraPicker = self.imagePicker(sourceType: .camera)
            cameraPicker.delegate = self
            
            self.present(cameraPicker, animated: true)
        }
        let  libraryPhotoAction  = UIAlertAction(title: "Gallery", style: .default){[weak self] _  in
            guard let self  = self else{
                return
            }
            let photoPicker = self.imagePicker(sourceType: .photoLibrary)
            photoPicker.delegate = self
            self.present(photoPicker, animated: true)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default,handler: nil)
        alert.addAction(cameraAction)
        alert.addAction(libraryPhotoAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true)
    }
    
    
}
extension UploadPaymentViewController:UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var selectedImage: UIImage!
        var imageUrl: URL!
            
            if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
                selectedImage = image
            } else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                selectedImage = image
            }
            
            if picker.sourceType == UIImagePickerController.SourceType.camera {
                
                let imgName = "\(UUID().uuidString).jpeg"
                let documentDirectory = NSTemporaryDirectory()
                let localPath = documentDirectory.appending(imgName)
                
                let data = selectedImage.jpegData(compressionQuality: 0.3)! as NSData
                data.write(toFile: localPath, atomically: true)
                imageUrl = URL.init(fileURLWithPath: localPath)
                if imageUrl.startAccessingSecurityScopedResource(){
                    print(imageUrl)
                    self.urlImage  = imageUrl as NSURL
                }
                do{
                    let urlImage = try FileManager.default.attributesOfItem(atPath: imageUrl.path)
                    print(urlImage)
                    self.urlImage  = imageUrl as NSURL
                }catch{
                    
                }
                
            } else if let selectedImageUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL {
                imageUrl = selectedImageUrl
                do{
                    let urlImage = try FileManager.default.attributesOfItem(atPath: imageUrl.path)
                    print(urlImage)
                }catch{
                    
                }
                self.urlImage  = imageUrl as NSURL
            }
        self.dismiss(animated: true)
    }
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
}
