//
//  UploadImageTableViewCell.swift
//  Shonic
//
//  Created by Gramedia on 24/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class UploadImageTableViewCell: UITableViewCell {

    @IBOutlet weak var uploadButtonView: UIView!
    var handler:(()->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        let borderLayer = CAShapeLayer()

        borderLayer.strokeColor = UIColor.blue.cgColor
        borderLayer.lineDashPattern = [2,2]
        borderLayer.frame = uploadButtonView.bounds
        borderLayer.fillColor = nil
        borderLayer.path = UIBezierPath(roundedRect: contentView.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 12, height: 12)).cgPath
        uploadButtonView.layer.addSublayer(borderLayer)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(onTapAction))
        uploadButtonView.addGestureRecognizer(gesture)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @objc
    func onTapAction(){
        handler!()
    }
    
    class func nib()-> UINib{
        UINib(nibName: "UploadImageTableViewCell", bundle: nil)
    }
}
