//
//  AddInformationPaymentTableViewCell.swift
//  Shonic
//
//  Created by Gramedia on 24/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class AddInformationPaymentTableViewCell: UITableViewCell {

    @IBOutlet weak var bankNameTextField: UITextField!
    @IBOutlet weak var customerTextField: UITextField!
    @IBOutlet weak var numberLicenseTextField: UITextField!
    @IBOutlet weak var totalTransferTextFeild: UITextField!
    
 
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    class func nib()-> UINib{
        UINib(nibName: "AddInformationPaymentTableViewCell", bundle: nil)
    }
    
}
