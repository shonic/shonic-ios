//
//  UploadPaymentConfigurator.swift
//  Shonic
//
//  Created by Gramedia on 24/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

enum UploadPaymentConfigurator{
    static func configure(viewCotroller:UploadPaymentViewController){
        let presenter = UploadPaymentPresenter(viewController: viewCotroller)
        let worker = UploadPaymentWorker()
        let interactor = UploadPaymentInteractor(worker: worker, presenter: presenter)
        viewCotroller.interactor = interactor
    }
}
