//
//  UploadPaymentPresenter.swift
//  Shonic
//
//  Created by Gramedia on 24/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

protocol UploadPaymentPresenterOutput{
    func payResponse(data:GetPaymentResponse)
    func error(message:String)
}
class UploadPaymentPresenter:UploadPaymentInteractorOutput{
    func response(data: GetPaymentResponse) {
        self.viewController?.payResponse(data: data)
    }
    
    func error(message: String) {
        self.error(message: message)
    }
    
    var viewController:UploadPaymentPresenterOutput?
    init(viewController:UploadPaymentPresenterOutput) {
        self.viewController = viewController
    }
}
