//
//  UploadPaymentInteractor.swift
//  Shonic
//
//  Created by Gramedia on 24/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

protocol UploadPaymentInteractorInput{
    func pay(body:UploadBody,filePath:NSURL,id:String)
}
protocol UploadPaymentInteractorOutput{
    func response(data:GetPaymentResponse)
    func error(message:String)
}

class UploadPaymentInteractor{
    var worker:UploadPaymentWorker
    var presenter:UploadPaymentInteractorOutput?
    init(worker:UploadPaymentWorker,presenter:UploadPaymentInteractorOutput) {
        self.worker = worker
        self.presenter = presenter
    }
}

extension UploadPaymentInteractor:UploadPaymentInteractorInput{
    func pay(body: UploadBody, filePath: NSURL, id: String) {
        self.worker.uploadPayment(id: id, body: body, filePath: filePath){ response in
            DispatchQueue.main.async {
                switch response{
                case.success(let data):
                    if data.status! == 400{
                        self.presenter?.error(message: "Image must be PNG/JPG/JPEG")
                    } else if data.status! >= 401{
                        self.presenter?.error(message: "Need Check data of submit")
                    }else{
                        self.presenter?.response(data: data)
                    }
                case .failure(let error):
                    self.presenter?.error(message: error.localizedDescription)
                }
            }
        }
    }
}
