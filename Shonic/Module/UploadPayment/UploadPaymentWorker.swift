//
//  UploadPaymentWorker.swift
//  Shonic
//
//  Created by Gramedia on 24/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation
import Alamofire

protocol UploadPaymentSceneLogic{
    func uploadPayment(id:String,body:UploadBody,filePath:NSURL,callback:@escaping(Result<GetPaymentResponse,AFError>)->Void)
}

class UploadPaymentWorker{
    var service = ApiClient.shared
    var baseUrl = "http://shonic-test.herokuapp.com"
    let userDefault = UserDefaults.standard
}

extension UploadPaymentWorker:UploadPaymentSceneLogic{
    func uploadPayment(id:String,body: UploadBody, filePath: NSURL, callback: @escaping (Result<GetPaymentResponse, AFError>) -> Void) {
        let authorization = "Bearer \(userDefault.string(forKey: "token")!)"
        let headers:HTTPHeaders = [
            "Authorization":authorization
        ]
        service.requestUpload(url: "\(baseUrl)/api/v1/order/\(id)/payment", body: body, filePath: filePath, method: .post, header: headers){(response:Result<GetPaymentResponse,AFError>) in
            switch response {
            case .success(let data):
                callback(.success(data))
            case .failure(let error):
                callback(.failure(error))
            }
        }
    }
    
    
}
