//
//  CompletingRegisterConfigurator.swift
//  Shonic
//
//  Created by Dzaki Izza on 10/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation
enum CompletingRegisterConfigurator {
    static func configure(viewController: CompletingRegisterViewController) {
        
        let presenter = CompletingRegisterPresenter(viewController: viewController)
        
        let service = DefaultAuthService()
        let worker = CompletingRegisterAuthWorker(service: service)
        let interactor = CompletingRegisterInteractor(presenter: presenter, worker: worker)

        viewController.interactor = interactor
        
        
    }
}
