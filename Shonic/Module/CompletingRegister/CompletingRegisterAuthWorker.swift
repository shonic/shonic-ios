//
//  RegisterAuthWorker.swift
//  Shonic
//
//  Created by Dzaki Izza on 08/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation
import Combine

protocol CompletingRegisterAuthLogic{
    func makeAuth(user:CompletingRegisterParameter,completion:@escaping(Result<CompletingRegisterResponseModel,ErrorMessage>)-> Void)
}

final class CompletingRegisterAuthWorker{
    private let service:AuthService
    private var bag = Set<AnyCancellable>()
    
    init(service:AuthService){
        self.service = service
    }
    
    enum CompletingRegisterAUthWorker:Error{
        case authFailed(String)
        case unauthorized
    }
}

extension CompletingRegisterAuthWorker:CompletingRegisterAuthLogic{
    func makeAuth(user: CompletingRegisterParameter, completion: @escaping (Result<CompletingRegisterResponseModel, ErrorMessage>) -> Void) {
        service.resgiter(email: user.email, password: user.password, fullName: user.fullName){(callback) in
            print(callback)
            switch callback{
            case .success(let result):
                completion(.success(result))
                break
                
            case .failure(let error):
                completion(.failure(error))
                break
            }
        }
    }
}

