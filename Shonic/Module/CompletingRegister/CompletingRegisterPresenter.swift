//
//  RegisterPresenter.swift
//  Shonic
//
//  Created by Dzaki Izza on 08/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

protocol CompletingRegisterPresenterOutput: Any {
    func success()
    func failure(message:String)
}

class CompletingRegisterPresenter:CompletingRegisterInteractorOutput{
    init(viewController: CompletingRegisterPresenterOutput){
        self.viewController = viewController
    }
    
    func presentRegister(response: CompletingRegisterResponseModel) {
        viewController?.success()
    }
    func presentAlert(message: String) {
        viewController?.failure(message: message)
    }
    //weak var viewController = LoginPresenterOutput?
    var viewController:CompletingRegisterPresenterOutput?
}

