//
//  RegisterInteractor.swift
//  Shonic
//
//  Created by Dzaki Izza on 08/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation
import Alamofire

protocol CompletingRegisterInteractorInput:AnyObject{
    func tryToRegister(email:String,password:String,fullName:String)
}

protocol CompletingRegisterInteractorOutput:Any{
    func presentRegister(response:CompletingRegisterResponseModel)
    func presentAlert(message:String)
}

final class CompletingRegisterInteractor{
    init(presenter: CompletingRegisterInteractorOutput, worker: CompletingRegisterAuthWorker){
        self.presenter = presenter
        self.worker = worker
    }
    var presenter:CompletingRegisterInteractorOutput?
    var worker:CompletingRegisterAuthWorker?
}

extension CompletingRegisterInteractor:CompletingRegisterInteractorInput{
    func tryToRegister(email: String, password: String, fullName: String) {
        let register = CompletingRegisterParameter(email: email, password: password, fullName: fullName)
        print(register)
        worker?.makeAuth(user: register){ result in
            print(result)
            DispatchQueue.main.async {
                [weak self] in
                switch result{
                case .success(let data):
                    print(data)
                    self?.presenter?.presentRegister(response: data)
                case .failure(_):
                    print("error")
                    self?.presenter?.presentAlert(message: "Error Email / Password")
                }
            }
        }
    }
}

