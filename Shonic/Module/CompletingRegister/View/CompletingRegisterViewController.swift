//
//  CompletingRegisterViewController.swift
//  Shonic
//
//  Created by Dzaki Izza on 05/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit


class CompletingRegisterViewController: UIViewController, CompletingRegisterPresenterOutput {

    @IBOutlet weak var doneBtn: RegularButton!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    @IBOutlet weak var errorMessage: UILabel!
    @IBOutlet weak var doneBtnBottomConstraint: NSLayoutConstraint!
    init() {
        super.init(nibName: "CompletingRegisterViewController", bundle: nil)
        CompletingRegisterConfigurator.configure(viewController: self)
        self.hidesBottomBarWhenPushed = true
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    var iconClick = false
    let iconEye = UIImageView()
    
    var emailTemp : String?
    
    var interactor: CompletingRegisterInteractorInput?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        passwordTF.enablePasswordToggle()
        confirmPasswordTF.enablePasswordToggle()
        completionForm()
        textFieldSetup()
        registerBtnSetup()
        
        
    }
    
    func failure(message: String) {
        let alert = UIAlertController(title: "\(message)", message: "Please check again", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: {_ in}))
        self.present(alert, animated: true)
    }
    func success() {
        print("Aman")
        self.navigationController?.pushViewController(TabBarViewController(), animated: true)
    }
    @IBAction func doneBtnTapped(_ sender: Any) {
        self.interactor?.tryToRegister(email: emailTemp ?? "first@gmail.com", password: passwordTF.text!, fullName: nameTF.text!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Lengkapi Pendaftaran"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Kembali", style: .plain, target: nil, action: nil)
    }
    
    func textFieldSetup(){
        [nameTF, passwordTF, confirmPasswordTF].forEach{
            $0?.addTarget(self, action: #selector(validateFields), for: .editingChanged)
            $0?.addTarget(self, action: #selector(touchTextField(_:)), for: .editingDidBegin)
        }
    }
    
    func completionForm(){
        errorMessage.isHidden = true
        errorMessage.text = "Password Tidak Sama"
        nameTF.text = ""
        passwordTF.text = ""
        confirmPasswordTF.text = ""
        
        confirmPasswordTF.layer.borderWidth = 1
        confirmPasswordTF.layer.cornerRadius = 8
        confirmPasswordTF.layer.masksToBounds = true
        
        nameTF.layer.borderWidth = 1
        nameTF.layer.cornerRadius = 8
        nameTF.layer.masksToBounds = true
        nameTF.autocorrectionType = .no
        
        passwordTF.layer.borderWidth = 1
        passwordTF.layer.cornerRadius = 8
        passwordTF.layer.masksToBounds = true
        
        nameTF.layer.borderColor = UIColor.systemGray5.cgColor
        passwordTF.layer.borderColor = UIColor.systemGray5.cgColor
        confirmPasswordTF.layer.borderColor = UIColor.systemGray5.cgColor
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyBoard)))
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func hideKeyBoard(){
        self.view.endEditing(true)
    }
    
    @objc private func keyboardWillHide(){
        doneBtnBottomConstraint.constant = 50
    }
    
    @objc private func keyboardWillShow(notification: NSNotification){
        
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as?
            NSValue {
            let keyboardHeight = keyboardFrame.cgRectValue.height
            doneBtnBottomConstraint.constant = keyboardHeight - (keyboardHeight/2) + 100
        }
    }
    
    @objc func touchTextField(_ sender: UITextField) {
        switch sender {
        case nameTF:
            nameTF.layer.borderColor = UIColor.systemBlue.cgColor
            passwordTF.layer.borderColor = UIColor.systemGray5.cgColor
            confirmPasswordTF.layer.borderColor = UIColor.systemGray5.cgColor
        case passwordTF:
            nameTF.layer.borderColor = UIColor.systemGray5.cgColor
            passwordTF.layer.borderColor = UIColor.systemBlue.cgColor
            confirmPasswordTF.layer.borderColor = UIColor.systemGray5.cgColor
        case confirmPasswordTF:
            nameTF.layer.borderColor = UIColor.systemGray5.cgColor
            passwordTF.layer.borderColor = UIColor.systemGray5.cgColor
            confirmPasswordTF.layer.borderColor = UIColor.systemBlue.cgColor
        default:
            break
        }
    }
    
    @objc func validateFields() {
        var validations: [Bool] = []
        
        [nameTF, passwordTF, confirmPasswordTF].forEach{

            if let pass = passwordTF.text {
                if let confirmPass = confirmPasswordTF.text {
                    if confirmPass != pass {
                        errorMessage.text = "Password tidak sama"
                        errorMessage.isHidden = false
                        confirmPasswordTF.layer.borderColor = UIColor.systemRed.cgColor
                        
                    }else {
                        if let text = $0?.text {
                            if !text.isEmpty {
                                validations.append(true)
                            }
                        }
                        errorMessage.isHidden = true
                        confirmPasswordTF.layer.borderColor = UIColor.systemGray5.cgColor
                    }
                }
            }
        }

        if validations.count > 2 {
            doneBtn.isUserInteractionEnabled = true
            doneBtn.tintColor = UIColor(named: "PrimaryBlue")
            
        } else{
            doneBtn.isUserInteractionEnabled = false
            doneBtn.tintColor = UIColor(named: "PrimaryFocus")
        }
        
    }
    
    func registerBtnSetup(){
        doneBtn.layer.cornerRadius = 8
        doneBtn.layer.masksToBounds = true
        doneBtn.tintColor = UIColor(named: "PrimaryFocus")
        doneBtn.isUserInteractionEnabled = false
    }
    
}
