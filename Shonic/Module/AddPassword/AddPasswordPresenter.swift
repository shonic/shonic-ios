//
//  AddPasswordPresenter.swift
//  Shonic
//
//  Created by Andira Yunita on 10/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

protocol AddNewPasswordPresenterOutput: Any {
    func success()
    func failure(message:String)
}

class AddNewPasswordPresenter: AddNewPasswordInteractorOutput {
    init(viewController: AddNewPasswordPresenterOutput) {
        self.viewController = viewController
    }
    
    func presentAddNewPassword(response: AddNewPasswordResponseModel) {
        viewController?.success()
    }
    
    func presentAlert(message: String) {
        viewController?.failure(message: message)
    }
    
    var viewController: AddNewPasswordPresenterOutput?
}
