//
//  AddPasswordConfigurator.swift
//  Shonic
//
//  Created by Andira Yunita on 11/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

enum AddPasswordConfigurator {
    static func configure(viewController: AddPasswordViewController) {
        let presenter = AddNewPasswordPresenter(viewController: viewController)
        
        let service = DefaultAuthService()
        let worker = AddNewPasswordWorker(service: service)
        let interactor = AddNewPasswordInteractor(presenter: presenter, worker: worker)
        
        viewController.interactor = interactor
    }
}
