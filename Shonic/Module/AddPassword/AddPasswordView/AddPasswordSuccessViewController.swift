//
//  AddPasswordSuccessViewController.swift
//  Shonic
//
//  Created by Andira Yunita on 14/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class AddPasswordSuccessViewController: UIViewController {
    @IBOutlet weak var resetSuccessImage: UIImageView!
    @IBOutlet weak var successLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var backToHomeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpBackToHomeButton()
    }
    
    func setUpBackToHomeButton() {
        backToHomeButton.configuration?.imagePadding = 8
        backToHomeButton.backgroundColor = .clear
        backToHomeButton.layer.cornerRadius = 8
        backToHomeButton.layer.borderWidth = 1
        backToHomeButton.layer.borderColor = UIColor.systemGray5.cgColor
    }

    @IBAction func loginTapped(_ sender: Any) {
        self.navigationController?.pushViewController(LoginViewController(), animated: true)
    }
    
    @IBAction func backToHomeTapped(_ sender: Any) {
        self.navigationController?.pushViewController(RegisterViewController(), animated: true)
    }
}
