//
//  AddPasswordViewController.swift
//  Shonic
//
//  Created by Andira Yunita on 04/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit
import NotificationBannerSwift

class AddPasswordViewController: UIViewController {
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmNewPasswordTextField: UITextField!
    @IBOutlet weak var errorMessageLabel: UILabel!
    @IBOutlet weak var saveButton: RegularButton!
    @IBOutlet weak var bottomSaveConstraint: NSLayoutConstraint!
    
    var iconClick = false
    let iconEye = UIImageView()
    var emailTemp : String?
    var token: String?
    var interactor: AddNewPasswordInteractorInput?
    
    init() {
        super.init(nibName: "AddPasswordViewController", bundle: nil)
        AddPasswordConfigurator.configure(viewController: self)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Atur Ulang Password"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Kembali", style: .plain, target: nil, action: nil)
        
        newPasswordTextField.enablePasswordToggle()
        confirmNewPasswordTextField.enablePasswordToggle()
        
        formPassword()
        setUpTextField()
        setUpSaveButton()
    }
    
    func formPassword() {
        errorMessageLabel.isHidden = true
        errorMessageLabel.text = ""
        newPasswordTextField.text = ""
        confirmNewPasswordTextField.text = ""
    }
    
    func setUpSaveButton() {
        saveButton.layer.cornerRadius = 8
        saveButton.layer.masksToBounds = true
        saveButton.tintColor = UIColor(named: "PrimaryFocus")
        saveButton.isUserInteractionEnabled = false
    }
    
    func isPasswordValid(_ value: String) -> String? {
        guard let newPassword = newPasswordTextField.text, !newPassword.isEmpty,
              let confirmNewPassword = confirmNewPasswordTextField.text, !confirmNewPassword.isEmpty else {
            return "Password tidak boleh kosong"
        }
        
        guard newPassword == confirmNewPassword else {
            return "Password tidak sama"
        }
        return nil
    }
    
    func validatePassword() {
        if errorMessageLabel.isHidden {
            saveButton.isUserInteractionEnabled = true
            saveButton.tintColor = UIColor(named: "PrimaryBlue")
        } else {
            saveButton.isUserInteractionEnabled = false
            saveButton.tintColor = UIColor(named: "PrimaryFocus")
        }
    }
    
    func setUpTextField() {
        newPasswordTextField.layer.borderWidth = 1
        newPasswordTextField.layer.cornerRadius = 8
        newPasswordTextField.layer.masksToBounds = true
        newPasswordTextField.layer.borderColor = UIColor.systemGray5.cgColor
        
        confirmNewPasswordTextField.layer.borderWidth = 1
        confirmNewPasswordTextField.layer.cornerRadius = 8
        confirmNewPasswordTextField.layer.masksToBounds = true
        confirmNewPasswordTextField.layer.borderColor = UIColor.systemGray5.cgColor
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyBoard)))
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func hideKeyBoard(){
        self.view.endEditing(true)
    }
    
    @objc private func keyboardWillHide(){
        bottomSaveConstraint.constant = 50
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as?
            NSValue {
            let keyboardHeight = keyboardFrame.cgRectValue.height
            bottomSaveConstraint.constant = keyboardHeight - 50
        }
    }
    
    @IBAction func confirmPassword(_ sender: Any) {
        if let password = confirmNewPasswordTextField.text {
            if let error = isPasswordValid(password) {
                errorMessageLabel.text = error
                errorMessageLabel.isHidden = false
                confirmNewPasswordTextField.layer.borderColor = UIColor.systemRed.cgColor
            } else {
                errorMessageLabel.isHidden = true
                confirmNewPasswordTextField.layer.borderColor = UIColor.systemGray5.cgColor
            }
        }
        validatePassword()
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        self.interactor?.tryToAddNewPassword(email: emailTemp!, newPassword: newPasswordTextField.text!, token: token!)
    }
}

extension AddPasswordViewController: AddNewPasswordPresenterOutput {
    func success() {
        print("Add new password success!")
        self.navigationController?.pushViewController(AddPasswordSuccessViewController(), animated: true)
    }
    
    func failure(message: String) {
        let banner = FloatingNotificationBanner(title:"Error", subtitle: "Password baru harus berbeda dengan password lama", style: .danger)
        banner.show()
    }
}
