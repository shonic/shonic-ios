//
//  AddPasswordInteractor.swift
//  Shonic
//
//  Created by Andira Yunita on 10/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation
import Alamofire

protocol AddNewPasswordInteractorInput: AnyObject {
    func tryToAddNewPassword(email: String, newPassword: String, token: String)
}

protocol AddNewPasswordInteractorOutput: Any {
    func presentAddNewPassword(response: AddNewPasswordResponseModel)
    func presentAlert(message: String)
}

final class AddNewPasswordInteractor {
    var presenter: AddNewPasswordInteractorOutput?
    var worker: AddNewPasswordWorker?
    
    init(presenter: AddNewPasswordInteractorOutput, worker: AddNewPasswordWorker) {
        self.presenter = presenter
        self.worker = worker
    }
}

extension AddNewPasswordInteractor: AddNewPasswordInteractorInput {
    func tryToAddNewPassword(email: String, newPassword: String, token: String) {
        let addNewPassword = AddNewPasswordParameter(email: email, newPassword: newPassword, token: token)
        print(addNewPassword)
        worker?.makeAuth(user: addNewPassword) { result in
            print(result)
            DispatchQueue.main.async { [weak self] in
                switch result {
                case .success(let data):
                    print(data)
                    self?.presenter?.presentAddNewPassword(response: data)
                case .failure(_):
                    print("error")
                    self?.presenter?.presentAlert(message: "Error Password")
                }
            }
        }
    }
}
