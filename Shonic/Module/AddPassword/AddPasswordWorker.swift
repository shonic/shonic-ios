//
//  AddPasswordWorker.swift
//  Shonic
//
//  Created by Andira Yunita on 10/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation
import Combine

protocol AddNewPasswordAuthLogic {
    func makeAuth(user: AddNewPasswordParameter, completion: @escaping(Result<AddNewPasswordResponseModel, ErrorMessage>) -> Void)
}

final class AddNewPasswordWorker {
    private let service: AuthService
    private var bag = Set<AnyCancellable>()
    
    init(service: AuthService){
        self.service = service
    }
    
    enum AddNewPasswordWorker: Error{
        case authFailed(String)
        case unauthorized
    }
}

extension AddNewPasswordWorker: AddNewPasswordAuthLogic {
    func makeAuth(user: AddNewPasswordParameter, completion: @escaping (Result<AddNewPasswordResponseModel, ErrorMessage>) -> Void) {
        service.addNewPassword(email: user.email, newPassword: user.newPassword, token: user.token) { (callback) in
            print(callback)
            switch callback {
            case .success(let result):
                completion(.success(result))
                break
                
            case .failure(let error):
                completion(.failure(error))
                break
            }
        }
    }
}
