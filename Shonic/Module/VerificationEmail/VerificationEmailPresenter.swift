//
//  VerificationEmailPresenter.swift
//  Shonic
//
//  Created by MEKARI on 08/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

protocol VerificationEmailPresenterOutput: Any {
    func successSendOTPRegister(message:String)
    func successOTPVerificationRegister()
    func successSendOTPForgotPassword(message:String)
    func successOTPVerificationForgotPassword()
    func failed()
}
class VerificationEmailPresenter:VerificationEmailInteractorOutput{
    init(viewController: VerificationEmailPresenterOutput){
        self.viewController = viewController
    }
    func successVerificationOTPForgotPassword(message: String) {
        self.viewController?.successOTPVerificationForgotPassword()
    }
    
    func successSendOTPForgotPassword(message: String) {
        self.successSendOTPRegister(message: message)
    }
    
    func successVerificationOTPRegister(message: String) {
        self.viewController?.successOTPVerificationRegister()
    }
    
    func successSendOTPRegister(message: String) {
        self.viewController?.successSendOTPRegister(message: message)
    }
    
    func failed(message: String) {
        self.viewController?.failed()
    }
    var viewController:VerificationEmailPresenterOutput?
    
}
