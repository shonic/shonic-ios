//
//  VerificationResponseModel.swift
//  Shonic
//
//  Created by MEKARI on 08/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

class VerificationEmailResponseModel:Codable{
    var timestamp:String?
    var status:Int
    var error:String?
    var path:String?
    var message:String?
}

class VerificationOTPParameter:Encodable{
    var email:String
    var otp:Int
    init(email:String,otp:Int) {
        self.email = email
        self.otp = otp
    }
}
