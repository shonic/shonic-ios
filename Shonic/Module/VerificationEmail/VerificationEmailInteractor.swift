//
//  VerificationEmailInteractor.swift
//  Shonic
//
//  Created by MEKARI on 08/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

protocol VerificationEmailInteractorInput:AnyObject{
    func sendOTPRegister(email:String)
    func verificationOTPRegister(email:String,code:Int)
    func sendOTPForgotPassword(email:String)
    func verificationOTPForgotPassword(email:String,code:Int)
}

protocol VerificationEmailInteractorOutput: AnyObject {
    func successVerificationOTPRegister(message:String)
    func successSendOTPRegister(message:String)
    func successVerificationOTPForgotPassword(message:String)
    func successSendOTPForgotPassword(message:String)
    func failed(message:String)
}

class VerificationEmailInteractor{
    init(presenter:VerificationEmailInteractorOutput , worker: VerificationEmailAuthWorker){
        self.presenter = presenter
        self.worker = worker
    }
    var presenter:VerificationEmailInteractorOutput?
    var worker:VerificationEmailAuthWorker?
}
extension VerificationEmailInteractor:VerificationEmailInteractorInput{
    func sendOTPForgotPassword(email: String) {
        self.worker?.sendForgotPasswordOTP(email: email){callback in
            DispatchQueue.main.async {
                switch callback{
                    
                case .success(_):
                    self.presenter?.successSendOTPRegister(message:"Nice")
                case .failure(_):
                    self.presenter?.failed(message: "Please check your email")
                }
            }
        }
    }
    
    func verificationOTPForgotPassword(email: String, code: Int) {
        self.worker?.verificationForgotPasswordOTP(email: email, otp: code){callback in
            DispatchQueue.main.async {
                switch callback{
                case .success(_):
                    self.presenter?.successVerificationOTPForgotPassword(message: "Nice")
                case .failure(_):
                    self.presenter?.failed(message: "Please check your code OTP")
                }
            }
        }
    }
    
    func sendOTPRegister(email: String) {
        self.worker?.sendRegisterOTP(email: email){callback in
            DispatchQueue.main.async {
                switch callback{
                    
                case .success(_):
                    self.presenter?.successSendOTPRegister(message:"Nice")
                case .failure(_):
                    self.presenter?.failed(message: "Please check your email")
                }
            }
        }
    }
    func verificationOTPRegister(email:String,code: Int) {
        self.worker?.verificationRegisterOTP(email: email, otp: code){callback in
            DispatchQueue.main.async {
                switch callback{
                case .success(_):
                    self.presenter?.successVerificationOTPRegister(message: "Nice")
                case .failure(_):
                    self.presenter?.failed(message: "Please check your code OTP")
                }
            }
        }
    }
}
