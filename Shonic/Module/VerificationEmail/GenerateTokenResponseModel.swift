//
//  GenerateTokenResponseModel.swift
//  Shonic
//
//  Created by MEKARI on 08/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

struct GenerateTokenReponseModel:Codable{
    var timestamp:String?
    var status:Int
    var error:String?
    var path:String?
    var message:String?
}
struct EmailData:Encodable{
    var email:String
}
