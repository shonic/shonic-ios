//
//  VerificationEmailWorker.swift
//  Shonic
//
//  Created by MEKARI on 08/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation
import Combine

protocol VerificationEmailAuthLogic{
    func sendRegisterOTP(email:String,callback:@escaping(Result<GenerateTokenReponseModel,Error>)->Void)
    func verificationRegisterOTP(email:String,otp:Int,callback:@escaping(Result<VerificationEmailResponseModel,Error>)->Void)
    func sendForgotPasswordOTP(email:String,callback:@escaping(Result<GenerateTokenReponseModel,Error>)->Void)
    func verificationForgotPasswordOTP(email:String,otp:Int,callback:@escaping(Result<VerificationEmailResponseModel,Error>)->Void)
}
final class VerificationEmailAuthWorker{
    private let service:AuthService
    private var bag = Set<AnyCancellable>()
    
    init(service:AuthService) {
        self.service = service
    }
    
    enum VerificationSceneAuthWorkerError:Error{
        case failedSendOTP(String)
        case unauthorized
    }
    
}

extension VerificationEmailAuthWorker:VerificationEmailAuthLogic{
    func sendForgotPasswordOTP(email: String, callback: @escaping (Result<GenerateTokenReponseModel, Error>) -> Void) {
        service.generateTokenResetPassword(email: email){response in
            switch response{
            case .success(let data):
                callback(.success(data))
            case .failure(_):
                callback(.failure(ErrorMessage.error))
            }
        }
    }
    
    func verificationForgotPasswordOTP(email: String, otp: Int, callback: @escaping (Result<VerificationEmailResponseModel, Error>) -> Void) {
        service.verificationOTPResetPassword(email: email, token: otp){response in
            switch response{
            case .success(let data):
                callback(.success(data))
            case .failure(_):
                callback(.failure(ErrorMessage.error))
            }
        }
    }
    
    func sendRegisterOTP(email: String, callback: @escaping (Result<GenerateTokenReponseModel, Error>) -> Void) {
        service.generateTokenRegister(email: email){response in
            switch response{
            case .success(let data):
                callback(.success(data))
            case .failure(_):
                callback(.failure(ErrorMessage.error))
            }
        }
    }
    func verificationRegisterOTP(email: String, otp: Int, callback: @escaping (Result<VerificationEmailResponseModel, Error>) -> Void) {
        service.verificationOTPRegister(email: email, token: otp){response in
            switch response{
            case .success(let data):
                callback(.success(data))
            case .failure(_):
                callback(.failure(ErrorMessage.error))
            }
        }
    }
    
}
