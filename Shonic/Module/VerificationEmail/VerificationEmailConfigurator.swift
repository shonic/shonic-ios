//
//  VerificationEmailConfigurator.swift
//  Shonic
//
//  Created by Dzaki Izza on 12/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation
enum VerificationEmailConfigurator {
  static func configure(viewController: VerificationEmailViewController) {
    let presenter = VerificationEmailPresenter(viewController: viewController)
    let service = DefaultAuthService()
    let worker = VerificationEmailAuthWorker(service: service)
    let interactor = VerificationEmailInteractor(presenter: presenter, worker: worker)

    viewController.interactor = interactor
  }
}
