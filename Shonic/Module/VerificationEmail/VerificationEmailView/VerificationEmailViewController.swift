//
//  VerificationEmailViewController.swift
//  team-d-iOS
//
//  Created by MEKARI on 31/05/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit
import NotificationBannerSwift

class VerificationEmailViewController: UIViewController,VerificationEmailPresenterOutput {
    func successSendOTPForgotPassword(message: String) {
        let banner = FloatingNotificationBanner(title:"Sukses Request",subtitle:"Kode OTP Sudah terkirim di email, silahkan di cek",style: .success)
        banner.show()
    }
    
    func successOTPVerificationForgotPassword() {
        let banner = FloatingNotificationBanner(title:"Sukses Verifikasi",subtitle:"Kode OTP Sudah diverifikasi, silahkan melanjutkan pengaturan ulang kata sandi",style: .success)
        banner.show()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2){
            let completion = AddPasswordViewController()
            completion.emailTemp = self.email
            completion.token = "\(self.one)"
            
            self.navigationController?.pushViewController(completion, animated: true)
        }
    }
    
    @IBOutlet weak var informationVerificationLabel: UILabel!
    @IBOutlet weak var code1TextField: UITextField!
    @IBOutlet weak var code2TextField: UITextField!
    @IBOutlet weak var code3TextField: UITextField!
    @IBOutlet weak var code4TextField: UITextField!
    @IBOutlet weak var code5TextField: UITextField!
    @IBOutlet weak var code6TextField: UITextField!
    @IBOutlet weak var resendCodeLabel: UILabel!
    @IBOutlet weak var bottomVerificationBtnConstraint: NSLayoutConstraint!
    @IBOutlet weak var verificationBtn: UIButton!
    @IBOutlet weak var resendButton: UIButton!
    var one:Int = 0
    var buttonResend = UIButton()
    var interactor:VerificationEmailInteractor?
    
    init() {
        super.init(nibName: "VerificationEmailViewController", bundle: nil)
        VerificationEmailConfigurator.configure(viewController: self)
        self.hidesBottomBarWhenPushed = true
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var email:String = "firstcustomer@gmail.com"
    var seconds:Int = 20
    var status:String = ""
    
    override func viewWillAppear(_ animated: Bool) {
        let banner = FloatingNotificationBanner(title:"Sukses Request",subtitle:"Kode OTP Sudah terkirim di email, silahkan di cek",style: .success)
        banner.show()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.timeDuration()
        self.code1TextField.delegate = self
        self.code2TextField.delegate = self
        self.code3TextField.delegate = self
        self.code4TextField.delegate = self
        self.code5TextField.delegate = self
        self.code6TextField.delegate = self
        self.code1TextField.addTarget(self, action: #selector(changeCharacter), for: .editingChanged)
        self.code2TextField.addTarget(self, action: #selector(changeCharacter), for: .editingChanged)
        self.code3TextField.addTarget(self, action: #selector(changeCharacter), for: .editingChanged)
        self.code4TextField.addTarget(self, action: #selector(changeCharacter), for: .editingChanged)
        self.code5TextField.addTarget(self, action: #selector(changeCharacter), for: .editingChanged)
        self.code6TextField.addTarget(self, action: #selector(changeCharacter), for: .editingChanged)
        self.setupInformationLabel()
        self.titleNavigation(title: "Verifikasi")
        self.resendCodeLabel.numberOfLines = 2
        self.resendCodeLabel.textAlignment = .center
        self.textFieldSetup()
        self.keyboardSetup()
        self.verificationBtnSetup()
        resendButton.isHidden = true
        resendButtonAction()
        self.resendCodeLabel.text = "Mohon tunggu dalam \(self.seconds) detik untuk kirim ulang"
        
    }
    
    private func resendButtonAction(){
        self.resendButton.addTarget(self, action: #selector(actionOfResend), for: .touchUpInside)
    }
    @objc private func actionOfResend(){
        if status == "register"{
            self.interactor?.sendOTPRegister(email:email)
        }else{
            self.interactor?.sendOTPForgotPassword(email: email)
        }
        
    }
    
    func timeDuration(){
        let timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: {
            timer in
            //AnimationThick
            self.seconds -= 1
            self.resendCodeLabel.text = "Mohon tunggu dalam \(self.seconds) detik untuk kirim ulang"
            if self.seconds < 1 {
                timer.invalidate()
                self.resendButton.isHidden = false
                self.buttonResend.titleLabel?.text = "Resend"
                self.resendCodeLabel.addSubview(self.buttonResend)
                self.resendCodeLabel.text = "Tidak menerima kodel verifikasi?"
            }
            
        })
        
        RunLoop.current.add(timer, forMode: .default)
    }
    
    
    @IBAction func verificationAction(_ sender: Any) {
        print(status)
        if self.status == "register"{
            if code1TextField.text!.isEmpty || code2TextField.text!.isEmpty || code3TextField.text!.isEmpty || code4TextField.text!.isEmpty||code5TextField.text!.isEmpty || code6TextField.text!.isEmpty{
                let banner = FloatingNotificationBanner(title:"Masukkan kode OTP",subtitle:"Periksa tidak ada yang kosong",style: .danger)
                banner.show()
            }
            else{
                 one = NumberFormatter().number(from: "\(code1TextField.text!)\(code2TextField.text!)\(code3TextField.text!)\(code4TextField.text!)\(code5TextField.text!)\(code6TextField.text!)")!.intValue
                
              
               
                self.interactor?.verificationOTPRegister(email: email, code:one )
            }
        }else{
            if code1TextField.text!.isEmpty || code2TextField.text!.isEmpty || code3TextField.text!.isEmpty || code4TextField.text!.isEmpty||code5TextField.text!.isEmpty || code6TextField.text!.isEmpty{
                let banner = FloatingNotificationBanner(title:"Masukkan kode OTP",subtitle:"Periksa tidak ada yang kosong",style: .danger)
                banner.show()
            }
            else{
                 one = NumberFormatter().number(from: "\(code1TextField.text!)\(code2TextField.text!)\(code3TextField.text!)\(code4TextField.text!)\(code5TextField.text!)\(code6TextField.text!)")!.intValue
                self.interactor?.verificationOTPForgotPassword(email: email, code:one )
            }
        }
        
    }
    @IBAction func clearAction(_ sender: Any) {
        self.code1TextField.text = ""
        self.code2TextField.text = ""
        self.code3TextField.text = ""
        self.code4TextField.text = ""
        self.code5TextField.text = ""
        self.code6TextField.text = ""
        self.code1TextField.becomeFirstResponder()
    }
    
    
    @objc func changeCharacter(textField:UITextField){
        
        if textField.text?.count == 1{
            switch textField{
            case code1TextField:
                code2TextField.becomeFirstResponder()
            case code2TextField:
                code3TextField.becomeFirstResponder()
            case code3TextField:
                code4TextField.becomeFirstResponder()
            case code4TextField:
                code5TextField.becomeFirstResponder()
            case code5TextField:
                code6TextField.becomeFirstResponder()
            default:
                break
            }
        
        }else if textField.text?.isEmpty == true{
            if textField.text?.isEmpty == true {
                verificationBtn.isUserInteractionEnabled = true
                verificationBtn.tintColor = UIColor(named: "PrimaryBlue")
                
            } else{
                verificationBtn.isUserInteractionEnabled = false
                verificationBtn.tintColor = UIColor(named: "PrimaryFocus")
            }
            switch textField{
            case code2TextField:
                code1TextField.becomeFirstResponder()
            case code3TextField:
                code2TextField.becomeFirstResponder()
            case code4TextField:
                code3TextField.becomeFirstResponder()
            case code5TextField:
                code4TextField.becomeFirstResponder()
            case code6TextField:
                code5TextField.becomeFirstResponder()
            default:
                break
            }
        }
    }
    
    func textFieldSetup(){
        [code1TextField, code2TextField, code3TextField, code4TextField, code5TextField, code6TextField].forEach{
            $0?.addTarget(self, action: #selector(checkTextField), for: .editingChanged)
        }
    }
    
    @objc func checkTextField(){
        var validations: [Bool] = []
        [code1TextField, code2TextField, code3TextField, code4TextField, code5TextField, code6TextField].forEach{
            if let text = $0?.text {
                if !text.isEmpty {
                    validations.append(true)
                }
            }
            if validations.count > 5 {
                verificationBtn.isUserInteractionEnabled = true
                verificationBtn.tintColor = UIColor(named: "PrimaryBlue")
                
            } else{
                verificationBtn.isUserInteractionEnabled = false
                verificationBtn.tintColor = UIColor(named: "PrimaryFocus")
            }
        }
    }
    
    func verificationBtnSetup(){
        verificationBtn.layer.cornerRadius = 8
        verificationBtn.layer.masksToBounds = true
        verificationBtn.tintColor = UIColor(named: "PrimaryFocus")
        verificationBtn.isUserInteractionEnabled = false
    }
    
    func setupInformationLabel() {
        let attributeString = NSMutableAttributedString(string: "Kode berhasil dikirim melalui email \(email), perikasa dan masukkan kode disini untuk dapat melakukan reset password")
        
        attributeString.addAttributes([
            .font: UIFont.systemFont(ofSize: 15)
        ], range: NSRange(location: 0, length: attributeString.length))
        
        attributeString.addAttributes([
            .foregroundColor: UIColor(named: "PrimaryBlue")!
        ], range: NSRange(location: 36, length: email.count))

        informationVerificationLabel.attributedText = attributeString
        informationVerificationLabel.isUserInteractionEnabled = false
    }
    
    func successSendOTPRegister(message:String) {
        let banner = FloatingNotificationBanner(title:"Sukses Request",subtitle:"Kode OTP Sudah terkirim di email, silahkan di cek",style: .success)
        banner.show()
    }
    
    func successOTPVerificationRegister() {
        let banner = FloatingNotificationBanner(title:"Sukses Verifikasi",subtitle:"Kode OTP Sudah diverifikasi, silahkan melanjutkan pendaftaran",style: .success)
        banner.show()
        let vc = CompletingRegisterViewController()
        vc.emailTemp = email
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2){
            let completion = CompletingRegisterViewController()
            completion.emailTemp = self.email
            self.navigationController?.pushViewController(completion, animated: true)
        }
    }
    
    func failed() {
        let banner = FloatingNotificationBanner(title:"Gagal Verifikasi",subtitle:"Silahkan check kode OTP",style: .danger)
        banner.show()
    }
    
    func keyboardSetup(){
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyBoard)))
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func hideKeyBoard(){
        self.view.endEditing(true)
    }
    
    @objc private func keyboardWillHide(){
        bottomVerificationBtnConstraint.constant = 16
    }
    
    @objc private func keyboardWillShow(notification: NSNotification){
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as?
            NSValue {
            let keyboardHeight = keyboardFrame.cgRectValue.height
            bottomVerificationBtnConstraint.constant = keyboardHeight - 16
        }
    }
}

extension VerificationEmailViewController:UITextFieldDelegate{
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        if code6TextField.text?.count == 1{
//            switch textField{
//            case code6TextField:
//                var code = "\(code1TextField.text)\(code2TextField.text)\(code3TextField.text)\(code4TextField.text)\(code5TextField.text)\(code6TextField.text)"
//                print(code)
//                return true
//            default:
//                break
//            }
//        }
//
//        return false
//    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if code6TextField.text?.count == 1{
            return false
        }
        return true
    }
    
    
}

