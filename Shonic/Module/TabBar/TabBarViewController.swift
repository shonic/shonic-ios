//
//  TabBarViewController.swift
//  Shonic
//
//  Created by MEKARI on 03/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {
    var searchTextField:UITextField = UITextField()
    
    var userDefault = UserDefaults.standard
    var indexTab:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setControllers()
        
        self.navigationItem.hidesBackButton = true
        self.delegate = self
        setNavigationBar(tabBarController: 0)
        tabBarController?.selectedIndex = indexTab ?? 0
        //setNavigationBar(tabBarController: self.tabBarController!)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        searchTextField.resignFirstResponder()
    }
    
    func setControllers(){
        
        tabBar.isTranslucent = false
        tabBar.barTintColor = UIColor.white
        tabBar.tintColor = UIColor.blue
        tabBar.shadowImage = UIImage(systemName: "house")
        tabBar.barStyle = .default
        tabBar.backgroundColor = UIColor.white
        let home = setTabController(viewController: HomeViewController(), title: "Home", image: "house")
        let profile = setTabController(viewController: userDefault.string(forKey: "token") != nil ? ProfileUserViewController() : EmptyProfileViewController(), title: "Profile", image:"person")
        let favorite = setTabController(viewController: MyTransactionViewController(), title: "Pesanan Saya", image: "scroll")
        let collection = setTabController(viewController: CollectionViewController(), title: "Collection", image: "square.grid.2x2")
        
        self.setViewControllers([home,collection,favorite,profile], animated: true)
    }
    
    func setTabController(viewController:UIViewController,title:String,image:String)->UIViewController{
        
        viewController.tabBarItem.title = title
        viewController.tabBarItem.image = UIImage(systemName: image)
        return viewController
    }
    
    func setNavigationBar(tabBarController:Int){
        switch tabBarController{
        case 0 :
            searchTextField = UITextField(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width ?? 240 - 48 , height: 32))
            searchTextField.borderStyle = .roundedRect
            searchTextField.placeholder = "Cari Produk"
            self.navigationItem.titleView = searchTextField
            self.navigationItem.rightBarButtonItems = [
                UIBarButtonItem(image: UIImage(systemName: "bell"), style: .plain, target: self, action: #selector(moveCart)),
                UIBarButtonItem(image: UIImage(systemName: "bag"), style: .plain, target: self, action: #selector(moveCart))
            ]
            searchTextField.addTarget(self, action: #selector(navigationSearch), for:.editingDidBegin)
        case 1 : self.navigationItem.titleView = UIView()
            self.navigationController?.navigationBar.prefersLargeTitles = false
            
        case 2 : self.navigationItem.titleView = UIView()
            self.navigationItem.titleView = UIView()
            self.navigationItem.rightBarButtonItems = []
            
        case 3 : self.navigationItem.title = "Akun"
            self.navigationItem.titleView = UIView()
            self.navigationItem.rightBarButtonItems = []
            
        default: break
        }
    }
    
    @objc func navigationSearch(){
        self.navigationController?.pushViewController(SearchingViewController(), animated: true)
    }
    @objc
    func moveCart(){
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension TabBarViewController:UITabBarControllerDelegate,UITextFieldDelegate{
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        self.setNavigationBar(tabBarController: tabBarController.selectedIndex)
    }
}
