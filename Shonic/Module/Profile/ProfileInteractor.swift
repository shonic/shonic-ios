//
//  ProfileInteractor.swift
//  Shonic
//
//  Created by Gramedia on 20/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

protocol ProfileInteractorInput{
    func deleteAccount()
}
protocol ProfileInteractorOutput{
    func delete(data:DeleteResponse)
    func error(message:String)
}

class ProfileInteractor{
    var presenter:ProfileInteractorOutput?
    var worker:ProfileService?
    init(presenter:ProfileInteractorOutput,worker:ProfileService){
        self.presenter = presenter
        self.worker = worker
    }
}

extension ProfileInteractor:ProfileInteractorInput{
    func deleteAccount() {
        self.worker?.delete{callback in
            switch callback{
            case .success(let data): self.presenter?.delete(data: data)
            case .failure(let error): self.presenter?.error(message: "Error")
            }
        }
    }
    
    
}
