//
//  ProfileWorker.swift
//  Shonic
//
//  Created by Gramedia on 20/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation
import Alamofire

protocol ProfileSceneLogic{
    func delete(callback:@escaping(Result<DeleteResponse,AFError>)->Void)
}

class ProfileService{
    let service = ApiClient.shared
    var baseUrl = "http://shonic-test.herokuapp.com"
    let userDefault = UserDefaults.standard
    
}

extension ProfileService:ProfileSceneLogic{
    func delete(callback:@escaping(Result<DeleteResponse,AFError>)->Void){
        let authorization = "Bearer \(userDefault.string(forKey: "token"))"
        let headers:HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization":authorization
        ]
        service.requestApi(urlString: "\(baseUrl)/api/v1/user/delete", method: .delete, header: headers){(response:Result<DeleteResponse,AFError>) in
            switch response{
            case .success(let data):callback(.success(data))
            case .failure(let error):callback(.failure(error))
            }
        }
    }
}
