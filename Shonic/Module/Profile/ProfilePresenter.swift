//
//  ProfilePresenter.swift
//  Shonic
//
//  Created by Gramedia on 20/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

protocol ProfilePresenterOutput{
    func delete(data:DeleteResponse)
    func error(message:String)
}

class ProfilePresenter:ProfileInteractorOutput{
    func delete(data: DeleteResponse) {
        self.delete(data: data)
    }
    
    func error(message: String) {
        self.error(message: message)
    }
    
    var viewController:ProfilePresenterOutput?
    init(viewController:ProfilePresenterOutput) {
        self.viewController = viewController
    }
}
