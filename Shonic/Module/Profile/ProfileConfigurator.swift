//
//  ProfileConfigurator.swift
//  Shonic
//
//  Created by Gramedia on 20/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

enum ProfileConfigurator{
    static func configure(viewController:ProfileUserViewController){
        let presenter =  ProfilePresenter(viewController: viewController)
        let worker = ProfileService()
        let interactor = ProfileInteractor(presenter: presenter, worker: worker)
        viewController.interactor  = interactor
    }
}
