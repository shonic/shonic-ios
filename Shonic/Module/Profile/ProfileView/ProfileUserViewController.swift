//
//  ProfileUserViewController.swift
//  Shonic
//
//  Created by MEKARI on 14/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

enum ProfileUserStructure{
    case info
    case user
    case logout
}

struct MyInfoInterface{
    let name:String
    let iconName:String
    let background:UIColor
}

class ProfileUserViewController: UIViewController {

    @IBOutlet weak var profileTableview: UITableView!
    var interactor:ProfileInteractor?
    var structure:[ProfileUserStructure] = [
        .info,.user,.logout]
    let myInfo:[MyInfoInterface] = [
        MyInfoInterface(name: "Edit Akun", iconName: "highlighter", background: UIColor.systemYellow),
        MyInfoInterface(name: "Pesanan saya", iconName: "scroll", background: .systemGreen),
        MyInfoInterface(name: "Wishlist", iconName: "heart.fill", background: .red),
        MyInfoInterface(name: "Tentang Aplikasi", iconName: "info.circle.fill", background: .blue),
        MyInfoInterface(name: "Hapus Akun", iconName: "person.fill.badge.minus", background: .black)
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileTableview.delegate = self
        profileTableview.dataSource = self
        profileTableview.register(UserTableViewCell.nib(), forCellReuseIdentifier: "UserTableViewCell")
        profileTableview.register(LogoutTableViewCell.nib(), forCellReuseIdentifier: "LogoutTableViewCell")
        profileTableview.register(MyInfoTableViewCell.nib(), forCellReuseIdentifier: "MyInfoTableViewCell")
        profileTableview.separatorInset = UIEdgeInsets(top: 0, left: 72, bottom: 0, right: 0)
        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ProfileUserViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch structure[section]{
            
        case .info:
            return 1
        case .user:
            return 5
        case .logout:
            return 1
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch structure[indexPath.section]{
            
        case .info:
            let cell:UserTableViewCell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell") as! UserTableViewCell
            cell.nameLabel.text = "Chondro Satrio Wibowo"
            cell.emailLabel.text = "chondrosatriowibowo@gmail.com"
            return cell
        case .user:
            let cell:MyInfoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyInfoTableViewCell") as!
            MyInfoTableViewCell
            cell.nameLabel.text = myInfo[indexPath.row].name
            cell.backgroundIconView.backgroundColor = myInfo[indexPath.row].background
            cell.iconImage.image = UIImage(systemName: myInfo[indexPath.row].iconName)
            return cell
        case .logout:
            let cell:LogoutTableViewCell = tableView.dequeueReusableCell(withIdentifier: "LogoutTableViewCell") as! LogoutTableViewCell
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch structure[indexPath.section]{
        case .info:
            break
        case .user:
            switch indexPath.row{
            case 4:
                let alert = UIAlertController(title: "Menghapus Akun", message: "Apakah yakin untuk menutup Akun?", preferredStyle: .alert)
                let closeActions = UIAlertAction(title: "Tidak", style: .cancel)
                let handler = UIAlertAction(title: "Iya", style: .destructive, handler: { _ in
                    
                })
                alert.addAction(closeActions)
                alert.addAction(handler)
                self.present(alert, animated: true)
            default: break
            }
            
        case .logout:
            let alert = UIAlertController(title: "Keluar akun", message: "Apakah yakin untuk menutup Akun?", preferredStyle: .alert)
            let closeActions = UIAlertAction(title: "Tidak", style: .cancel)
            let handler = UIAlertAction(title: "Iya", style: .destructive, handler: { _ in
                let userDefault = UserDefaults.standard
                userDefault.removeObject(forKey: "token")
                let vc = LoginViewController()
                vc.statusCode = "logout"
                self.navigationController?.pushViewController(vc, animated: true)
            })
            alert.addAction(closeActions)
            alert.addAction(handler)
            self.present(alert, animated: true)
        }
    }
    
    
}
extension ProfileUserViewController:ProfilePresenterOutput{
    func delete(data: DeleteResponse) {
        self.navigationController?.pushViewController(LoginViewController(), animated: true)
    }
    
    func error(message: String) {
        
    }
    
    
}
