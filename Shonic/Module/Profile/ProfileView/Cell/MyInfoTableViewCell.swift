//
//  MyInfoTableViewCell.swift
//  Shonic
//
//  Created by MEKARI on 14/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class MyInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var backgroundIconView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundIconView.dropDownUI(borderWidth:0)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    class func nib ()->UINib{
        UINib(nibName: "MyInfoTableViewCell", bundle: nil)
    }
    
}
