//
//  UserTableViewCell.swift
//  Shonic
//
//  Created by MEKARI on 14/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {

    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    class func nib ()-> UINib{
        UINib(nibName: "UserTableViewCell", bundle: nil)
    }
    
}
