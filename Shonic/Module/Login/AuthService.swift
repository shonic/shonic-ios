//
//  AuthService.swift
//  Shonic
//
//  Created by MEKARI on 07/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation
import Alamofire
import Combine

protocol AuthService{
    func resgiter(email:String,password:String, fullName:String, callback:@escaping(Result<CompletingRegisterResponseModel,ErrorMessage>)->Void)
    func addNewPassword(email: String, newPassword: String, token: String, callback: @escaping (Result<AddNewPasswordResponseModel, ErrorMessage>) -> Void)
    func generateTokenRegister(email:String,callback:@escaping(Result<GenerateTokenReponseModel,ErrorMessage>)->Void)
    func generateTokenResetPassword(email:String,callback:@escaping(Result<GenerateTokenReponseModel,ErrorMessage>)->Void)
    func verificationOTPRegister(email:String,token:Int,callback:@escaping(Result<VerificationEmailResponseModel,ErrorMessage>)->Void)
    func verificationOTPResetPassword(email:String,token:Int,callback:@escaping(Result<VerificationEmailResponseModel,ErrorMessage>)->Void)
}

enum ErrorMessage: Error{
    case authorizationFailed
    case failedLogin
    case failedRegister
    case failedGenerateToken
    case error
    case failedGetDataDummy
    case networkError
}

class DefaultAuthService:AuthService{
    var baseUrl = "http://shonic-test.herokuapp.com"
    
    func generateTokenResetPassword(email: String, callback: @escaping (Result<GenerateTokenReponseModel, ErrorMessage>) -> Void) {
        let headers:HTTPHeaders = [
            "Content-Type":"application/json",
            "Accept":"application/json"
        ]
        
        let email = EmailData(email: email)
        
        AF.request("\(baseUrl)/api/v1/forgotpassword/send",method: .post, parameters: email,encoder: JSONParameterEncoder.default,headers: headers).validate(statusCode: 200..<600).responseDecodable(of:GenerateTokenReponseModel.self){response in

            switch response.result{
            case .success(let data):
                if data.status >= 400{
                    callback(.failure(ErrorMessage.failedGenerateToken))
                }else{
                    callback(.success(data))
                }
                
                break
            case .failure(_):
                callback(.failure(ErrorMessage.failedGenerateToken))
                break
            }
        }
    }
    
    func verificationOTPResetPassword(email: String, token: Int, callback: @escaping (Result<VerificationEmailResponseModel, ErrorMessage>) -> Void) {
        let headers:HTTPHeaders = [
            "Content-Type":"application/json",
            "Accept":"application/json"
        ]
        let parameters:VerificationOTPParameter = VerificationOTPParameter(email: email, otp: token)
        AF.request("\(baseUrl)/api/v1/forgotpassword/validate",method: .post,parameters: parameters,encoder: JSONParameterEncoder.default,headers: headers).validate(statusCode: 200..<500).responseDecodable(of:VerificationEmailResponseModel.self){response in
            print(response)
            switch response.result{
            case .success(let data):
                if data.status >= 400{
                    callback(.failure(ErrorMessage.error))
                }else{
                    callback(.success(data))
                }
            case .failure(_):
                callback(.failure(ErrorMessage.error))
            }
        }
    }
    
    func addNewPassword(email: String, newPassword: String, token: String, callback: @escaping (Result<AddNewPasswordResponseModel, ErrorMessage>) -> Void) {
        let addPassword = AddNewPasswordParameter(email: email, newPassword: newPassword, token: token)
        let headers: HTTPHeaders = [
            "Content-Type":"application/json",
            "Accept":"application/json"
        ]
        AF.request("\(baseUrl)/api/v1/forgotpassword/reset_password", method: .post, parameters: addPassword, encoder: JSONParameterEncoder.default, headers: headers).validate(statusCode: 200..<600).responseDecodable(of: AddNewPasswordResponseModel.self) { response in
            print(response.result)
            
            switch response.result {
            case.success(let data):
                print(data.status)
                if data.status >= 400 {
                    callback(.failure(ErrorMessage.authorizationFailed))
                } else {
                    callback(.success(data))
                }
                break
                
            case .failure(_):
                callback(.failure(ErrorMessage.error))
                break
            }
        }
    }
    
    func resgiter(email: String, password: String, fullName: String, callback: @escaping (Result<CompletingRegisterResponseModel, ErrorMessage>) -> Void) {
        let register = CompletingRegisterParameter(email: email, password: password, fullName: fullName)
        let headers:HTTPHeaders = [
            "Content-Type":"application/json",
            "Accept":"application/json"
        ]
        AF.request("http://shonic-test.herokuapp.com/api/v1/auth/register",method: .post,parameters: register,encoder: JSONParameterEncoder.default,headers: headers).validate(statusCode: 200..<600).responseDecodable(of:CompletingRegisterResponseModel.self){ response in
            print(response.result)
            switch response.result{
            case.success(let data):
                print(data.status)
                if data.status >= 400 {
                    callback(.failure(ErrorMessage.authorizationFailed))
                }else{
                    callback(.success(data))
                }
                break
                
                
            case .failure(_):
                callback(.failure(ErrorMessage.failedRegister))
                break
            }
        }
    }
    
    
    func generateTokenRegister(email:String,callback:@escaping(Result<GenerateTokenReponseModel,ErrorMessage>)->Void){
        let headers:HTTPHeaders = [
            "Content-Type":"application/json",
            "Accept":"application/json"
        ]
        
        let email = EmailData(email: email)
        
        AF.request("\(baseUrl)/api/v1/otp/send",method: .post, parameters: email,encoder: JSONParameterEncoder.default,headers: headers).validate(statusCode: 200..<600).responseDecodable(of:GenerateTokenReponseModel.self){response in
            switch response.result{
            case .success(let data):
                if data.status >= 400{
                    callback(.failure(ErrorMessage.failedGenerateToken))
                }else{
                    callback(.success(data))
                }
                
                break
            case .failure(_):
                callback(.failure(ErrorMessage.failedGenerateToken))
                break
            }
        }
    }
    
    
    func verificationOTPRegister(email:String,token:Int,callback:@escaping(Result<VerificationEmailResponseModel,ErrorMessage>)->Void){
        let headers:HTTPHeaders = [
            "Content-Type":"application/json",
            "Accept":"application/json"
        ]
        let parameters:VerificationOTPParameter = VerificationOTPParameter(email: email, otp: token)
        AF.request("\(baseUrl)/api/v1/otp/validate",method: .post,parameters: parameters,encoder: JSONParameterEncoder.default,headers: headers).validate(statusCode: 200..<500).responseDecodable(of:VerificationEmailResponseModel.self){response in
            print(response)
            switch response.result{
            case .success(let data):
                if data.status >= 400{
                    callback(.failure(ErrorMessage.error))
                }else{
                    callback(.success(data))
                }
            case .failure(_):
                callback(.failure(ErrorMessage.error))
            }
        }
    }
}

