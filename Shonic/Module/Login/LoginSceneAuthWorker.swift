//
//  LoginSceneAuthWorker.swift
//  Shonic
//
//  Created by MEKARI on 07/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation
import Combine
import Alamofire

protocol LoginSceneAuthLogic{
    func makeAuth(user:LoginParameter,completion:@escaping(Result<LoginResponseModel,AFError>)-> Void)
}

final class LoginSceneAuthWorker{
    private let service:ApiClient = ApiClient.shared
    let baseUrl = "http://shonic-test.herokuapp.com"
    private var bag = Set<AnyCancellable>()
    
    
    enum LoginSceneAuthWorkerError:Error{
        case authFailed(String)
        case unauthorized
    }
}

extension LoginSceneAuthWorker:LoginSceneAuthLogic{
    func makeAuth(user:LoginParameter,completion: @escaping (Result<LoginResponseModel, AFError>) -> Void) {
        service.requestApi(urlString: "\(baseUrl)/api/v1/auth/login", method: .post, parameters: user){(callback:Result<LoginResponseModel, AFError>) in
            switch callback{
            case .success(let result):
                print(result)
                completion(.success(result))
                break
            case .failure(let error):
                completion(.failure(error))
                break
            }
            
        }
    }
}
