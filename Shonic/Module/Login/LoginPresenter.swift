//
//  LoginPresenter.swift
//  team-d-iOS
//
//  Created by Chondro on 25/05/22.
//

import Foundation

protocol LoginScenePresenterOutput: Any{
    func success(response:LoginResponseModel)
    func failure(message:String)
}

class LoginPresenter:LoginSceneInteractorOutput{
    init(viewController: LoginScenePresenterOutput){
        self.viewController = viewController
    }
    func presentLogin(response: LoginResponseModel) {
        viewController?.success(response: response)
    }
    func presentAlert(message: String) {
        viewController?.failure(message: message)
    }
    //weak var viewController = LoginPresenterOutput?
    var viewController:LoginScenePresenterOutput?
}
