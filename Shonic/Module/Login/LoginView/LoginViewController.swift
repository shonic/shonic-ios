//
//  LoginViewController.swift
//  team-d-iOS
//
//  Created by Chondro on 25/05/22.
//

import UIKit
import NotificationBannerSwift


class LoginViewController: UIViewController,LoginScenePresenterOutput {
    
    var interactor: LoginSceneInteractorInput?
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var googleSignButton: UIButton!
    @IBOutlet weak var emailErrorMessage: UILabel!
    @IBOutlet weak var passwordErrorMessage: UILabel!
    @IBOutlet weak var passwordTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var forgotPasswordTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomLoginBtnConstraint: NSLayoutConstraint!
    @IBOutlet weak var loginBtn: UIButton!
    
    var userDefault = UserDefaults.standard
    var statusCode:String = ""
    init() {
        super.init(nibName: "LoginViewController", bundle: nil)
        LoginSceneConfigurator.configure(viewController: self)
        self.hidesBottomBarWhenPushed = true
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if statusCode == "Profile"{
            
        }else{
            self.navigationItem.hidesBackButton = true
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Profile"
        self.navigationController?.navigationBar.prefersLargeTitles = false
        googleBtnSetup()
//        loginBtnSetup()
        loginFormSetup()
        textFieldSetup()
        passwordTextField.enablePasswordToggle()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Daftar", style: .plain, target: self, action: #selector(navigateRegisterAction))
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Kembali", style: .plain, target: nil, action: nil)
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyBoard)))
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @IBAction func forgotPasswordBtn(_ sender: Any) {
        self.navigationController?.pushViewController(ResetPasswordViewController(), animated: true)
    }
    func failure(message: String) {
        let banner = FloatingNotificationBanner(title:"Error",subtitle:"Email dan/atau password Anda salah",style: .danger)
        banner.show()

    }
    func success(response:LoginResponseModel) {
        if response.status >= 400{
            let banner = FloatingNotificationBanner(title:"\(response.error)",subtitle: "Silahkan  periksa akun anda")
            banner.show()
        }else{
            userDefault.set(response.token!, forKey: "token")
            self.navigationController?.pushViewController(  TabBarViewController(), animated: true)
        }
        
    }
    
    func loginFormSetup(){
        emailErrorMessage.isHidden = true
        passwordErrorMessage.isHidden = true
        
        emailTextField.layer.borderWidth = 1
        emailTextField.layer.cornerRadius = 8
        emailTextField.layer.masksToBounds = true
        
        passwordTextField.layer.borderWidth = 1
        passwordTextField.layer.cornerRadius = 8
        passwordTextField.layer.masksToBounds = true
        
        passwordTopConstraint.constant = 12
        
        emailTextField.layer.borderColor = UIColor.systemGray5.cgColor
        passwordTextField.layer.borderColor = UIColor.systemGray5.cgColor
    
    }
    
    func googleBtnSetup(){
        googleSignButton.configuration?.imagePadding = 8
        googleSignButton.backgroundColor = .clear
        googleSignButton.layer.cornerRadius = 8
        googleSignButton.layer.borderWidth = 1
        googleSignButton.layer.borderColor = UIColor.systemGray5.cgColor
    }
    
//    func loginBtnSetup(){
//        loginBtn.layer.cornerRadius = 8
//        loginBtn.layer.masksToBounds = true
//        loginBtn.tintColor = UIColor(named: "PrimaryFocus")
//        loginBtn.isUserInteractionEnabled = false
//    }
    
    func textFieldSetup(){
        [emailTextField, passwordTextField].forEach{
            $0?.addTarget(self, action: #selector(validateFields), for: .editingDidEnd)
            $0?.addTarget(self, action: #selector(touchTextField(_:)), for: .editingDidBegin)
//            $0?.addTarget(self, action: #selector(checkForValidForm), for: .editingDidEnd)
        }
    }
    
    @objc func checkForValidForm()
    {
        if emailErrorMessage.isHidden && passwordErrorMessage.isHidden && !emailTextField.text!.isEmpty && !passwordTextField.text!.isEmpty
        {
            loginBtn.isUserInteractionEnabled = true
            loginBtn.tintColor = UIColor(named: "PrimaryBlue")
            
        }
        else
        {
            loginBtn.isUserInteractionEnabled = false
            loginBtn.tintColor = UIColor(named: "PrimaryFocus")
        }
    }
    
    func invalidEmail(_ value: String) -> String?
    {
        let reqularExpression = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let predicate = NSPredicate(format: "SELF MATCHES %@", reqularExpression)
        if !predicate.evaluate(with: value)
        {
            return "Format email tidak valid, contoh: shonic@gmail.com"
        }
        
        return nil
    }
    
    func invalidPassword(_ value: String) -> String?
    {
        let password = NSPredicate(format: "SELF MATCHES %@","^(?=.*[a-z])(?=.*[0-9]).{6,}$")
        if !password.evaluate(with: value)
        {
            return "Password min. 6 karakter, terdiri dari angka dan huruf"
        }
        return nil
    }
    
//    private func setup(){
//        let viewController = self
//        let interactorData = LoginSceneInteractor()
//        let presenter = LoginPresenter()
//        let authServive = DefaultAuthService()
//        viewController.interactor = interactorData
//        interactorData.presenter = presenter
//        interactorData.worker = LoginSceneAuthWorker(service: authServive)
//        presenter.viewController = viewController
//    }
    @IBAction func loginAction(_ sender: Any) {
        self.interactor?.tryToLogin(email: emailTextField.text!, password: passwordTextField.text!)
    }
    @objc func navigateRegisterAction(){
        
        self.navigationController?.pushViewController(RegisterViewController(), animated: true)
    }
    
    @objc func validateFields(_ sender: UITextField){
        switch sender {
        case emailTextField:
            if let email = emailTextField.text
            {
                if let error = invalidEmail(email)
                {
                    emailErrorMessage.text = error
                    emailErrorMessage.isHidden = false
                    emailTextField.layer.borderColor = UIColor.systemRed.cgColor
                    passwordTopConstraint.constant = 30

                }
                else
                {
                    emailErrorMessage.isHidden = true
                    emailTextField.layer.borderColor = UIColor.systemGray5.cgColor
                    passwordTopConstraint.constant = 12
                }
            }
            case passwordTextField:
                if let password = passwordTextField.text
                {
                    if let error = invalidPassword(password)
                    {
                        passwordErrorMessage.text = error
                        passwordErrorMessage.isHidden = false
                        passwordTextField.layer.borderColor = UIColor.systemRed.cgColor
                        forgotPasswordTopConstraint.constant = 30

                    }
                    else
                    {
                        passwordErrorMessage.isHidden = true
                        passwordTextField.layer.borderColor = UIColor.systemGray5.cgColor
                        forgotPasswordTopConstraint.constant = 12
                }
            }
        default:
            break
            
        }
    }
    
    @objc func touchTextField(_ sender: UITextField) {
        switch sender {
        case emailTextField:
            emailTextField.layer.borderColor = UIColor.systemBlue.cgColor
            if passwordErrorMessage.isHidden != true {
                passwordTextField.layer.borderColor = UIColor.systemRed.cgColor
            }else {
                passwordTextField.layer.borderColor = UIColor.systemGray5.cgColor
            }
        case passwordTextField:
            if emailErrorMessage.isHidden != true {
                emailTextField.layer.borderColor = UIColor.systemRed.cgColor
            }else {
                emailTextField.layer.borderColor = UIColor.systemGray5.cgColor
            }
            passwordTextField.layer.borderColor = UIColor.systemBlue.cgColor
        default:
            break
        }
    }
    @objc private func hideKeyBoard(){
        self.view.endEditing(true)
    }
    
    @objc private func keyboardWillHide(){
        bottomLoginBtnConstraint.constant = 16
    }
    
    @objc private func keyboardWillShow(notification: NSNotification){
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as?
            NSValue {
            let keyboardHeight = keyboardFrame.cgRectValue.height
            bottomLoginBtnConstraint.constant = keyboardHeight - (keyboardHeight/2) + 36
            
        }
    }

    @IBAction func forgotAction(_ sender: Any) {
        self.navigationController?.pushViewController(ResetPasswordViewController(), animated: true)
    }
        
        
    
    
}

extension LoginViewController:UITextFieldDelegate{
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        return false
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
}
