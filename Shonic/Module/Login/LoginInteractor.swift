//
//  LoginInteractor.swift
//  team-d-iOS
//
//  Created by Chondro on 25/05/22.
//

import Foundation
import Alamofire



protocol LoginSceneInteractorInput:AnyObject{
    func tryToLogin(email:String,password:String)
}

protocol LoginSceneInteractorOutput:Any{
    func presentLogin(response:LoginResponseModel)
    func presentAlert(message:String)
}

final class LoginSceneInteractor{
    init(presenter:LoginSceneInteractorOutput , worker: LoginSceneAuthWorker){
        self.presenter = presenter
        self.worker = worker
    }
    var presenter:LoginSceneInteractorOutput?
    var worker:LoginSceneAuthWorker?
}

extension LoginSceneInteractor:LoginSceneInteractorInput{
    func tryToLogin(email: String, password: String) {
        let login = LoginParameter(email: email, password: password)
        print(login)
        worker?.makeAuth(user: login){ result in
            print(result)
            DispatchQueue.main.async {
                [weak self] in
                switch result{
                case .success(let data):
                    self?.presenter?.presentLogin(response: data)
                case .failure(_):
                    self?.presenter?.presentAlert(message: "Error Email / Password")
                }
            }
        }
    }
}
