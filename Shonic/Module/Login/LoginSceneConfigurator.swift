//
//  LoginSceneConfigurator.swift
//  Shonic
//
//  Created by Dzaki Izza on 10/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

enum LoginSceneConfigurator {
  static func configure(viewController: LoginViewController) {
    let presenter = LoginPresenter(viewController: viewController)
    let worker = LoginSceneAuthWorker()
    let interactor = LoginSceneInteractor(presenter: presenter, worker: worker)

    viewController.interactor = interactor
  }
}
