//
//  LoginResponseModel.swift
//  Shonic
//
//  Created by MEKARI on 07/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

class LoginResponseModel:Decodable{
    var token:String?
    var status:Int
    var message:String?
    var error:String?
    var path:String?
}

class LoginParameter:Codable{
    var email:String
    var password:String
    
    init(email:String,password:String) {
        self.email = email
        self.password = password
    }
}

class LoginFailedResponseModel:Decodable{
    var message:String
    var status:Int
    var timeStamp:String
    var error:String
}
