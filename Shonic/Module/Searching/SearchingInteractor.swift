//
//  SearchingInteractor.swift
//  Shonic
//
//  Created by MEKARI on 18/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation
import RxSwift

protocol SearchSceneInteractorInput:AnyObject{
    func searchProduct(key:String)
    func getSearchFilterProduct(keySearch:String,pageNo:Int,pageSize:Int,minPrice:Int,maxPrice:Int,onlyDiscount:Bool,filter4Star:Bool)
    func getSearchSortProrduct(keySearch:String,pageNo:Int,pageSize:Int,sort_dateDesc:Bool,sort_priceAsc:Bool,sort_priceDesc:Bool)
}
protocol SearchSceneInteractorOutput:Any{
    func getProductSearch(data:SearchProductResponse)
    func failureGetData(message:ErrorMessage)
    
}
final class SearchingInteractor{
    init(presenter:SearchSceneInteractorOutput,worker:SearchSceneWorker){
        self.presenter = presenter
        self.worker = worker
    }
    var presenter:SearchSceneInteractorOutput?
    var worker:SearchSceneWorker?
}

extension SearchingInteractor:SearchSceneInteractorInput{
    
    func getSearchFilterProduct(keySearch: String, pageNo: Int, pageSize: Int, minPrice: Int, maxPrice: Int, onlyDiscount: Bool, filter4Star: Bool) {
        self.worker?.getSearchFilterProduct(keySearch: keySearch, pageNo: pageNo, pageSize: pageSize, minPrice: minPrice, maxPrice: maxPrice, onlyDiscount: onlyDiscount, filter4Star: filter4Star){result in
            switch result{
            case .success(let data):
                self.presenter?.getProductSearch(data: data)
            case .failure(let message):
                self.presenter?.failureGetData(message: message)
            }
        }
    }
    
    func getSearchSortProrduct(keySearch: String, pageNo: Int, pageSize: Int, sort_dateDesc: Bool, sort_priceAsc: Bool, sort_priceDesc: Bool) {
        self.worker?.getSearchSortProrduct(keySearch: keySearch, pageNo: pageNo, pageSize: pageSize, sort_dateDesc: sort_dateDesc, sort_priceAsc: sort_priceAsc, sort_priceDesc: sort_priceDesc){result in
            switch result{
            case .success(let data):
                self.presenter?.getProductSearch(data: data)
            case .failure(let message):
                self.presenter?.failureGetData(message: message)
            }
        }
    }
    
    func searchProduct(key: String) {
        self.worker?.getDataSearch(key: key){(result) in
            DispatchQueue.main.async {
                switch result{
                case .success(let data):
                    self.presenter?.getProductSearch(data: data)
                case .failure(let message):
                    self.presenter?.failureGetData(message: message)
                }
                
            }
        }
    }
}
