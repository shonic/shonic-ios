//
//  SearchSceneWorker.swift
//  Shonic
//
//  Created by Gramedia on 02/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

protocol SearchSceneLogic:AnyObject{
    func getDataSearch(key:String,completion:@escaping(Result<SearchProductResponse,ErrorMessage>)->Void)
    func getSearchFilterProduct(keySearch:String,pageNo:Int,pageSize:Int,minPrice:Int,maxPrice:Int,onlyDiscount:Bool,filter4Star:Bool,completion:@escaping(Result<SearchProductResponse,ErrorMessage>)->Void)
    func getSearchSortProrduct(keySearch:String,pageNo:Int,pageSize:Int,sort_dateDesc:Bool,sort_priceAsc:Bool,sort_priceDesc:Bool,completion:@escaping(Result<SearchProductResponse,ErrorMessage>)->Void)
    
}
class SearchSceneWorker{
    private let service:ProductService?
    init(service:ProductService){
        self.service = service
    }
}

extension SearchSceneWorker:SearchSceneLogic{
    func getSearchFilterProduct(keySearch: String, pageNo: Int, pageSize: Int, minPrice: Int, maxPrice: Int, onlyDiscount: Bool, filter4Star: Bool, completion: @escaping (Result<SearchProductResponse, ErrorMessage>) -> Void) {
        service?.getSearchFilterProduct(keySearch: keySearch, pageNo: pageNo, pageSize: pageSize, minPrice: minPrice, maxPrice: maxPrice, onlyDiscount: onlyDiscount, filter4Star: filter4Star){callback in
            switch callback{
            case .success(let data):
                completion(.success(data))
            case .failure(let message):
                completion(.failure(message))
            }
        }
    }
    
    func getSearchSortProrduct(keySearch: String, pageNo: Int, pageSize: Int, sort_dateDesc: Bool, sort_priceAsc: Bool, sort_priceDesc: Bool, completion: @escaping (Result<SearchProductResponse, ErrorMessage>) -> Void) {
        service?.getSearchSortProrduct(keySearch: keySearch, pageNo: pageNo, pageSize: pageSize, sort_dateDesc: sort_dateDesc, sort_priceAsc: sort_priceAsc, sort_priceDesc: sort_priceDesc){callback in
            switch callback{
            case .success(let data):
                completion(.success(data))
            case .failure(let message):
                completion(.failure(message))
            }
        }
    }
    
    
    
    func getDataSearch(key: String, completion: @escaping (Result<SearchProductResponse, ErrorMessage>) -> Void) {
        service?.getSearchProduct(keySearch: key, pageNo: 1, pageSize: 10){(callback) in
            switch callback{
            case .success(let data):
                completion(.success(data))
            case .failure(let message):
                completion(.failure(message))
            }
        }
    }
    
    
}
