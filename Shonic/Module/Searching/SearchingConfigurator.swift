//
//  SearchingConfigurator.swift
//  Shonic
//
//  Created by Gramedia on 02/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

enum SearchingConfigurator{
    static func configure(viewController:SearchingViewController){
        let presenter = SearchingPresenter(viewcontroller: viewController)
        let service = DefaultProductService()
        let worker = SearchSceneWorker(service: service)
        let interactor = SearchingInteractor(presenter: presenter, worker: worker)
        viewController.interactor = interactor
    }
}
