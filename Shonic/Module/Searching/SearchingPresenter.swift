//
//  SearchingPresenter.swift
//  Shonic
//
//  Created by Gramedia on 02/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

protocol SearchingPresenterOutput:Any{
    func getSearchData(data:SearchProductResponse)
    func getFailureMessage(message:String)
}

class SearchingPresenter:SearchSceneInteractorOutput{
    var viewController:SearchingPresenterOutput?
    init(viewcontroller:SearchingPresenterOutput) {
        self.viewController = viewcontroller
    }
    func getProductSearch(data:SearchProductResponse) {
        viewController?.getSearchData(data: data)
    }
    
    func failureGetData(message: ErrorMessage) {
        switch message {
        case .failedGetDataDummy:
            viewController?.getFailureMessage(message: "Error get data message")
        case .networkError:
            viewController?.getFailureMessage(message: "Network error")
        default:
            viewController?.getFailureMessage(message: "Unknown error")
        }
    }
    
    
}
