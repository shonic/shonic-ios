//
//  SortTableViewCell.swift
//  Shonic
//
//  Created by MEKARI on 13/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class SortTableViewCell: UITableViewCell {

    @IBOutlet weak var informatianLabel: UILabel!
    @IBOutlet weak var indicatorImage: UIImageView!
    override var isSelected: Bool{
        didSet{
            indicatorImage.image =  isSelected ? UIImage(systemName: "circle.circle.fill" ) : UIImage(systemName: "circle")
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        indicatorImage.image = isSelected ? UIImage(systemName: "circle.circle.fill" ) : UIImage(systemName: "circle")
        // Initialization code
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        indicatorImage.image = selected ? UIImage(systemName: "circle.circle.fill" ) : UIImage(systemName: "circle")
        // Configure the view for the selected state
    }
    
    class func nib ()->UINib{
        UINib(nibName: "SortTableViewCell", bundle: nil)
    }
    
    
        
    
    
}
