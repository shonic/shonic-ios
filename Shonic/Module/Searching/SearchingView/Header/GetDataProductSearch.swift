//
//  GetDataProductSearch.swift
//  Shonic
//
//  Created by Gramedia on 02/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation


// MARK: - SearchProductResponse
struct SearchProductResponse: Codable {
    var status: Int?
    var message: String?
    var data: DataClass?
    var error: String?
}

// MARK: - DataClass
struct DataClass: Codable {
    var product: [Product]?
    var found: Bool?
}

// MARK: - Product
struct Product: Codable {
    var id, createdAt: String?
    var image: String?
    var name: String?
    var price, qty, discount: Int?
    var rating: Double
    var brand: Brand?
    var category: CategoryData?
    var review: Int?
    var after_discount:Int?
}

// MARK: - Brand
struct Brand: Codable {
    var id: Int?
    var name: String?
}

// MARK: - Category
struct CategoryData: Codable {
    var categoryID: Int?
    var name: String?
    var categoryParent: Brand?

    enum CodingKeys: String, CodingKey {
        case categoryID
        case name
        case categoryParent
    }
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public func hash(into hasher: inout Hasher) {
        // No-op
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
