//
//  GetDataDetailProduct.swift
//  Shonic
//
//  Created by Gramedia on 02/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

struct GetDataDetailProductResponse:Codable{
    let status:Int
    let message:String
    let data:DetailProduct
    let error:String?
}

struct DetailProduct:Codable{
    let id:String
    let name:String
    let price:Int
    let qty:Int
    let description:String?
    let discount:Int?
    let category:CategoryData?
    let brand:Brand
    let flashsale:String?
    let image:String?
    let rating:Float?
    let review:Int?
    let createdAt:String?
    //let ratingList:[String]?
}
