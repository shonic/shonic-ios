//
//  FilterAndSortCollectionReusableView.swift
//  Shonic
//
//  Created by Gramedia on 02/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class FilterAndSortCollectionReusableView: UICollectionReusableView {
    
    var actionFilterHandler:(()->Void)?
    var actionSortHandler:(()->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func filterAction(_ sender: Any) {
        actionFilterHandler!()
    }
    @IBAction func sortAction(_ sender: Any) {
        actionSortHandler!()
    }
    
    class func nib()->UINib{
        UINib(nibName: "FilterAndSortCollectionReusableView", bundle: nil)
    }
}
