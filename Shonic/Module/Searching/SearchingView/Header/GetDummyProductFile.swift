//
//  GetDummyProductFile.swift
//  Shonic
//
//  Created by Gramedia on 02/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

struct GetDummyProducResponse:Codable{
    let status:Int
    let message:String
    let data:[ProductData]
}

struct ProductData:Codable{
    let id:String
    let name:String
    let createdAt:String
    let image:String
    let price:Int
    let qty:Int
    let description:String
    let discount:String
    let rating:Float
    let brand:Brand
    let categories:Categories
}

//struct Brand:Codable{
//    let id:String?
//    let name:String?
//}

struct Categories:Codable{
    let id:String
    let name:String
}
