//
//  HeaderRecomendationCollectionReusableView.swift
//  Shonic
//
//  Created by Gramedia on 02/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class HeaderRecomendationCollectionReusableView: UICollectionReusableView {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    class func nib()->UINib{
        UINib(nibName: "HeaderRecomendationCollectionReusableView", bundle: nil)
    }
}
