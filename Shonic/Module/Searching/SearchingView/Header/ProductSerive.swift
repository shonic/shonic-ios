//
//  ProductSerive.swift
//  Shonic
//
//  Created by Gramedia on 02/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation
import Alamofire

protocol ProductService{
    func getSearchProduct(keySearch:String,pageNo:Int,pageSize:Int,callback:@escaping (Result<SearchProductResponse,ErrorMessage>)->Void)
    func getDetailProduct(id:String,callback:@escaping (Result<GetDataDetailProductResponse,ErrorMessage>)->Void)
    func getSimiliarProduct(id:String,callback:@escaping(Result<GetAllLastestProductResponse,ErrorMessage>)->Void)
    func getAllLastestProduct(noPage:Int,pageSize:Int,callback:@escaping(Result<GetAllLastestProductResponse,ErrorMessage>)->Void)
    func getAllByCategory(nameCategory:String,callback:@escaping(Result<SearchProductResponse,ErrorMessage>)->Void)
    func getAllBrand(callback:@escaping(Result<GetAllBrandResponse,ErrorMessage>)->Void)
    func getSearchFilterProduct(keySearch:String,pageNo:Int,pageSize:Int,minPrice:Int,maxPrice:Int,onlyDiscount:Bool,filter4Star:Bool,callback:@escaping(Result<SearchProductResponse,ErrorMessage>)->Void)
    func getSearchSortProrduct(keySearch:String,pageNo:Int,pageSize:Int,sort_dateDesc:Bool,sort_priceAsc:Bool,sort_priceDesc:Bool,callback:@escaping(Result<SearchProductResponse,ErrorMessage>)->Void)
     
}
class DefaultProductService:ProductService{
    func getSearchSortProrduct(keySearch:String,pageNo:Int,pageSize:Int,sort_dateDesc:Bool,sort_priceAsc:Bool,sort_priceDesc:Bool,callback:@escaping(Result<SearchProductResponse,ErrorMessage>)->Void){
        AF.request("\(baseUrl)/api/v1/product/search?keyword=\(keySearch)&pageNo=\(pageNo)&pageSize=\(pageSize)&sort_dateDesc=\(sort_dateDesc)&sort_priceAsc=\(sort_priceAsc)&sort_priceDesc=\(sort_priceDesc)",method: .get).validate(statusCode: 200..<600).responseDecodable(of:SearchProductResponse.self){response in
            switch response.result{
            case .success(let dataResponse):
                if dataResponse.status!  >= 400 {
                    callback(.failure(ErrorMessage.failedGetDataDummy))
                }else{
                    callback(.success(dataResponse))
                }
            case .failure(let error):
                print(error)
                callback(.failure(ErrorMessage.networkError))
            }
        }
    }
    func getSearchFilterProduct(keySearch: String, pageNo: Int, pageSize: Int, minPrice: Int, maxPrice: Int, onlyDiscount: Bool, filter4Star: Bool, callback: @escaping (Result<SearchProductResponse, ErrorMessage>) -> Void) {
        print(minPrice)
        print(maxPrice)
        print(onlyDiscount)
        print(filter4Star)
        AF.request("\(baseUrl)/api/v1/product/search?filter_4Star=\(filter4Star)&filter_maxPrice=\(maxPrice)&filter_minPrice=\(minPrice)&filter_onlyDiscount=\(onlyDiscount)&keyword=\(keySearch)&pageNo=\(pageNo)&pageSize=\(pageSize)",method: .get).validate(statusCode: 200..<600).responseDecodable(of:SearchProductResponse.self){response in
            print(response.result)
            switch response.result{
            case .success(let dataResponse):
                if dataResponse.status!  >= 400 {
                    callback(.failure(ErrorMessage.failedGetDataDummy))
                }else{
                    callback(.success(dataResponse))
                }
            case .failure(let error):
                print(error)
                callback(.failure(ErrorMessage.networkError))
            }
        }
    }
    

    
    var baseUrl = "http://shonic-test.herokuapp.com"
    func getAllBrand(callback: @escaping (Result<GetAllBrandResponse, ErrorMessage>) -> Void) {
        AF.request("\(baseUrl)/api/v1/brand/getAllBrand",method: .get).validate(statusCode: 200..<600).responseDecodable(of:GetAllBrandResponse.self){response in
            switch response.result{
            case .success(let data):
                if data.status >= 400{
                    callback(.failure(ErrorMessage.failedGetDataDummy))
                }else{
                    callback(.success(data))
                }
            case .failure(let message):
                callback(.failure(ErrorMessage.networkError))
            }
        }
            
    }
    
    
    func getAllLastestProduct(noPage: Int, pageSize: Int, callback: @escaping (Result<GetAllLastestProductResponse, ErrorMessage>) -> Void) {
        AF.request("\(baseUrl)/api/v1/product/getAllLatestProduct").validate(statusCode: 200..<600).responseDecodable(of:GetAllLastestProductResponse.self){response in
            switch response.result{
            case .success(let data):
                if data.status >= 400{
                    callback(.failure(ErrorMessage.failedGetDataDummy))
                }else{
                    callback(.success(data))
                }
            case .failure(let message):
                callback(.failure(ErrorMessage.networkError))
            }
        }
    }
    
    func getAllByCategory(nameCategory: String, callback: @escaping (Result<SearchProductResponse, ErrorMessage>) -> Void) {
        
    }
    
    func getSimiliarProduct(id: String, callback: @escaping (Result<GetAllLastestProductResponse, ErrorMessage>) -> Void) {
        AF.request("\(baseUrl)/api/v1/product/simmilar/\(id)").validate(statusCode: 200..<600).responseDecodable(of:GetAllLastestProductResponse.self){response in
            print(response.result)
            switch response.result{
               
            case .success(let data):
                if data.status >= 400{
                    callback(.failure(ErrorMessage.failedGetDataDummy))
                }else{
                    callback(.success(data))
                }
            case .failure(let message):
                callback(.failure(ErrorMessage.networkError))
            }
        }
    }
    

    
    
    func getSearchProduct(keySearch: String, pageNo: Int, pageSize: Int, callback: @escaping (Result<SearchProductResponse, ErrorMessage>) -> Void) {
        AF.request("\(baseUrl)/api/v1/product/search?keyword=\(keySearch)&pageNo=\(pageNo)&pageSize=\(pageSize)",method: .get).validate(statusCode: 200..<600).responseDecodable(of:SearchProductResponse.self){response in
            switch response.result{
            case .success(let dataResponse):
                if dataResponse.status!  >= 400 {
                    callback(.failure(ErrorMessage.failedGetDataDummy))
                }else{
                    callback(.success(dataResponse))
                }
            case .failure(let error):
                print(error)
                callback(.failure(ErrorMessage.networkError))
            }
        }
        
    }
    
    func getDetailProduct(id: String, callback: @escaping (Result<GetDataDetailProductResponse, ErrorMessage>) -> Void) {
        AF.request("\(baseUrl)/api/v1/product/getById/\(id)",method: .get).validate(statusCode: 200..<600).responseDecodable(of:GetDataDetailProductResponse.self){response in
            switch response.result{
            case .success(let dataResponse):
                if dataResponse.status >= 400{
                    callback(.failure(ErrorMessage.failedGetDataDummy))
                }else{
                    print(dataResponse)
                    callback(.success(dataResponse))
                }
            case .failure(_):
                callback(.failure(ErrorMessage.networkError))
            }
        }
    }
    
    
    
}
