//
//  ZeroCollectionViewCell.swift
//  Shonic
//
//  Created by Gramedia on 04/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class ZeroCollectionViewCell: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    class func nib ()-> UINib{
        UINib(nibName: "ZeroCollectionViewCell", bundle: nil)
    }

}
