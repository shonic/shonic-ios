//
//  SearchCollectionViewCell.swift
//  Shonic
//
//  Created by Gramedia on 02/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit
import SDWebImage

class SearchCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var searchUIView: UIView!
    
    @IBOutlet weak var imageProduct: UIImageView!
    
    @IBOutlet weak var priceProduct: UILabel!
    @IBOutlet weak var titleProduct: UILabel!
    
    var product:Product?{
        didSet{
            if let product = product{
                self.configureUI(image: product.image ?? "", title: product.name ?? "", price: product.price ?? 0)
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        searchUIView.shadow(alpha: 0.5, opacity: 0.3, shadowOffset: CGSize(width: 0.5, height: 0.5), shadowRadius: 5.0, cornerRadius: 5.0)
        // Initialization code
    }
    
    func configureUI(image:String,title:String,price:Int){
        print(title)
        print(price)
        var url = URL(string: image)
        imageProduct.sd_setImage(with: url, placeholderImage: UIImage(systemName: "gearshape.fill"))
        titleProduct.text = title
        priceProduct.text = "Rp \(price)"
    }
    
    class func nib()-> UINib{
        UINib(nibName: "SearchCollectionViewCell", bundle: nil)
    }

}
