//
//  FilterButtonCollectionViewCell.swift
//  Shonic
//
//  Created by MEKARI on 08/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class FilterButtonCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bakcgroundButtonView: UIView!
    
    
    @IBOutlet weak var labelInformation: UILabel!
    var title:String?{
        didSet{
            if let title = title {
                labelInformation.text = title
            }
        }
    }
    override var isSelected: Bool{
        didSet{
            self.bakcgroundButtonView.backgroundColor = isSelected ? UIColor(named: "PrimaryFocus") : UIColor.white
            self.labelInformation.textColor = isSelected ? UIColor.blue : UIColor.black
            self.bakcgroundButtonView.layer.borderWidth = isSelected ? 0.0 : 1.0
            self.bakcgroundButtonView.layer.borderColor = isSelected ? UIColor.white.cgColor : UIColor.black.cgColor
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.bakcgroundButtonView.backgroundColor = UIColor.white
        self.labelInformation.textColor = isSelected ? UIColor.blue : UIColor.black
        self.dropDownUI()
        // Initialization code
    }
    
    class func nib()-> UINib{
        UINib(nibName: "FilterButtonCollectionViewCell", bundle: nil)
    }

}
