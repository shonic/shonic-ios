//
//  SortFilterViewController.swift
//  Shonic
//
//  Created by MEKARI on 12/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class SortFilterViewController: UIViewController {

    @IBOutlet weak var sortTableView: UITableView!
    
    var actionHandler:((Int)->Void)?
    var closeActionHandler:(()->Void)?
    var sortAsArray:[String] = [
        "Paling Relevan",
        "Produk Terbaru",
        "Harga Terendah",
        "Harga Tertinggi"
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        sortTableView.delegate = self
        sortTableView.dataSource = self
        sortTableView.register(SortTableViewCell.nib(), forCellReuseIdentifier: "SortTableViewCell")
        // Do any additional setup after loading the view.
    }
    @IBAction func closeAction(_ sender: Any) {
        closeActionHandler!()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SortFilterViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        sortAsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:SortTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SortTableViewCell") as! SortTableViewCell
        cell.informatianLabel.text = sortAsArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        actionHandler!(indexPath.row)
    }
    
}
