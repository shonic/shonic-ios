//
//  FilterSearchProductViewController.swift
//  Shonic
//
//  Created by MEKARI on 08/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

enum FilterType{
    case price
    case rating
    case discount
}
class FilterSearchProductViewController: UIViewController {

    @IBOutlet weak var priceCollectionView: UICollectionView!
    @IBOutlet weak var isDiscountButton: UIButton!
    @IBOutlet weak var is4startButton: UIButton!
    
    
    var structureType:[FilterType] = [
        .price,.discount,.rating]
    var isDiscount:Bool = false
    var is4start:Bool = false
    var priceMin:Int = 0
    var priceMax:Int = 100000000
    var arrayPrice:[String] = [
        "< Rp 100.000",
        "Rp 100 rb - Rp 500 rb",
        "Rp 500 rb - Rp 1.5 jt",
        "Rp 1.5 jt - Rp 5.5 jt",
        "Rp 5.5 jt - Rp 10 jt",
        "Rp 10.000.000 >"
    ]
    
    var actionHandler:((Bool,Bool,Int,Int)->Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        priceCollectionView.delegate = self
        priceCollectionView.dataSource = self
        priceCollectionView.register(FilterButtonCollectionViewCell.nib(), forCellWithReuseIdentifier: "FilterButtonCollectionViewCell")
        let customLayout = CustomViewFlowLayout()
        if #available(iOS 10.0, *) {
            customLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        } else {
            customLayout.estimatedItemSize = CGSize(width: 41, height: 41)
        }
        priceCollectionView.collectionViewLayout = customLayout
        
        isDiscountButton.backgroundColor = isDiscount ? UIColor(named: "PrimaryFocus") : UIColor.white
        isDiscountButton.titleLabel?.textColor = isDiscount ? UIColor.blue : UIColor.black
        is4startButton.backgroundColor = is4start ? UIColor(named: "PrimaryFocus") : UIColor.white
        is4startButton.titleLabel?.textColor = isDiscount ? UIColor.blue : UIColor.black
        // Do any additional setup after loading the view.
    }
    @IBAction func isDiscountAction(_ sender: Any) {
        isDiscount.toggle()
    }
    @IBAction func is4StarAction(_ sender: Any) {
        is4start.toggle()
    }
    @IBAction func filterAction(_ sender: Any) {
        actionHandler!(isDiscount,is4start,priceMin,priceMax)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FilterSearchProductViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func textSize(font: UIFont, text: String, width: CGFloat = .greatestFiniteMagnitude, height: CGFloat = .greatestFiniteMagnitude) -> CGSize {
           let label = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: height))
           label.numberOfLines = 0
           label.font = font
           label.text = text
           label.sizeToFit()
           return label.frame.size
       }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.01
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch structureType[indexPath.section]{

        case .price:
            var size = self.textSize(font: UIFont.systemFont(ofSize: 12.0), text: arrayPrice[indexPath.row])
            size.width += 8.0
            size.height += 8.0
            return CGSize(width: size.width, height: size.height)
        case .rating:
            let width = UIScreen.main.bounds.size.width / 4
            let height = 40.0
            return CGSize(width: width, height: height)
        case .discount:
            let width = UIScreen.main.bounds.size.width / 4
            let height = 40.0
            return CGSize(width: width, height: height)
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch structureType[indexPath.section]{
            
        case .price:
            let cell: FilterButtonCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterButtonCollectionViewCell", for: indexPath) as! FilterButtonCollectionViewCell
            cell.title = arrayPrice[indexPath.row]
            return cell
        case .rating:
            let cell: FilterButtonCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterButtonCollectionViewCell", for: indexPath) as! FilterButtonCollectionViewCell
            return cell
        case .discount:
            let cell: FilterButtonCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterButtonCollectionViewCell", for: indexPath) as! FilterButtonCollectionViewCell
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch structureType[indexPath.section]{
        case .price:
            switch indexPath.row{
            case 0:
                priceMin = 0
                priceMax = 100000
                break
            case 1:
                priceMin = 100000
                priceMax = 500000
                break
            case 2:
                priceMin = 500000
                priceMax = 1500000
                break
            case 3:
                priceMin = 1500000
                priceMax = 5000000
            case 4:
                priceMin = 5000000
                priceMax = 10000000
            default:
                priceMin = 10000000
                priceMax = 999999999
            }
        default :
            break
        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        structureType.count
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch structureType[section]{
        case .price :
            return arrayPrice.count
        case .rating :
            return 0
        case .discount:
            return 0
        }
    }
}

class CustomViewFlowLayout: UICollectionViewFlowLayout {
    let cellSpacing: CGFloat = 10

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        self.minimumLineSpacing = 10.0
        self.sectionInset = UIEdgeInsets(top: 12.0, left: 16.0, bottom: 0.0, right: 16.0)
        let attributes = super.layoutAttributesForElements(in: rect)

        var leftMargin = sectionInset.left
        var maxY: CGFloat = -1.0
        attributes?.forEach { layoutAttribute in
            if layoutAttribute.frame.origin.y >= maxY {
                leftMargin = sectionInset.left
            }
            layoutAttribute.frame.origin.x = leftMargin
            leftMargin += layoutAttribute.frame.width + cellSpacing
            maxY = max(layoutAttribute.frame.maxY, maxY)
        }
        return attributes
    }
}
