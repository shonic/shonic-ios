//
//  SearchingViewController.swift
//  Shonic
//
//  Created by MEKARI on 18/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit
import FloatingPanel
import Alamofire
import NotificationBannerSwift

class SearchingViewController: UIViewController ,FloatingPanelControllerDelegate{

    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    var fpc:FloatingPanelController!
    
    var searchTextField = UITextField()
    
    var interactor:SearchSceneInteractorInput?
    
    var dataProductList:[Product] = []
    
    var isFound = false
    let formatter = NumberFormatter()
    init() {
        super.init(nibName: "SearchingViewController", bundle: nil)
        SearchingConfigurator.configure(viewController: self)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewWillAppear(_ animated: Bool) {
        
        searchTextField.becomeFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backButtonTitle = "Kembali"
        searchTextField = UITextField(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width * 0.75,height: 32))
        searchTextField.placeholder = "Cari"
        searchTextField.borderStyle = .roundedRect
        self.navigationItem.titleView = searchTextField
        collectionView.delegate = self
        searchTextField.delegate = self
        collectionView.dataSource = self
        collectionView.register(ZeroCollectionViewCell.nib(), forCellWithReuseIdentifier: "ZeroCollectionViewCell")
        collectionView.register(EmptyCollectionViewCell.nib(), forCellWithReuseIdentifier: "EmptyResultProduct")
        collectionView.register(HeaderRecomendationCollectionReusableView.nib(), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderRecomendation")
        collectionView.register(RecommendationCollectionViewCell.nib(), forCellWithReuseIdentifier: "RecomendationCollectionCell")
        collectionView.register(EmptyCollectionReusableView.nib(), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "EmptySearchHeader")
        collectionView.register(FilterAndSortCollectionReusableView.nib(), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "FilterAndSortHeader")
        collectionView.register(SearchCollectionViewCell.nib(), forCellWithReuseIdentifier: "SearchProductCell")
        let layout = UICollectionViewFlowLayout()
        layout.sectionHeadersPinToVisibleBounds = true
        collectionView.collectionViewLayout = layout
        fpc = FloatingPanelController(delegate: self)
        fpc.delegate = self
        fpc.layout = HideFilter()
        fpc.hide()
        fpc.addPanel(toParent: self)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SearchingViewController:UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if dataProductList.isEmpty && isFound == false{
            return 1
        }else if dataProductList.count > 0 && isFound == false{
            return 2
        }
        else{
            return 1
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if dataProductList.isEmpty && isFound == false {
            return 1
        }else if dataProductList.count > 0 && isFound == false{
            switch section{
            case 0 : return 1
            default: return dataProductList.count
                
            }
        }
        else {
            return dataProductList.count
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if dataProductList.isEmpty && isFound == false{
            let header:EmptyCollectionReusableView =
            collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "EmptySearchHeader", for: indexPath) as! EmptyCollectionReusableView
            return header
        }else if isFound == false && dataProductList.count > 0{
            switch indexPath.section{
            case 1:
                let header:HeaderRecomendationCollectionReusableView = (collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderRecomendation", for: indexPath) as? HeaderRecomendationCollectionReusableView)!
                return header
            default:
                let header:EmptyCollectionReusableView =
                collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "EmptySearchHeader", for: indexPath) as! EmptyCollectionReusableView
                return header
            }
        }else{
            let header:FilterAndSortCollectionReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FilterAndSortHeader", for: indexPath) as! FilterAndSortCollectionReusableView
            header.actionFilterHandler = {
                let controller = FilterSearchProductViewController()
                controller.actionHandler = { (isDiscount,is4Star,priceMin,priceMax) in
                    self.interactor?.getSearchFilterProduct(keySearch: self.searchTextField.text!, pageNo: 1, pageSize: 10, minPrice: priceMin, maxPrice: priceMax, onlyDiscount: isDiscount, filter4Star: is4Star)
                    self.fpc.layout = HideFilter()
                    self.fpc.hide()
                }
                self.fpc.set(contentViewController: controller)
                self.fpc.layout = ShowFilter()
                self.fpc.show()
                
            }
            header.actionSortHandler = {
                let controller = SortFilterViewController()
                controller.actionHandler = { index in
                    switch index{
                    case 0:
                        self.interactor?.getSearchSortProrduct(keySearch: self.searchTextField.text!, pageNo: 1, pageSize: 10, sort_dateDesc: false, sort_priceAsc: false, sort_priceDesc: false)
                        
                    case 1:
                        self.interactor?.getSearchSortProrduct(keySearch: self.searchTextField.text!, pageNo: 1, pageSize: 10, sort_dateDesc: true, sort_priceAsc: false, sort_priceDesc: false)
                    case 2:
                        self.interactor?.getSearchSortProrduct(keySearch: self.searchTextField.text!, pageNo: 1, pageSize: 10, sort_dateDesc: false, sort_priceAsc: true, sort_priceDesc: false)
                    default:
                        self.interactor?.getSearchSortProrduct(keySearch: self.searchTextField.text!, pageNo: 1, pageSize: 10, sort_dateDesc: false, sort_priceAsc: false, sort_priceDesc: true)
                    }
                }
                controller.closeActionHandler = {  
                    self.fpc.layout = HideFilter()
                    self.fpc.hide()
                }
                self.fpc.set(contentViewController: controller)
                self.fpc.layout = ShowFilter()
                self.fpc.show()
            }
            return header
        }
        
            
        

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if dataProductList.isEmpty && isFound == false {
            return CGSize(width: UIScreen.main.bounds.size.width, height: 0)
        }
        else if dataProductList.count > 0 && isFound == false{
            switch section{
            case 1:
                return CGSize(width: UIScreen.main.bounds.size.width, height: 32)
            default:
              
                return CGSize(width: UIScreen.main.bounds.size.width, height: 0)
                
            }
        }else{
            return CGSize(width: UIScreen.main.bounds.size.width , height: 40)
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if dataProductList.isEmpty && isFound == false{
        }else if dataProductList.count > 0 && isFound == false{
            switch indexPath.section{
            case 0:break;
            default:
                let detail = DetailProductViewController()
                detail.id = dataProductList[indexPath.row].id
                self.navigationController?.pushViewController(detail, animated: true)
            }
        }else{
            let detail = DetailProductViewController()
            detail.id = dataProductList[indexPath.row].id
            self.navigationController?.pushViewController(detail, animated: true)
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        100.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        0.0001
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if dataProductList.isEmpty && isFound == false{
            let width = UIScreen.main.bounds.width - 32
            let height = width + 5
            return CGSize(width: width, height: height)
        }else if dataProductList.count > 0 && isFound == false{
            switch indexPath.section{
            case 0:
                let width = UIScreen.main.bounds.width - 32
                let height = width + 5
                return CGSize(width: width, height: height)
            default:
                let width = (UIScreen.main.bounds.size.width - 16)  / 2
                let height = (307.0 / 181) * width
              return CGSize(width: width, height: height)
            }
        }else{
            let width = (UIScreen.main.bounds.size.width - 16)  / 2
            let height = (307.0 / 181) * width
          return CGSize(width: width, height: height)
        }

    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if dataProductList.isEmpty && isFound == false {
            let cell:ZeroCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ZeroCollectionViewCell", for: indexPath) as! ZeroCollectionViewCell
            return cell
        }else if dataProductList.count > 0 && isFound == false {
            switch indexPath.section{
            case 0:
                let cell:EmptyCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmptyResultProduct", for: indexPath) as! EmptyCollectionViewCell
                return cell
                
                
            default:
                let cell:RecommendationCollectionViewCell =
                collectionView.dequeueReusableCell(withReuseIdentifier: "RecomendationCollectionCell", for: indexPath) as! RecommendationCollectionViewCell
                cell.product = dataProductList[indexPath.row]
                return cell
            }
        } else {
            let cell:RecommendationCollectionViewCell =
            collectionView.dequeueReusableCell(withReuseIdentifier: "RecomendationCollectionCell", for: indexPath) as! RecommendationCollectionViewCell
            cell.product = dataProductList[indexPath.row]
            formatter.locale = Locale(identifier: "id_ID")
            formatter.numberStyle = .currency
            formatter.groupingSeparator = "."
            let priceString =  formatter.string(from: NSNumber(value: dataProductList[indexPath.row].price ?? 0))
            cell.priceLabel.text = priceString
            return cell
        }

        
    }
    
    
}

extension SearchingViewController:UITextFieldDelegate{
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        interactor?.searchProduct(key: textField.text!)
        indicatorView.startAnimating()
        collectionView.isHidden = true
        collectionView.reloadData()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text != nil{
            interactor?.searchProduct(key: textField.text!)
            indicatorView.startAnimating()
            collectionView.isHidden = true
            collectionView.reloadData()
            return true
        }
        return false
    }
}

extension SearchingViewController:SearchingPresenterOutput{
    func getSearchData(data: SearchProductResponse) {
        if data.data?.found == true{
            dataProductList.removeAll()
            isFound = true
            dataProductList += data.data?.product ?? []
            indicatorView.stopAnimating()
            indicatorView.isHidden = true
            collectionView.isHidden = false
            collectionView.reloadData()
            searchTextField.resignFirstResponder()
            self.dismissKeyboard()
        }else if data.data?.found == false{
            if data.data?.product == nil{
                let banner = FloatingNotificationBanner(title:"Ooops, Lagi ada yang error",subtitle: "Silahkan menunggu sesaat lagi",style: .danger)
                banner.show()
            } else {
                dataProductList.removeAll()
                isFound = false
                dataProductList += data.data?.product ?? []
                collectionView.reloadData()
                searchTextField.resignFirstResponder()
                self.dismissKeyboard()
            }
            
        }
    }
    
    func getFailureMessage(message: String) {
        print(message)
    }
    
    
}

class HideFilter:FloatingPanelLayout{
    let position: FloatingPanelPosition = .bottom
    let initialState: FloatingPanelState = .hidden
    var anchors: [FloatingPanelState : FloatingPanelLayoutAnchoring]{
        return [

            .tip: FloatingPanelLayoutAnchor(absoluteInset: 120, edge: .bottom, referenceGuide: .safeArea),
            .hidden: FloatingPanelLayoutAnchor(absoluteInset: -10, edge: .bottom, referenceGuide: .safeArea)
        ]
    }
}

class ShowFilter:FloatingPanelLayout{
    let position: FloatingPanelPosition = .bottom
    let initialState: FloatingPanelState = .tip
    var anchors: [FloatingPanelState : FloatingPanelLayoutAnchoring]{
        return [

            .tip: FloatingPanelLayoutAnchor(absoluteInset: UIScreen.main.bounds.size.height * 0.6, edge: .bottom, referenceGuide: .safeArea),
            .hidden: FloatingPanelLayoutAnchor(absoluteInset: 100, edge: .bottom, referenceGuide: .safeArea)
        ]
    }
}
