//
//  MyTransactionPresenter.swift
//  Shonic
//
//  Created by Gramedia on 24/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

protocol MyTransactionPresenterOutput{
    func getAllOrderList(data:GetOrderListResponse)
    func getAllOrderFilter(data:GetOrderListResponse)
    func error(message:String)
}

class MyTransactionPresenter:MyTransactionInteractorOutput{
    func getAllOrderList(data: GetOrderListResponse) {
        self.viewController?.getAllOrderList(data: data)
    }
    
    func getAllOrderFilter(data: GetOrderListResponse) {
        self.getAllOrderFilter(data: data)
    }
    
    func error(message: String) {
        self.error(message: message)
    }
    
    var viewController:MyTransactionPresenterOutput?
    init(viewController:MyTransactionPresenterOutput) {
        self.viewController = viewController
    }
}
