//
//  MyTransactionWorker.swift
//  Shonic
//
//  Created by Gramedia on 24/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation
import Alamofire

protocol MyTransactionSceneLogic{
    func getAllOrder(callback:@escaping(Result<GetOrderListResponse,AFError>)->Void)
    func getAllOrderFilter(status:String,callback:@escaping(Result<GetOrderListResponse,AFError>)->Void)
}

class MyTransactionWorker{
    var baseUrl = "http://shonic-test.herokuapp.com"
    let service:ApiClient = ApiClient.shared
    let userDefault = UserDefaults.standard
}

extension MyTransactionWorker:MyTransactionSceneLogic{
    func getAllOrderFilter(status:String,callback:@escaping(Result<GetOrderListResponse,AFError>)->Void){
        let authorization = "Bearer \(userDefault.string(forKey: "token")!)"
        let headers:HTTPHeaders = [
            "Authorization":authorization
        ]
        service.requestApi(urlString: "\(baseUrl)/api/v1/order/list?status=\(status)", method: .get, header: headers){(response:Result<GetOrderListResponse, AFError>) in
            callback(response)
        }
    }
    
    func getAllOrder(callback: @escaping (Result<GetOrderListResponse, AFError>) -> Void) {
        let authorization = "Bearer \(userDefault.string(forKey: "token") ?? "")"
        let headers:HTTPHeaders = [
            "Authorization":authorization
        ]
        service.requestApi(urlString: "\(baseUrl)/api/v1/order/list", method: .get, header: headers){(response:Result<GetOrderListResponse, AFError>) in
            callback(response)
        }
    }
    
    
    
}
