//
//  MyTransactionConfigurator.swift
//  Shonic
//
//  Created by Gramedia on 24/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

enum MyTransactionConfigurator{
    static func configure(viewController:MyTransactionViewController){
        let presenter  = MyTransactionPresenter(viewController: viewController)
        let worker = MyTransactionWorker()
        let interactor = MyTransactionInteractor(worker: worker, presenter: presenter)
        viewController.interactor = interactor
    }
}
