//
//  EmptyOrderTableViewCell.swift
//  Shonic
//
//  Created by Gramedia on 26/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class EmptyOrderTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    class func nib()-> UINib{
        UINib(nibName: "EmptyOrderTableViewCell", bundle: nil)
    }
}
