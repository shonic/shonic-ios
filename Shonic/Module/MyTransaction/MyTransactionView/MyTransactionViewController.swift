//
//  MyTransactionViewController.swift
//  Shonic
//
//  Created by Gramedia on 20/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class MyTransactionViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var indicatorLoading: UIActivityIndicatorView!
    var interactor:MyTransactionInteractor?
    var getDataResponse:GetOrderListResponse?
    init() {
        super.init(nibName: "MyTransactionViewController", bundle: nil)
        MyTransactionConfigurator.configure(viewController: self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewWillAppear(_ animated: Bool) {
        self.interactor?.getAllOrderList()
        indicatorLoading.startAnimating()
        tableView.isHidden = true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Pesanan Saya"
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UnPaidTableViewCell.nib(), forCellReuseIdentifier: "UnPaidTableViewCell")
        tableView.register(OnProcessTableViewCell.nib(), forCellReuseIdentifier: "OnProcessTableViewCell")
        tableView.register(EmptyOrderTableViewCell.nib(), forCellReuseIdentifier: "EmptyOrderTableViewCell")
       
        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MyTransactionViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getDataResponse?.data?.count ?? 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if getDataResponse?.data != nil{
            switch getDataResponse?.data![indexPath.row].status{
            case Status(rawValue: "Belum Bayar")!:
                let cell:UnPaidTableViewCell = tableView.dequeueReusableCell(withIdentifier: "UnPaidTableViewCell") as! UnPaidTableViewCell
                cell.deadlineLabel.text = getDataResponse?.data![indexPath.row].deadline ?? ""
                cell.priceLabel.text = "Rp \(getDataResponse?.data![indexPath.row].product?.price ?? 0)"
                cell.priceLabel.text = getDataResponse?.data![indexPath.row].product?.name ?? ""
                cell.qtyLabel.text = "Kuantitas: \(getDataResponse?.data![indexPath.row].product?.qty ?? 0 )"
                cell.totalPriceLabel.text = "Rp \(getDataResponse?.data![indexPath.row].total_price ?? 0)"
                return cell
            default:
                let cell:OnProcessTableViewCell = tableView.dequeueReusableCell(withIdentifier: "OnProcessTableViewCell") as! OnProcessTableViewCell
                return cell
            }
        } else{
            let cell:EmptyOrderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "EmptyOrderTableViewCell") as! EmptyOrderTableViewCell
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return getDataResponse?.data != nil ? 220 : 360
    }
    
}

extension MyTransactionViewController:MyTransactionPresenterOutput{
    func getAllOrderList(data: GetOrderListResponse) {
        if data.status! >= 400{
            
        } else{
            tableView.isHidden = false
            indicatorLoading.startAnimating()
            indicatorLoading.isHidden = true
            getDataResponse = data
            tableView.reloadData()
        }
    }
    
    func getAllOrderFilter(data: GetOrderListResponse) {
        
    }
    
    func error(message: String) {
        
    }
    
    
}
