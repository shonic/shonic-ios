//
//  MyTransactionInteractor.swift
//  Shonic
//
//  Created by Gramedia on 24/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

protocol MyTransactionInteractorInput{
    func getAllOrderList()
    func getAllOrderFilter(status:String)
}

protocol MyTransactionInteractorOutput{
    func getAllOrderList(data:GetOrderListResponse)
    func getAllOrderFilter(data:GetOrderListResponse)
    func error(message:String)
}

class MyTransactionInteractor{
    var worker:MyTransactionWorker?
    var presenter:MyTransactionInteractorOutput?
    init(worker:MyTransactionWorker,presenter:MyTransactionInteractorOutput) {
        self.worker = worker
        self.presenter = presenter
    }
}

extension MyTransactionInteractor:MyTransactionInteractorInput{
    func getAllOrderList() {
        worker?.getAllOrder{callback in
            DispatchQueue.main.async {
                switch callback{
                case .success(let data):
                    self.presenter?.getAllOrderList(data: data)
                case .failure(let error):
                    self.presenter?.error(message: error.localizedDescription)
                }
            }
        }
    }
    
    func getAllOrderFilter(status: String) {
        worker?.getAllOrderFilter(status: status){callback in
            DispatchQueue.main.async {
                switch callback{
                case .success(let data):
                    self.presenter?.getAllOrderFilter(data: data)
                case .failure(let error):
                    self.presenter?.error(message: error.localizedDescription)
                }
            }
        }
    }
    
    
}
