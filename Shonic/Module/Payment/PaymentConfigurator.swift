//
//  PaymentConfigurator.swift
//  Shonic
//
//  Created by Gramedia on 24/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

enum PaymentConfigurator{
    static func configure(viewController:PaymentViewController){
        let presenter = PaymentPresenter(viewController: viewController)
        let worker = PaymentWorker()
        let interactor = PaymentInteractor(worker: worker, presenter: presenter)
        viewController.interactor = interactor
    }
}
