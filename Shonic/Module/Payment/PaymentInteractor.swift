//
//  PaymentInteractor.swift
//  Shonic
//
//  Created by Gramedia on 04/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

protocol PaymentInteractorInput{
    func detailOrder(id:Int)
}
protocol PaymentInteractorOutput{
    func detailResponse(data:GetDetailOrder)
    func message(error:String)
}

class PaymentInteractor{
    var worker:PaymentWorker?
    var presenter:PaymentInteractorOutput?
    init(worker:PaymentWorker,presenter:PaymentInteractorOutput) {
        self.worker = worker
        self.presenter = presenter
    }
}

extension PaymentInteractor:PaymentInteractorInput{
    func detailOrder(id: Int) {
        worker?.getOrderDetail(id: id){callback in
            DispatchQueue.main.async {
                switch callback{
                case .success(let data):
                    self.presenter?.detailResponse(data: data)
                case .failure(let error):
                    self.presenter?.message(error: error.localizedDescription)
                }
            }
        }
    }
}
