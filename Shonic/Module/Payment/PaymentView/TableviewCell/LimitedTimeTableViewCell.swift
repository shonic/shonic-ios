//
//  LimitedTimeTableViewCell.swift
//  Shonic
//
//  Created by Gramedia on 04/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class LimitedTimeTableViewCell: UITableViewCell {

    @IBOutlet weak var deadlineInformationLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    class func nib ()-> UINib{
        UINib(nibName: "LimitedTimeTableViewCell", bundle: nil)
    }
    
}
