//
//  TransferBankTableViewCell.swift
//  Shonic
//
//  Created by Gramedia on 04/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class TransferBankTableViewCell: UITableViewCell {

    @IBOutlet weak var noLicenseLabel: UITextView!
    @IBOutlet weak var licenseNumberView: UIView!
    @IBOutlet weak var totalPaymentView: UIView!
    @IBOutlet weak var totalPrice: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        licenseNumberView.dropDownUI()
        totalPaymentView.dropDownUI()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    class func nib ()-> UINib{
        UINib(nibName: "TransferBankTableViewCell", bundle: nil)
    }
    
}
