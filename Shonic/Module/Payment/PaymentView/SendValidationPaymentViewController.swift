//
//  SendValidationPaymentViewController.swift
//  Shonic
//
//  Created by Gramedia on 04/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class SendValidationPaymentViewController: UIViewController {

    var handler:(()->Void)?
    var navigationTransaction:(()->Void)?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func actionNavigation(_ sender: Any) {
    handler!()
    }
    @IBAction func actionMyTransaction(_ sender: Any) {
        navigationTransaction!()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
