//
//  PaymentViewController.swift
//  Shonic
//
//  Created by Gramedia on 04/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit
import FloatingPanel

class PaymentViewController: UIViewController, FloatingPanelControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    var fpc:FloatingPanelController!
    var interactor:PaymentInteractor?
    var detailData:GetDetailOrder?
    var id:Int?
    
    init() {
        super.init(nibName: "PaymentViewController", bundle: nil)
        PaymentConfigurator.configure(viewController: self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        interactor?.detailOrder(id: id ?? 0)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.register(TransferBankTableViewCell.nib(), forCellReuseIdentifier: "TransferBankTableViewCell")
        tableView.register(InformationPaymentTableViewCell.nib(), forCellReuseIdentifier: "InformationPaymentTableViewCell")
        tableView.register(LimitedTimeTableViewCell.nib(), forCellReuseIdentifier: "LimitedTimeTableViewCell")
//        fpc = FloatingPanelController(delegate: self)
//        fpc.layout =  ShownPickerCourier()
//        var controller = SendValidationPaymentViewController()
//        controller.handler = {
//            self.navigationController?.pushViewController(SuccessTransactionViewController(), animated: true)
//        }
//        controller.navigationTransaction = {
//            self.navigationController?.pushViewController(MyTransactionViewController(), animated: true)
//        }
//        fpc.set(contentViewController: controller)
//        fpc.show()
//        fpc.delegate = self
//        fpc.addPanel(toParent: self)
        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func sendTranscationAction(_ sender: Any) {
        self.navigationController?.pushViewController(UploadPaymentViewController(), animated: true)
    }
    

}

extension PaymentViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row{
        case 0:
            return 80
        case 1:
            return 300
        case 2:
            return 200
        default:
            return 120
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row{
        case 0:
            let cell:LimitedTimeTableViewCell = tableView.dequeueReusableCell(withIdentifier: "LimitedTimeTableViewCell", for: indexPath) as! LimitedTimeTableViewCell
            cell.deadlineInformationLabel.text = detailData?.data?.deadline ?? ""
            return cell
        case 1:
            let cell:TransferBankTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TransferBankTableViewCell", for: indexPath) as! TransferBankTableViewCell
            cell.totalPrice.text = "\(detailData?.data?.total_price ?? 0)"
            cell.noLicenseLabel.text = "\(detailData?.data?.payment ?? "")"
            return cell
        case 2:
            let cell:InformationPaymentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "InformationPaymentTableViewCell", for: indexPath) as! InformationPaymentTableViewCell
            return cell
        default:
            return UITableViewCell()
        }
    }
}

extension PaymentViewController:PaymentPresenterOutput{
    func detailResponse(data: GetDetailOrder) {
        print(data)
        if data.status!  >= 400{
            
        }else{
            detailData = data
            tableView.reloadData()
        }
    }
    
    func message(error: String) {
        
    }
    
    
}
