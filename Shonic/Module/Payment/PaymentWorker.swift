//
//  PaymentWorker.swift
//  Shonic
//
//  Created by Gramedia on 24/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation
import Alamofire

protocol PaymentSceneLogic{
    func getOrderDetail(id:Int,callback:@escaping(Result<GetDetailOrder,AFError>)->Void)
}

class PaymentWorker{
    var baseUrl = "http://shonic-test.herokuapp.com"
    let service:ApiClient = ApiClient.shared
    let userDefault = UserDefaults.standard
}

extension PaymentWorker:PaymentSceneLogic{
    func getOrderDetail(id: Int, callback: @escaping (Result<GetDetailOrder, AFError>) -> Void) {
        let authorization = "Bearer \(userDefault.string(forKey: "token")!)"
        let headers:HTTPHeaders = [
            "Authorization":authorization
        ]
        service.requestApi(urlString: "\(baseUrl)/api/v1/order/detail/\(id)", method: .get, header: headers){(response:Result<GetDetailOrder, AFError>) in
            print(response)
            callback(response)
        }
    }
    
    
}
