//
//  PaymentPresenter.swift
//  Shonic
//
//  Created by Gramedia on 24/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

protocol PaymentPresenterOutput{
    func detailResponse(data:GetDetailOrder)
    func message(error:String)
}

class PaymentPresenter:PaymentInteractorOutput{
    func detailResponse(data: GetDetailOrder) {
        self.viewController?.detailResponse(data: data)
    }
    
    func message(error: String) {
        self.viewController?.message(error: error)
    }
    
    var viewController:PaymentPresenterOutput?
    init(viewController:PaymentPresenterOutput) {
        self.viewController = viewController
    }
}
