//
//  AddressWorker.swift
//  Shonic
//
//  Created by MEKARI on 14/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift
import UIKit
import RxCocoa

protocol AddressSceneLogin{
    func getAllProvinces(completion:@escaping(Result<GetAllProvincesDataResponse,AFError>)->Void)
    func getAllCitiesByProvince(id:Int,completion:@escaping (Result<GetAllCityDataResponse,AFError>)->Void)
    func postAddess(body:AddressBody,completion:@escaping(Result<PostSaveAddressDataResponse,AFError>)->Void)
}

class AddressWorker{
    private let service: ApiClient = ApiClient.shared
    var baseUrl = "http://shonic-test.herokuapp.com"
    var userDefault = UserDefaults.standard
}

extension AddressWorker:AddressSceneLogin{
    
    func getAllProvinces(completion: @escaping (Result<GetAllProvincesDataResponse, AFError>) -> Void) {
        let authorization = "Bearer \(userDefault.string(forKey: "token"))"
        let headers:HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization":authorization
        ]
        service.requestApi(urlString: "\(baseUrl)/api/v1/address/provinces", method: .get, header:headers){(callback:Result<GetAllProvincesDataResponse, AFError>) in
            switch callback{
            case .success(let data):
                completion(.success(data))
            case .failure(let message):
                completion(.failure(message))
            }
        }
    }
    
    func getAllCitiesByProvince(id: Int, completion: @escaping (Result<GetAllCityDataResponse, AFError>) -> Void) {
        let authorization = "Bearer \(userDefault.string(forKey: "token"))"
        let headers:HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization":authorization
        ]
        service.requestApi(urlString: "\(baseUrl)/api/v1/address/province/\(id)/cities", method: .get, header: headers){(callback:Result<GetAllCityDataResponse, AFError>) in
            switch callback{
            case .success(let data):
                completion(.success(data))
            case .failure(let message):
                completion(.failure(message))
            }
        }
    }
    
    func postAddess(body: AddressBody,completion: @escaping (Result<PostSaveAddressDataResponse, AFError>) -> Void) {
        let authorization = "Bearer \(userDefault.string(forKey: "token")!)"
        let headers:HTTPHeaders = [
            "Authorization":authorization
        ]
        service.requestApi(urlString: "\(baseUrl)/api/v1/address/save", method: .post, parameters:body ,header: headers){ (callback:Result<PostSaveAddressDataResponse, AFError>) in
            switch callback{
            case .success(let data):
                completion(.success(data))
            case .failure(let message):
                completion(.failure(message))
            }
        }
    }
}
