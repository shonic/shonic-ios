//
//  AddressCourierViewController.swift
//  Shonic
//
//  Created by Gramedia on 04/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit
import NotificationBannerSwift

class AddressCourierViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var interactor:AddressInteractor?
    var listProvince:[Province] = []
    var listDistrict:[District] = []
    var idProvince:Int = 1
    var name:String = ""
    var phoneNumber:String = ""
    var actionHandler:(()->Void)?
    init(){
        super.init(nibName: "AddressCourierViewController", bundle: nil)
        AddressConfigurator.configure(viewController: self)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        interactor?.getAllProvinces()
        interactor?.getAllcitiesByProvinces(id: idProvince)
    }
    override func viewWillDisappear(_ animated: Bool) {
    
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(ReceiverInformationCollectionViewCell.nib(), forCellWithReuseIdentifier: "ReceiverInformationCollectionViewCell")
        collectionView.register(SenderAddressCollectionViewCell.nib(), forCellWithReuseIdentifier: "SenderAddressCollectionViewCell")
        collectionView.register(AddressHeaderCollectionReusableView.nib(), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "AddressHeaderCollectionReusableView")

        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddressCourierViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        2
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        1
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let width = UIScreen.main.bounds.size.width
        let height = 40.0
        return CGSize(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch  indexPath.section{
        case 0:
            let header:AddressHeaderCollectionReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "AddressHeaderCollectionReusableView", for: indexPath) as! AddressHeaderCollectionReusableView
            header.titleHeader.text = "Informasi Penerima"
            header.actionButton.setTitle("Reset", for: .normal)
            return header
        default:
            let header:AddressHeaderCollectionReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "AddressHeaderCollectionReusableView", for: indexPath) as! AddressHeaderCollectionReusableView
            header.titleHeader.text  = "Alamat Pengiriman"
            header.actionButton.setTitle("Reset", for: .normal)
            return header
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section{
        case 0:
            let cell:ReceiverInformationCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReceiverInformationCollectionViewCell", for: indexPath) as! ReceiverInformationCollectionViewCell
            name = cell.nameLabel.text!
            phoneNumber = cell.phoneLabel.text!
            return cell
        default:
            let cell:SenderAddressCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SenderAddressCollectionViewCell", for: indexPath) as! SenderAddressCollectionViewCell
            cell.listProvince = listProvince
            cell.actionIdCities = { provinceId in
                self.idProvince = provinceId
                self.interactor?.getAllcitiesByProvinces(id: provinceId)
            }
            cell.listDistrict = listDistrict
            cell.saveHandler = { (kecamatan,street,codepos,idP,idC) in
                let body = AddressBody(cityID: idC, detail: street, kecamatan: kecamatan, name: self.name, phone: self.phoneNumber, postalCode: codepos, provinceID: idP)
                if body.name.isEmpty || body.phone.isEmpty{
                    let banner = FloatingNotificationBanner(title:"Empty Field",subtitle: "Please check field empty",style: .danger)
                    banner.show()
                }else{
                    self.interactor?.postAddresss(body: body)
                }
                
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.section{
        case 0:
            let width = UIScreen.main.bounds.size.width
            let height = (105 / 375) * width
            return CGSize(width: width, height: height)
        default:
            let width = UIScreen.main.bounds.size.width
            let height = (273 / 375) * width
            return CGSize(width: width, height: height)
        }
    }
    
    
}

extension AddressCourierViewController:AddressPresenterOutput{
    func getAllProvinces(data: GetAllProvincesDataResponse) {
        listProvince += data.data!
        collectionView.reloadData()
    }
    
    func getAllCitiesByProvinces(data: GetAllCityDataResponse) {
        if listDistrict.isEmpty{
            listDistrict += data.data!
            collectionView.reloadData()
        }
        else{
            listDistrict.removeAll()
            listDistrict += data.data!
            collectionView.reloadData()
        }
        
    }
    
    func postAddress(data: PostSaveAddressDataResponse) {
        actionHandler!()
        self.navigationController?.popViewController(animated: true)
    }
    
    func getError(message: String) {
        
    }
    
    
}
