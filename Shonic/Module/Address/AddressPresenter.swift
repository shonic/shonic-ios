//
//  AddressPresenter.swift
//  Shonic
//
//  Created by MEKARI on 14/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

protocol AddressPresenterOutput:Any{
    func getAllProvinces(data:GetAllProvincesDataResponse)
    func getAllCitiesByProvinces(data:GetAllCityDataResponse)
    func postAddress(data:PostSaveAddressDataResponse)
    func getError(message:String)
}

class AddressPresenter:AddressInteractorOutput{
    var viewcontroller:AddressPresenterOutput?
    init(viewController:AddressPresenterOutput){
        self.viewcontroller = viewController
    }
    func getAllProvinces(data: GetAllProvincesDataResponse) {
        self.viewcontroller?.getAllProvinces(data: data)
    }
    
    func getAllCitiesByProvinces(data: GetAllCityDataResponse) {
        self.viewcontroller?.getAllCitiesByProvinces(data: data)
    }
    
    func postAddress(data: PostSaveAddressDataResponse) {
        self.viewcontroller?.postAddress(data: data)
    }
    
    func getError(message: String) {
        self.viewcontroller?.getError(message: message)
    }
}

