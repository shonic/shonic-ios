//
//  AddressConfigurator.swift
//  Shonic
//
//  Created by MEKARI on 14/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation
import UIKit

enum AddressConfigurator{
    static func configure(viewController:AddressCourierViewController){
        let presenter = AddressPresenter(viewController: viewController)
        let worker = AddressWorker()
        let interactor = AddressInteractor(worker: worker, presenter: presenter)
        viewController.interactor = interactor
    }
}
