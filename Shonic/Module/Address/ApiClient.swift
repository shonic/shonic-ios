//
//  ApiClient.swift
//  Shonic
//
//  Created by MEKARI on 15/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation
import Alamofire



class ApiClient{
    static let shared:ApiClient = ApiClient()
    func requestApi<T: Decodable>(urlString: String,method:HTTPMethod, header:HTTPHeaders?,completion: @escaping (Result<T,AFError>) -> Void) {
        AF.request(urlString,method: method,headers:header).validate(statusCode: 200..<600).responseDecodable(of:T.self,decoder: JSONDecoder()){response in
            completion(response.result)
        }
    }
    
    func requestUpload<T:Decodable>(url:String,body:UploadBody,filePath:NSURL,method:HTTPMethod,header:HTTPHeaders,completion:@escaping (Result<T,AFError>)->Void){
        AF.upload(multipartFormData: { multipartFormdata in
            multipartFormdata.append(filePath as URL, withName: "photo", fileName: "upload\(body.name).jpeg", mimeType: "image/jpeg")
            multipartFormdata.append(body.bank_name.data(using: .utf8) ?? "".data(using: .utf8)!, withName: "bank_name")
            multipartFormdata.append(body.name.data(using: .utf8) ?? "".data(using: .utf8)!, withName: "name")
            multipartFormdata.append(body.bank_number.data(using: .utf8) ?? "".data(using: .utf8)!, withName: "bank_number")
            multipartFormdata.append(body.total_transfer.data, withName:"total_transfer")
        }, to: url,method: .post,headers: header).validate(statusCode: 200..<600).responseDecodable(of:T.self){ response in
            completion(response.result)
        }
    }
    func requestApi<S:Encodable,T: Decodable>(urlString: String,method:HTTPMethod, parameters:S,header:HTTPHeaders?,completion: @escaping (Result<T,AFError>) -> Void) {
            AF.request(urlString,method: method,parameters:parameters,encoder: JSONParameterEncoder.default,headers: header).validate(statusCode: 200..<600).responseDecodable(of:T.self,decoder: JSONDecoder()){response in
                completion(response.result)
            }
    }
    
    func requestApi<S:Encodable,T: Decodable>(urlString: String,method:HTTPMethod, parameters:S,completion: @escaping (Result<T,AFError>) -> Void) {
            AF.request(urlString,method: method,parameters:parameters,encoder: JSONParameterEncoder.default).validate(statusCode: 200..<600).responseDecodable(of:T.self,decoder: JSONDecoder()){response in
                completion(response.result)
            }
    }
    
    private func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    private func encodeAndMerge(_ values: Codable...) throws -> [String: Any] {
        let encoder = JSONEncoder()
        let result: [String: Any] = try values.reduce([String: Any]()) { runningResult, codable in
            let data = try encoder.encode(AnyEncodable(value: codable))
            guard let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
                return runningResult
            }
            return runningResult.merging(json, uniquingKeysWith: { $1 })
        }
        return result
    }
}
    

struct AnyEncodable: Encodable {
    let value: Encodable

    func encode(to encoder: Encoder) throws {
        try value.encode(to: encoder)
    }
}
