//
//  AddressInteractor.swift
//  Shonic
//
//  Created by MEKARI on 14/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

protocol AddressInteractorInput{
    func getAllProvinces()
    func getAllcitiesByProvinces(id:Int)
    func postAddresss(body:AddressBody)
}

protocol AddressInteractorOutput{
    func getAllProvinces(data:GetAllProvincesDataResponse)
    func getAllCitiesByProvinces(data:GetAllCityDataResponse)
    func postAddress(data:PostSaveAddressDataResponse)
    func getError(message:String)
}

class AddressInteractor{
    var worker:AddressWorker?
    var presenter:AddressInteractorOutput?
    init(worker:AddressWorker,presenter:AddressInteractorOutput){
        self.worker = worker
        self.presenter = presenter
    }
}

extension AddressInteractor:AddressInteractorInput{
    func getAllProvinces() {
        self.worker?.getAllProvinces{response in
            switch response{
            case .success(let provinces):
                self.presenter?.getAllProvinces(data: provinces)
            case .failure(let message):
                if message.isSessionTaskError {
                    self.presenter?.getError(message: "Error Network")
                }else{
                    self.presenter?.getError(message: "")
                }
            }
        }
    }
    
    func getAllcitiesByProvinces(id: Int) {
        self.worker?.getAllCitiesByProvince(id: id){response in
            print(response)
            switch response{
            case .success(let cities):
                self.presenter?.getAllCitiesByProvinces(data: cities)
            case .failure(let message):
                if message.isSessionTaskError {
                    self.presenter?.getError(message: "Error Network")
                }else{
                    self.presenter?.getError(message: "")
                }
            }
        }
    }
    
    func postAddresss(body: AddressBody) {
        self.worker?.postAddess(body: body){response in
            switch response{
            case .success(let data):
                self.presenter?.postAddress(data: data)
            case .failure(let message):
                if message.isSessionTaskError {
                    self.presenter?.getError(message: "Error Network")
                }else{
                    self.presenter?.getError(message: "")
                }
            }
        }
    }
    
    
}
