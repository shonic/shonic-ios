//
//  AddNewPasswordResponseModel.swift
//  Shonic
//
//  Created by Andira Yunita on 10/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

struct AddNewPasswordResponseModel: Decodable {
    var token: String?
    var message: String
    var status: Int
    var error: String?
    var path: String?
}

struct AddNewPasswordParameter: Encodable {
    var email: String
    var newPassword: String
    var token: String
    
    init(email: String, newPassword: String, token: String) {
        self.email = email
        self.newPassword = newPassword
        self.token = token
    }
}

struct AddNewPasswordFailedResponseModel: Decodable {
    var message: String
    var status: Int
    var error: String
}
