//
//  ResetPasswordPresenter.swift
//  Shonic
//
//  Created by Andira Yunita on 10/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//
//
//import Foundation
//
//protocol ResetPasswordPresenterProtocol: Any {
//    func presentResetPassword(response: ResetPasswordResponseModel)
//    func presentAlert(message: String)
//}
//
//class ResetPasswordPresenter: ResetPasswordPresenterProtocol {
//    func presentResetPassword(response: ResetPasswordResponseModel) {
//        viewController?.success()
//    }
//
//    func presentAlert(message: String) {
//        viewController?.failure(message: message)
//    }
//
//    var viewController: ResetPasswordViewProtocol?
//}
