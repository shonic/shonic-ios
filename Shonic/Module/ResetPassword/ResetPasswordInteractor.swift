//
//  ResetPasswordInteractor.swift
//  Shonic
//
//  Created by Andira Yunita on 10/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

//import Foundation
//import Alamofire
//
//protocol ResetPasswordInteractorProtocol: AnyObject {
//    func tryToResetPassword(email: String, newPassword: String, token: String)
//}
//
//final class ResetPasswordInteractor {
//    var presenter: ResetPasswordPresenterProtocol?
//    var worker: ResetPasswordWorker?
//}
//
//extension ResetPasswordInteractor: ResetPasswordInteractorProtocol {
//    func tryToResetPassword(email: String, newPassword: String, token: String) {
//        let resetPassword = ResetPasswordParameter(email: email, newPassword: newPassword, token: token)
//        print(resetPassword)
//        
//    }
//}
