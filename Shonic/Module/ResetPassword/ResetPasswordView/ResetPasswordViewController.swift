//
//  ResetPasswordViewController.swift
//  Shonic
//
//  Created by Andira Yunita on 04/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit
import NotificationBannerSwift

class ResetPasswordViewController: UIViewController {
    
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var errorMessage: UILabel!
    @IBOutlet weak var submitButton: RegularButton!
    @IBOutlet weak var backPageLabel: UILabel!
    @IBOutlet weak var bottomLabelConstraint: NSLayoutConstraint!
    
    var accountName: String?
    
    init() {
        super.init(nibName: "ResetPasswordViewController", bundle: nil)
        self.hidesBottomBarWhenPushed = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Ganti Password"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Kembali", style: .plain, target: nil, action: nil)
        self.infoLabel.text = "Masukkan email yang terdaftar pada akun \(accountName ?? "") untuk mengatur ulang password Anda"
        
        formEmail()
        setUpTextField()
        setUpSubmitButton()
        setUpBackPage()
    }

    func showBanner() {
        let banner = FloatingNotificationBanner(title: "Error", subtitle: "Tidak ada akun dengan email tersebut", style: .danger)
        banner.show()
    }
    
    func formEmail(){
        errorMessage.isHidden = true
        errorMessage.text = ""
        emailTextField.text = ""
    }
    
    func setUpSubmitButton(){
        submitButton.layer.cornerRadius = 8
        submitButton.layer.masksToBounds = true
        submitButton.tintColor = UIColor(named: "PrimaryFocus")
        submitButton.isUserInteractionEnabled = false
    }
    
    func invalidEmail(_ value: String) -> String? {
        let reqularExpression = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let predicate = NSPredicate(format: "SELF MATCHES %@", reqularExpression)
        if !predicate.evaluate(with: value) {
            return "Format email tidak valid, contoh: shonic@gmail.com"
        }
        return nil
    }
    
    func validateForm() {
        if errorMessage.isHidden {
            submitButton.isUserInteractionEnabled = true
            submitButton.tintColor = UIColor(named: "PrimaryBlue")
        } else {
            submitButton.isUserInteractionEnabled = false
            submitButton.tintColor = UIColor(named: "PrimaryFocus")
        }
    }
    
    func setUpBackPage() {
        let attributeString = NSMutableAttributedString(string: "Kembali ke halaman Login atau Daftar")
        
        attributeString.addAttributes([
            .font: UIFont.systemFont(ofSize: 15)
        ], range: NSRange(location: 0, length: attributeString.length))
        
        attributeString.addAttributes([
            .foregroundColor: UIColor(named: "PrimaryBlue")!
        ], range: NSRange(location: 19, length: 5))
        
        attributeString.addAttributes([
            .foregroundColor: UIColor(named: "PrimaryBlue")!
        ], range: NSRange(location: 30, length: 6))
        
        backPageLabel.attributedText = attributeString
        backPageLabel.isUserInteractionEnabled = true
        backPageLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapLogin(_:))))
        backPageLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapRegister(_:))))
    }
    
    @objc func didTapLogin(_ recognizer: UITapGestureRecognizer) {
        if recognizer.didTapAttributedTextInLabel(label: backPageLabel, inRange: NSRange(location: 19, length: 5)) {
            navigationController?.pushViewController(LoginViewController(), animated: true)
        }
    }
    
    @objc func didTapRegister(_ recognizer: UITapGestureRecognizer) {
        if recognizer.didTapAttributedTextInLabel(label: backPageLabel, inRange: NSRange(location: 30, length: 6)) {
            navigationController?.pushViewController(RegisterViewController(), animated: true)
        }
    }
    
    func setUpTextField() {
        emailTextField.layer.borderColor = UIColor.systemGray5.cgColor
        emailTextField.layer.borderWidth = 1
        emailTextField.layer.cornerRadius = 8
        emailTextField.layer.masksToBounds = true
        emailTextField.addTarget(self, action: #selector(touchTextField), for: .editingDidBegin)
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyBoard)))
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func touchTextField() {
        emailTextField.layer.borderColor = UIColor.systemBlue.cgColor
    }
    
    @objc private func hideKeyBoard(){
        self.view.endEditing(true)
    }
    
    @objc private func keyboardWillHide(){
        bottomLabelConstraint.constant = 16
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as?
            NSValue {
            let keyboardHeight = keyboardFrame.cgRectValue.height
            let bottomSpace = self.view.frame.height - (submitButton.frame.origin.y + submitButton.frame.height)
            bottomLabelConstraint.constant = keyboardHeight - bottomSpace + 36
        }
    }
    
    @IBAction func emailOnChange(_ sender: Any) {
        if let email = emailTextField.text {
            if let error = invalidEmail(email) {
                errorMessage.text = error
                errorMessage.isHidden = false
                emailTextField.layer.borderColor = UIColor.systemRed.cgColor
            } else {
                errorMessage.isHidden = true
                emailTextField.layer.borderColor = UIColor.systemGray5.cgColor
            }
        }
        validateForm()
    }
    
    @IBAction func submitTapped(_ sender: Any) {
        let vc = VerificationEmailViewController()
        vc.email = emailTextField.text!
        vc.status = "forgotpassword"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
