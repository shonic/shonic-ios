//
//  ResetPasswordWorker.swift
//  Shonic
//
//  Created by Andira Yunita on 10/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

//import Foundation
//import Combine
//
//protocol ResetPasswordAuthLogic {
//    func makeAuth(user: ResetPasswordParameter, completion: @escaping(Result<ResetPasswordResponseModel, ErrorMessage>) -> Void)
//}
//
//final class ResetPasswordWorker {
//    private let service: AuthService
//    private var bag = Set<AnyCancellable>()
//    
//    init(service: AuthService){
//        self.service = service
//    }
//    
//    enum ResetPasswordWorker: Error{
//        case authFailed(String)
//        case unauthorized
//    }
//}
//
//extension ResetPasswordWorker: ResetPasswordAuthLogic {
//    func makeAuth(user: ResetPasswordParameter, completion: @escaping (Result<ResetPasswordResponseModel, ErrorMessage>) -> Void) {
//        service.resetPassword(email: user.email, newPassword: user.newPassword, token: user.token) { (callback) in
//            print(callback)
//            switch callback {
//            case .success(let result):
//                completion(.success(result))
//                break
//                
//            case .failure(let error):
//                completion(.failure(error))
//                break
//            }
//        }
//    }
//}
