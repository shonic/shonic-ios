//
//  DetailProductPresenter.swift
//  Shonic
//
//  Created by Gramedia on 04/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

protocol DetailProductPresenterOutput:AnyObject{
    func getDetailProduct(data:GetDataDetailProductResponse)
    func getSimiliarProduct(data:GetAllLastestProductResponse)
    func postCheckoutCost(data:CheckoutResponse)
    func getFailedProduct(message:String)
}
class DetailProductPresenter:DetailProductInteractorOutput{
    func postCheckoutCost(data: CheckoutResponse) {
        self.viewController?.postCheckoutCost(data: data)
    }
    
    func getSimiliarProduct(data: GetAllLastestProductResponse) {
        self.viewController?.getSimiliarProduct(data: data)
    }
    
    var viewController:DetailProductPresenterOutput?
    init(viewController:DetailProductPresenterOutput) {
        self.viewController = viewController
    }
    func getDetailProduct(data: GetDataDetailProductResponse) {
        self.viewController?.getDetailProduct(data: data)
    }
    
    func getFailedDetailProduct(message: String) {
        self.viewController?.getFailedProduct(message: message)
    }
    
    
}
