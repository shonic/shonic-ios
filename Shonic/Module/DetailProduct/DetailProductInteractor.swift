//
//  DetailProductInteractor.swift
//  Shonic
//
//  Created by Gramedia on 03/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

protocol DetailProductInteractorInput:AnyObject{
    func getDetailProduct(idProduct:String)
    func getSimiliarProduct(idProduct:String)
    func postCheckoutCost(body:CheckoutCostBody)
}
protocol DetailProductInteractorOutput:Any{
    func getDetailProduct(data:GetDataDetailProductResponse)
    func getSimiliarProduct(data:GetAllLastestProductResponse)
    func postCheckoutCost(data:CheckoutResponse)
    func getFailedDetailProduct(message:String)
}
class DetailProductInteractor{
    init(worker:DetailProductWorker,presenter:DetailProductInteractorOutput) {
        self.presenter = presenter
        self.worker = worker
    }
    var presenter:DetailProductInteractorOutput?
    var worker:DetailProductWorker?
   
}

extension DetailProductInteractor:DetailProductInteractorInput{
    func getSimiliarProduct(idProduct: String) {
        self.worker?.getSimiliarProduct(by: idProduct){(completion) in
            DispatchQueue.main.async {
                switch completion{
                case .success(let data):
                    self.presenter?.getSimiliarProduct(data: data)
                case .failure(let message):
                    if message.isSessionTaskError{
                        self.presenter?.getFailedDetailProduct(message: "Network Error")
                    } else {
                        self.presenter?.getFailedDetailProduct(message: "Uknown Error")
                    }
                }
            }
        }
    }
    
    func getDetailProduct(idProduct: String) {
        self.worker?.getDetailProduct(by: idProduct){(completion) in
            
            DispatchQueue.main.async {
                switch completion{
                case .success( let data):
                    self.presenter?.getDetailProduct(data: data)
                case .failure(let message):
                    if message.isSessionTaskError{
                        self.presenter?.getFailedDetailProduct(message: "Network Error")
                    } else {
                        self.presenter?.getFailedDetailProduct(message: "Uknown Error")
                    }
                }
                }
            }
            
        }
    func postCheckoutCost(body: CheckoutCostBody) {
        self.worker?.postCheckoutCost(by: body){ (completion) in
            DispatchQueue.main.async {
                switch completion{
                case .success( let data):
                    self.presenter?.postCheckoutCost(data: data)
                case .failure(let message):
                    if message.isSessionTaskError{
                        self.presenter?.getFailedDetailProduct(message: "Network Error")
                    } else {
                        self.presenter?.getFailedDetailProduct(message: "Uknown Error")
                    }
                }
            }
        }
        
    }
}
