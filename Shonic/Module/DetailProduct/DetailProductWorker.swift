//
//  DetailProductWorker.swift
//  Shonic
//
//  Created by Gramedia on 04/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

protocol DetailProductScene:AnyObject{
    func getDetailProduct(by id:String,completion:@escaping(Result<GetDataDetailProductResponse,AFError>)->Void)
    func getSimiliarProduct(by id:String,completion:@escaping(Result<GetAllLastestProductResponse,AFError>)->Void)
    func postCheckoutCost(by body:CheckoutCostBody,completion:@escaping(Result<CheckoutResponse,AFError>)->Void)
}

class DetailProductWorker{
    var baseUrl = "http://shonic-test.herokuapp.com"
    let service:ApiClient = ApiClient.shared
}

extension DetailProductWorker:DetailProductScene{
    func getSimiliarProduct(by id: String, completion: @escaping (Result<GetAllLastestProductResponse, AFError>) -> Void) {
        service.requestApi(urlString: "\(baseUrl)/api/v1/product/simmilar/\(id)", method: .get, header: nil) { (callback:Result<GetAllLastestProductResponse,AFError>) in
            switch callback{
            case .success(let data):
                completion(.success(data))
            case .failure(let message):
                completion(.failure(message))
            }
        }
    }
    
    //func getReviewList(by id:String,){}
    
    func getDetailProduct(by id: String, completion: @escaping (Result<GetDataDetailProductResponse, AFError>) -> Void) {
        service.requestApi(urlString: "\(baseUrl)/api/v1/product/getById/\(id)", method: .get, header: nil){(callback:Result<GetDataDetailProductResponse, AFError>) in
            switch callback{
            case .success(let data):
                completion(.success(data))
            case .failure(let message):
                completion(.failure(message))
            }
        }
    }
    func postCheckoutCost(by body: CheckoutCostBody, completion: @escaping (Result<CheckoutResponse, AFError>) -> Void) {
        let userDefault = UserDefaults.standard
        let authorization = "Bearer \(userDefault.string(forKey: "token")!)"
        let headers:HTTPHeaders = [
            "Authorization":authorization
        ]
        service.requestApi(urlString: "\(baseUrl)/api/v1/checkout/cost", method: .post, parameters: body, header: headers){(callback:Result<CheckoutResponse, AFError>) in
            switch callback{
            case .success(let data):
                print(data.status)
                completion(.success(data))
            case .failure(let message):
                completion(.failure(message))
            }
        }
    }
}
