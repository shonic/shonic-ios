//
//  HeaderReviewCollectionReusableView.swift
//  Shonic
//
//  Created by Gramedia on 03/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class HeaderReviewCollectionReusableView: UICollectionReusableView {

    
    override func awakeFromNib() {
        super.awakeFromNib()

        // Initialization code
    }
    
    class func nib ()-> UINib{
        UINib(nibName: "HeaderReviewCollectionReusableView", bundle: nil)
    }
    
}

