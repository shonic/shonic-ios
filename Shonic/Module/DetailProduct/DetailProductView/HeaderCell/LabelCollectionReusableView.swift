//
//  LabelCollectionReusableView.swift
//  Shonic
//
//  Created by Gramedia on 03/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class LabelCollectionReusableView: UICollectionReusableView {

    @IBOutlet weak var labelHeader: UILabel!
    var title:String?{
        didSet{
            if let titleProduct = title{
                labelHeader.text = titleProduct
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    class func nib()-> UINib{
        UINib(nibName: "LabelCollectionReusableView", bundle: nil)
    }
    
}
