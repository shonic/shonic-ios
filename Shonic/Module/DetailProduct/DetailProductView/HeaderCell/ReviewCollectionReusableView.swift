//
//  ReviewCollectionReusableView.swift
//  Shonic
//
//  Created by Gramedia on 03/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class ReviewCollectionReusableView: UICollectionReusableView {

    var actionHandler:(()->Void)?
    
    @IBOutlet weak var reviewTotalLabel: UILabel!
    @IBOutlet weak var ratingTotalLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    @IBAction func navigationAction(_ sender: Any) {
        actionHandler!()
    }

    
    
    
    class func nib ()-> UINib{
        UINib(nibName: "ReviewCollectionReusableView", bundle: nil)
    }
    
}
