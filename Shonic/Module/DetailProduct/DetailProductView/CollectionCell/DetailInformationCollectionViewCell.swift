//
//  DetailInformationCollectionViewCell.swift
//  Shonic
//
//  Created by Gramedia on 03/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class DetailInformationCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var informationOriginalView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var namelabel: UILabel!
    @IBOutlet weak var reviewLabel: UILabel!
    @IBOutlet weak var totalratingLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        informationOriginalView.layer.cornerRadius = 6
        // Initialization code
    }

    class func nib ()-> UINib{
        UINib(nibName: "DetailInformationCollectionViewCell", bundle: nil)
    }
}
