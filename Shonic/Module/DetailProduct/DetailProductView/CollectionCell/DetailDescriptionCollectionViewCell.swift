//
//  DetailDescriptionCollectionViewCell.swift
//  Shonic
//
//  Created by Gramedia on 03/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class DetailDescriptionCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var qtyLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var merkLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    class func nib ()-> UINib{
        UINib(nibName: "DetailDescriptionCollectionViewCell", bundle: nil)
    }

}
