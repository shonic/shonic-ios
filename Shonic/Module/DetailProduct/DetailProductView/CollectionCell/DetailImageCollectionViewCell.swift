//
//  DetailImageCollectionViewCell.swift
//  Shonic
//
//  Created by Gramedia on 03/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit
import SDWebImage

class DetailImageCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageDetail: UIImageView!
    var image:String?{
        didSet{
            if let image = image {
                self.setImage(image: image)
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setImage(image:String){
        let url = URL(string: image)
        self.imageDetail.sd_setImage(with: url)
    }
    
    class func nib()->UINib{
        UINib(nibName: "DetailImageCollectionViewCell", bundle: nil)
    }

}
