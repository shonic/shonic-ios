//
//  DetailProductViewController.swift
//  Shonic
//
//  Created by Gramedia on 03/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit
import FloatingPanel

enum DetailSectionType{
    case imageProduct
    case informationProduct
    case informationDescription
    case ratingAndReview
    case similiarProduct
}

protocol ReviewDelegate:Any{
    func navigate()
}
class DetailProductViewController: UIViewController,FloatingPanelControllerDelegate,ReviewDelegate{
    
    func navigate() {
        self.navigationController?.pushViewController(ReviewViewController(), animated: true)
    }
    init() {
        super.init(nibName: "DetailProductViewController", bundle: nil)
        DetailProductConfigurator.configurator(viewController: self)
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    var interactor:DetailProductInteractor?
    
    var detailData:GetDataDetailProductResponse?
    var dataSimiliar:GetAllLastestProductResponse?
    var imageData:String?
    let detailType:[DetailSectionType] = [
        .imageProduct,.informationProduct,.informationDescription,.ratingAndReview,.similiarProduct]
    var qty:Int = 1
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var qtyLabel: UILabel!
    var id:String?
    var fpc:FloatingPanelController!
    let formatter = NumberFormatter()
    
    func floatingPanel(_ fpc: FloatingPanelController, layoutFor size: CGSize) -> FloatingPanelLayout {
        return MyFloatingPanelLayout()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.interactor?.getDetailProduct(idProduct: id!)
        self.interactor?.getSimiliarProduct(idProduct: id!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Detail Produk"
        self.navigationItem.backButtonTitle = "Kembali"
        self.navigationItem.rightBarButtonItems = [
            UIBarButtonItem(image: UIImage(systemName: "magnifyingglass"), style: .plain, target: self, action: #selector(moveSearch)),
            UIBarButtonItem(image: UIImage(systemName: "bag"), style: .plain, target: self, action: #selector(moveSearch))
        ]
        qtyLabel.text = "\(qty)"
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(DetailImageCollectionViewCell.nib(), forCellWithReuseIdentifier: "DetailImageCollectionViewCell")
        collectionView.register(DetailInformationCollectionViewCell.nib(), forCellWithReuseIdentifier: "DetailInformationCollectionViewCell")
        collectionView.register(DetailDescriptionCollectionViewCell.nib(), forCellWithReuseIdentifier: "DetailDescriptionCollectionViewCell")
        collectionView.register(DetailReviewCollectionViewCell.nib(), forCellWithReuseIdentifier: "DetailReviewCollectionViewCell")
        collectionView.register(RecommendationCollectionViewCell.nib(), forCellWithReuseIdentifier: "RecomendationCollectionCell")
        collectionView.register(ZeroCollectionViewCell.nib(), forCellWithReuseIdentifier: "ZeroCollectionViewCell")
        collectionView.register(ReviewCollectionReusableView.nib(), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "ReviewCollectionReusableView")
        collectionView.register(LabelCollectionReusableView.nib(), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "LabelCollectionReusableView")
        collectionView.register(EmptyCollectionReusableView.nib(), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "EmptySearchHeader")
    }
    
    @objc
    func navigateReviewList(){
        self.navigationController?.pushViewController(ReviewViewController(), animated: true)
    }
    @IBAction func plusProduct(_ sender: Any) {
        qty += 1
        qtyLabel.text = "\(qty)"
    }
    @IBAction func minusProduct(_ sender: Any) {
        if qty < 1{
            qty = 0
            qtyLabel.text = "\(qty)"
        }else {
            qty -= 1
            qtyLabel.text = "\(qty)"
        }
    }
    @objc
    func moveSearch(){
        
    }

    @IBAction func navigateToCheckout(_ sender: Any) {
        let products = [ProductsBodyId(id: detailData?.data.id ?? "", qty: qty)]
        let body = CheckoutCostBody(products: products)
        let vc = CheckoutViewController()
        vc.bodyCheckout = body
        vc.price = detailData?.data.price ?? 0
        if UserDefaults.standard.string(forKey: "token") != nil{
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let alert = UIAlertController(title: "Belum Login", message: "Kamu butuh login terlebih dahulu", preferredStyle: .alert)
            let closeActions = UIAlertAction(title: "Tutup", style: .cancel)
            let handler = UIAlertAction(title: "Masuk", style: .destructive, handler: { _ in
                self.navigationController?.pushViewController(LoginViewController(), animated: true)
            })
            alert.addAction(closeActions)
            alert.addAction(handler)
            self.present(alert, animated: true)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DetailProductViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        switch section{
        case 2:
            let width = UIScreen.main.bounds.size.width
            let height = 32.0
          return CGSize(width: width, height: height)
        case 3:
            let width = UIScreen.main.bounds.size.width
            let height = 72.0
          return CGSize(width: width, height: height)
        case 4:
            let width = UIScreen.main.bounds.size.width
            let height = 40.0
          return CGSize(width: width, height: height)
        default:
            return CGSize(width: 0, height: 0)
        }
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch indexPath.section{
        case 2: let header:LabelCollectionReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LabelCollectionReusableView", for: indexPath) as! LabelCollectionReusableView
            header.title = "Rincian Produk"
            return header
        case 3:
            let header:ReviewCollectionReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ReviewCollectionReusableView", for: indexPath) as! ReviewCollectionReusableView
            header.ratingTotalLabel.text = "\(detailData?.data.rating ?? 0.0)"
            header.reviewTotalLabel.text = "\(detailData?.data.review ?? 0) ulasan"
            header.actionHandler = {
                self.navigationController?.pushViewController(ReviewViewController(), animated: true)
            }
            return header
        case 4:
            let header:LabelCollectionReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LabelCollectionReusableView", for: indexPath) as! LabelCollectionReusableView
                header.title = "Mungkin anda suka"
                return header
        default:
            let header:EmptyCollectionReusableView =
            collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "EmptySearchHeader", for: indexPath) as! EmptyCollectionReusableView
            return header
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.section{
        case 0:
            let width = UIScreen.main.bounds.size.width
            let height = width
            return CGSize(width: width, height: height)
        case 1:
            let width = UIScreen.main.bounds.size.width
            let height = (198 / 375) * width
            return CGSize(width: width, height: height)
        case 2:
            let width = UIScreen.main.bounds.size.width
            let height = (597 / 375) * width - 20
            return CGSize(width: width, height: height)
        case 3:
            let width = UIScreen.main.bounds.size.width - 24
            let height = (100 / 375) * width
            return CGSize(width: width, height: height)
        case 4:
            let width = (UIScreen.main.bounds.size.width - 16)  / 2
            let height = (307.0 / 181) * width
          return CGSize(width: width, height: height)
        default:
            let width = UIScreen.main.bounds.size.width
            let height = width
            return CGSize(width: width, height: height)
        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section{
        case 3: return 4
        case 4: return dataSimiliar?.data?.count ?? 0
        default:return 1
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section{
        case 0:
            let cell:DetailImageCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DetailImageCollectionViewCell", for: indexPath) as! DetailImageCollectionViewCell
            cell.image = detailData?.data.image
           
            return cell
        case 1:
            let cell:DetailInformationCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DetailInformationCollectionViewCell", for: indexPath) as! DetailInformationCollectionViewCell
            cell.namelabel.text = detailData?.data.name
            formatter.locale = Locale(identifier: "id_ID")
            formatter.numberStyle = .currency
            formatter.groupingSeparator = "."
            let priceString =  formatter.string(from: NSNumber(value: detailData?.data.price ?? 0))
            cell.priceLabel.text = "\(priceString ?? "")"
            cell.reviewLabel.text = "\(detailData?.data.review ?? 0)"
            cell.totalratingLabel.text = "\(detailData?.data.rating ?? 0.0)"
            
            return cell
        case 2:
            let cell:DetailDescriptionCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DetailDescriptionCollectionViewCell", for: indexPath) as! DetailDescriptionCollectionViewCell
            cell.qtyLabel.text = "\(detailData?.data.qty ?? 0)"
            cell.merkLabel.text = "\(detailData?.data.brand.name ?? "")"
            cell.categoryLabel.text = detailData?.data.category?.name ?? ""
            cell.descriptionTextView.text = detailData?.data.description ?? ""
            return cell
        case 3 :
            let cell:DetailReviewCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DetailReviewCollectionViewCell", for: indexPath) as! DetailReviewCollectionViewCell
            return cell
        case 4:
            let cell:RecommendationCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecomendationCollectionCell", for: indexPath) as! RecommendationCollectionViewCell
            let priceString =  formatter.string(from: NSNumber(value: dataSimiliar?.data?[indexPath.row].price ?? 0))
            cell.product = dataSimiliar?.data?[indexPath.row]
            cell.priceLabel.text = priceString
            return cell

        default:
            let cell:ZeroCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ZeroCollectionViewCell", for: indexPath) as! ZeroCollectionViewCell
            return cell
            
        }
        
    }
    
    
}

extension DetailProductViewController:DetailProductPresenterOutput{
    func postCheckoutCost(data: CheckoutResponse) {
        let vc = CheckoutViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getSimiliarProduct(data: GetAllLastestProductResponse) {
        self.dataSimiliar = data
        self.collectionView.reloadData()
    }
    
    func getDetailProduct(data: GetDataDetailProductResponse) {
        self.detailData = data
        self.collectionView.reloadData()

    }
    
    func getFailedProduct(message: String) {
        print(message)
    }
    
    
}
class MyFloatingPanelLayout:FloatingPanelLayout{
    let position: FloatingPanelPosition = .bottom
    let initialState: FloatingPanelState = .tip
    var anchors: [FloatingPanelState : FloatingPanelLayoutAnchoring]{
        return [
            .tip: FloatingPanelLayoutAnchor(absoluteInset: 120, edge: .bottom, referenceGuide: .safeArea)
        ]
    }
}

