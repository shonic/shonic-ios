//
//  DetailProductConfigurator.swift
//  Shonic
//
//  Created by Gramedia on 04/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

enum DetailProductConfigurator{
    static func configurator(viewController:DetailProductViewController){
        let presenter = DetailProductPresenter(viewController: viewController)
        let worker = DetailProductWorker()
        let interactor = DetailProductInteractor(worker: worker, presenter: presenter)
        viewController.interactor = interactor
    }
}
