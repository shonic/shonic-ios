//
//  CheckoutResponse.swift
//  Shonic
//
//  Created by MEKARI on 13/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

struct CheckoutResponse:Codable {
    let status: Int?
    let message: String?
    let data: [CourierData]?
    let error: String?
}

// MARK: - Datum
struct CourierData :Codable{
    let code, name: String?
    let costs: [CostCourier]?
}

// MARK: - DatumCost
struct CostCourier :Codable{
    let service, description: String?
    let cost: [CostDetailCourier]?
}

// MARK: - CostCost
struct CostDetailCourier :Codable{
    let value: Int?
    let etd, note: String?
}
