//
//  GetAllAccountBank.swift
//  Shonic
//
//  Created by MEKARI on 17/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

struct GetAllAccountBankDataResponse:Codable {
    let status: Int?
    let message: String?
    let data: [Bank]?
    let error: String?
}

// MARK: - Datum
struct Bank:Codable{
    let id: Int?
    let name, number, bank, branch: String?
    let image: String?
}
