//
//  GetPaymentResponse.swift
//  Shonic
//
//  Created by Gramedia on 24/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

struct GetPaymentResponse: Codable {
    let status: Int?
    let message: String?
    let data: DataStruct?
    let error: String?
}
