//
//  AddressBody.swift
//  Shonic
//
//  Created by MEKARI on 13/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

struct AddressBody:Encodable{
    var city_id: Int
    var detail, kecamatan, name, phone: String
    var postal_code: String
    var province_id: Int
    init(cityID:Int,detail:String,kecamatan:String,name:String,phone:String,postalCode:String,provinceID:Int){
        self.city_id = cityID
        self.detail = detail
        self.kecamatan = kecamatan
        self.name = name
        self.phone = phone
        self.postal_code = postalCode
        self.province_id = provinceID
    }
}
