//
//  CheckoutCreateBody.swift
//  Shonic
//
//  Created by MEKARI on 17/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

class CheckoutCreateBody:Encodable{
    let courier:CourierCheckout?
    let note:String?
    let payment_account_id:Int?
    let products:ProductCheckout?
    init(courier:CourierCheckout,note:String,paymentAccountId:Int,products:ProductCheckout){
        self.courier = courier
        self.note = note
        self.payment_account_id = paymentAccountId
        self.products = products
    }
}

class CourierCheckout:Encodable{
    let code:String?
    let service:String?
    init(code:String,service:String){
        self.code = code
        self.service = service
    }
}

class ProductCheckout:Encodable{
    let productId:String?
    let qty:Int?
    init(productId:String,qty:Int){
        self.productId = productId
        self.qty = qty
    }
}
