//
//  GetSimiliarProductResponse.swift
//  Shonic
//
//  Created by MEKARI on 07/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

struct GetSimiliarProductResponse{
    let status:Int
    let message:String
    let data:[Product]
    let error:String?
}
