//
//  ProductsBody.swift
//  Shonic
//
//  Created by MEKARI on 13/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

struct CheckoutBody:Codable{
    let products:[ProductBody]
}

struct ProductBody:Codable{
    let productId:String
    let qty:Int
}
