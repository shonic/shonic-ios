//
//  DeleteResponse.swift
//  Shonic
//
//  Created by Gramedia on 20/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

struct DeleteResponse: Codable {
    let status: Int?
    let message: String?
    let data: DataUser?
    let error: String?
}

// MARK: - DataClass
struct DataUser: Codable {
    let id, username, password, fullname: String?
    let addresses: Addresses?
    let roles: [Province]?
    let provider, rating: JSONNull?
}

// MARK: - Addresses
struct Addresses: Codable {
    let id: Int?
    let name, phone: String?
    let city: City?
    let province: Province?
    let kecamatan, postalCode, detailAddress: String?

    enum CodingKeys: String, CodingKey {
        case id, name, phone, city, province, kecamatan, postalCode
        case detailAddress = "detail_address"
    }
}
