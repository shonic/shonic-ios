//
//  GetAllProvincesDataResponse.swift
//  Shonic
//
//  Created by MEKARI on 13/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

struct GetAllProvincesDataResponse:Codable {
    let status: Int?
    let message: String?
    let data: [Province]?
    let error: String?
}

