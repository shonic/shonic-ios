//
//  GetCheckoutDataResponse.swift
//  Shonic
//
//  Created by MEKARI on 17/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

struct GetCheckoutDataResponse:Codable{
    let status: Int?
    let message: String?
    let data: ResponseDataCheckout?
    let error: String?
}

// MARK: - DataClass
struct ResponseDataCheckout :Codable{
    let order_id: String?
}
