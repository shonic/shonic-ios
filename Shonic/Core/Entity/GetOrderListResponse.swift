//
//  GetOrderListResponse.swift
//  Shonic
//
//  Created by Gramedia on 24/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let deleteResponse = try? newJSONDecoder().decode(DeleteResponse.self, from: jsonData)

import Foundation

// MARK: - DeleteResponse
struct GetOrderListResponse: Codable {
    let status: Int?
    let message: String?
    let data: [OrderData]?
    let error: String?
}

// MARK: - Datum
struct OrderData: Codable {
    let id: Int?
    let status: Status?
    let deadline: String?
    let product: ProductOrder?
    let subtotal, cost: Int?
    let reviewed: Bool?
    let order_number: String?
    let status_detail: StatusDetail?
    let total_price: Int?

}

// MARK: - Product
struct ProductOrder: Codable {
    let id, name: String?
    let price, discount, qty, weight: Int?
    let price_after_disc: Int?
    let image_url: String?
}

enum Status: String, Codable {
    case belumBayar = "Belum Bayar"
}

enum StatusDetail: String, Codable {
    case bayarPesananSebelum = "Bayar pesanan sebelum"
}


