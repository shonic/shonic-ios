//
//  CheckoutCostBodu.swift
//  Shonic
//
//  Created by MEKARI on 17/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

class CheckoutCostBody:Encodable{
    let products:[ProductsBodyId]
    init(products:[ProductsBodyId]){
        self.products = products
    }
}

class ProductsBodyId:Encodable{
    let productId:String
    let qty:Int
    init(id:String,qty:Int){
        self.productId = id
        self.qty = qty
    }
}
