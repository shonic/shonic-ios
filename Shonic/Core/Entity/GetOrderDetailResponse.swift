//
//  GetOrderDetailResponse.swift
//  Shonic
//
//  Created by Gramedia on 25/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

struct GetDetailOrder: Codable {
    let status: Int?
    let message: String?
    let data: ProductOrderDetail?
    let error: String?
}

// MARK: - DataClass
struct ProductOrderDetail: Codable {
    let id: Int?
    let date, status: String?
    let deadline: String?
    let product: ProductBookOrderDetail?
    let subtotal, cost: Int?
    let reviewed: Bool?
    let note: String?
    let address: Address?
    let payment, order_number, status_detail: String?
    let total_price: Int?
    let courier_code: String?
    let contact_admin: Bool?
}

struct ProductBookOrderDetail:Codable{
    let id, name: String?
    let price, discount, qty, weight: Int?
    let priceAfterDisc: Int?
    let imageURL: String?
    enum CodingKeys: String, CodingKey {
            case id, name, price, discount, qty, weight
            case priceAfterDisc = "price_after_disc"
            case imageURL = "image_url"
        }
}

