//
//  PostSaveAddressDataResponse.swift
//  Shonic
//
//  Created by MEKARI on 13/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

struct PostSaveAddressDataResponse:Codable {
    let status: Int?
    let message: String?
    let data: Address?
    let error: String?
}

// MARK: - DataClass
struct Address:Codable {
    let id: Int?
    let name, phone: String?
    let city: City?
    let province: Province?
    let kecamatan, detail, postalCode: String?
}

// MARK: - City
struct City :Codable{
    let id: Int?
    let name: String?
    let province: Province?
}

