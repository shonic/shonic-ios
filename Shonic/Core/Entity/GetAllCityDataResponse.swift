//
//  GetAllCityDataResponse.swift
//  Shonic
//
//  Created by MEKARI on 13/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

struct GetAllCityDataResponse:Codable {
    let status: Int?
    let message: String?
    let data: [District]?
    let error: String?
}

// MARK: - Datum
struct District :Codable{
    let id: Int?
    let name: String?
    let province: Province?
}

// MARK: - Province
struct Province:Codable{
    let id: Int?
    let name: String?
}
