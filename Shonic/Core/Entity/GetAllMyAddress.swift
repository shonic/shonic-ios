//
//  GetAllMyAddress.swift
//  Shonic
//
//  Created by Gramedia on 19/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

struct GetMyAddress: Codable {
    let status: Int?
    let message: String?
    let data: DataStruct?
    let error: String?
}

// MARK: - DataClass
struct DataStruct: Codable {
    let id: Int?
    let name, phone: String?
    let city: City?
    let province: Province?
    let kecamatan, postalCode, detailAddress: String?

    enum CodingKeys: String, CodingKey {
        case id, name, phone, city, province, kecamatan, postalCode
        case detailAddress = "detail_address"
    }
}
