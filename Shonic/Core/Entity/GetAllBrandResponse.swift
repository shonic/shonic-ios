//
//  GetAllBrandResponse.swift
//  Shonic
//
//  Created by MEKARI on 07/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

struct GetAllBrandResponse:Codable{
    let status:Int
    let message:String
    let data:[Brand]
    let error:String?
}
