//
//  UploadBody.swift
//  Shonic
//
//  Created by Gramedia on 24/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

struct UploadBody{
    var fileUrl:String
    var bank_name:String
    var total_transfer:Int
    var bank_number:String
    var name:String
}
