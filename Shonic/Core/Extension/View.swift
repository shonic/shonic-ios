//
//  View.swift
//  team-d-iOS
//
//  Created by MEKARI on 30/05/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    func shadow(alpha: CGFloat = 0.3, opacity: Float = 1,  shadowOffset: CGSize = CGSize(width: 2.5, height: 2.5), shadowRadius: CGFloat = 1, cornerRadius: CGFloat = 12){
        self.layer.shadowColor = CGColor.init(red: 0, green: 0, blue: 0, alpha: alpha)
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = shadowOffset
        self.layer.shadowRadius = shadowRadius
        self.layer.cornerRadius = cornerRadius
    }
    
    func dropDownUI(cornerRadius:CGFloat = 4,borderWidth:CGFloat = 1, borderColor:UIColor = .black){
        self.layer.cornerRadius = cornerRadius
        self.layer.borderColor = borderColor.withAlphaComponent(0.6).cgColor
        self.layer.borderWidth = borderWidth
    }
    
    @discardableResult
        func addLineDashedStroke(pattern: [NSNumber]?, radius: CGFloat, color: CGColor) -> CALayer {
            let borderLayer = CAShapeLayer()

            borderLayer.strokeColor = color
            borderLayer.lineDashPattern = pattern
            borderLayer.frame = bounds
            borderLayer.fillColor = nil
            borderLayer.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: radius, height: radius)).cgPath
            layer.addSublayer(borderLayer)
            return borderLayer
        }
}
