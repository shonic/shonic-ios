//
//  UIViewController.swift
//  Shonic
//
//  Created by MEKARI on 02/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation
import UIKit

enum NavigationbarType {
    case title
    case hidden
    case titleWithBarItem
}

extension UIViewController{
//    func navigationBar(type:NavigationbarType){
//        switch type {
//        case .title:
//            titleNavigation(title: "Cho")
//        case .hidden:
//            <#code#>
//        case .titleWithBarItem:
//            <#code#>
//        }
//    }
    
    func titleNavigation(title:String){
        self.title = title
    }
    
    func hideKeyboard(){
        let tap =  UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard(){
            self.view.endEditing(true)
        }
}
