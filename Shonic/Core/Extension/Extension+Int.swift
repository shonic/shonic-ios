//
//  Extension+Int.swift
//  Shonic
//
//  Created by Gramedia on 05/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation


protocol JsonEncoding where Self: Encodable { }

extension JsonEncoding {
    func encode(using encoder: JSONEncoder) throws -> Data {
        try encoder.encode(self)
    }
}

extension Dictionary where Value == JsonEncoding {
    func encode(using encoder: JSONEncoder) throws -> [Key: String] {
        try compactMapValues {
            try String(data: $0.encode(using: encoder), encoding: .utf8)
        }
    }
}
extension String: JsonEncoding { }
extension Int: JsonEncoding { }

extension Int{
    var data: Data {
            var int = self
            return Data(bytes: &int, count: MemoryLayout<UInt8>.size)
    }
}
