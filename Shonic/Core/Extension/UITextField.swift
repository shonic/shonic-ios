//
//  UITextField.swift
//  Shonic
//
//  Created by Dzaki Izza on 05/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit
extension UITextField {
fileprivate func setPasswordToggleImage(_ button: UIButton) {
    if(isSecureTextEntry){
        button.setImage(UIImage(systemName: "eye.slash.fill"), for: .normal)
    }else{
        button.setImage(UIImage(systemName: "eye.fill"), for: .normal)
    }
}

func enablePasswordToggle(){
    var config = UIButton.Configuration.plain()
    config.buttonSize = .small
    config.imagePadding = 5

    let button = UIButton(configuration: config)
    button.addTarget(self, action: #selector(self.togglePasswordView), for: .touchUpInside)
    button.tintColor = .systemGray2
    setPasswordToggleImage(button)
    self.rightView = button
    self.rightViewMode = .always
}
@IBAction func togglePasswordView(_ sender: Any) {
    self.isSecureTextEntry = !self.isSecureTextEntry
    setPasswordToggleImage(sender as! UIButton)
}
}
