//
//  RegisterResponseModel.swift
//  Shonic
//
//  Created by Dzaki Izza on 08/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation

struct CompletingRegisterResponseModel:Decodable{
    var token:String?
    var status:Int
    var message:String
    var error:String?
    var path:String?
    var data: CompletingRegisterResponseData?
}

struct CompletingRegisterResponseData: Decodable {
    var fullName: String?
    var id: String?
    var email : String?
}

struct CompletingRegisterParameter:Encodable{
    var email:String
    var password:String
    var fullName: String
    
    init(email:String,password:String, fullName: String) {
        self.email = email
        self.password = password
        self.fullName = fullName
    }
}

struct CompletingRegisterFailedResponseModel:Decodable{
    var message:String
    var status:Int
    var timeStamp:String
    var error:String
}
