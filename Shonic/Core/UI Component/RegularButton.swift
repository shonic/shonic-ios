//
//  RegularButton.swift
//  Shonic
//
//  Created by Dzaki Izza on 05/06/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import UIKit

class RegularButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    func setup(){
        self.layer.cornerRadius = 8
        self.layer.masksToBounds = true
        self.tintColor = UIColor(named: "PrimaryFocus")
        self.isUserInteractionEnabled = false
    }
}
