//
//  MockupClient.swift
//  ShonicTests
//
//  Created by Gramedia on 18/07/22.
//  Copyright © 2022 Shonic. All rights reserved.
//

import Foundation
import Shonic
import XCTest
class MockupClient{
    func register()->[String:Any]{
        let json = """
{"token":"eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJjaG9uZHJvc2F0cmlvd2lib3dvQGdtYWlsLmNvbSIsImV4cCI6MTY1ODA4MzcyMCwiaWF0IjoxNjU4MDc3NzIwfQ.fS1CjyoH9w5Sq6vuTM5eb0t_VOmBtPXmf3Awu3w4d9g","message":"Register Berhasil","status":200,"data":{"fullName":"Chondro Satrio","id":"e4abd4c1-cb61-4860-ad94-7efd4404f28f","email":"chondrosatriowibowo@gmail.com"}
}
"""
       let data = convertToDictionary(text: json)
        return data!
        
    }
    
    func login()->[String:Any]{
        let json =
        """
{
    "token": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJjaG9uZHJvc2F0cmlvd2lib3dvQGdtYWlsLmNvbSIsImV4cCI6MTY1ODA4ODAzNSwiaWF0IjoxNjU4MDgyMDM1fQ.yTiL2KOCZrc5ei04Pf88jwwunkT7HOAy7TxQJGt5G08",
    "status": 200,
    "message": "success"
}
"""
        let data = convertToDictionary(text: json)
         return data!
    }
    
    func getAllProduct()->[String:Any]{
        let json = """
{
    "status": 200,
    "message": "success",
    "data": [
        {
            "id": "47c06f32-fac6-4ed3-a62f-57c0033fe10b",
            "createdAt": "2022-07-17T15:14:42.56",
            "image": "https://res.cloudinary.com/shonic/image/upload/w_300,h_300/v1657992953/handphone_20_gzv6iu.png",
            "name": "ADVANCE Tab 8 Belajar Elite 3",
            "price": 1350000,
            "qty": 0,
            "discount": 75,
            "rating": 4.93,
            "brand": {
                "id": 6,
                "name": "ADVANCE"
            },
            "category": {
                "categoryId": 16,
                "name": "smartphone",
                "category_parent": {
                    "id": 15,
                    "name": "handphone"
                }
            },
            "review": 30,
            "after_discount": 337500
        },
        {
            "id": "1205f2db-551c-4205-b42f-5244a81090e9",
            "createdAt": "2022-07-17T15:14:42.556",
            "image": "https://res.cloudinary.com/shonic/image/upload/w_300,h_300/v1657992921/handphone_19_hzyd0z.png",
            "name": "APPLE Watch Series 3",
            "price": 3299000,
            "qty": 8,
            "discount": 0,
            "rating": 3.01,
            "brand": {
                "id": 20,
                "name": "APPLE"
            },
            "category": {
                "categoryId": 18,
                "name": "smartwatch",
                "category_parent": {
                    "id": 15,
                    "name": "handphone"
                }
            },
            "review": 44,
            "after_discount": 3299000
        },
        {
            "id": "43252ddc-bb22-4c5a-bdc5-ad180123b1b4",
            "createdAt": "2022-07-17T15:14:42.551",
            "image": "https://res.cloudinary.com/shonic/image/upload/w_300,h_300/v1657992882/handphone_18_xq071s.png",
            "name": "REALME Watch 2 Pro Smartwatch",
            "price": 849000,
            "qty": 17,
            "discount": 5,
            "rating": 4.64,
            "brand": {
                "id": 24,
                "name": "REALME"
            },
            "category": {
                "categoryId": 18,
                "name": "smartwatch",
                "category_parent": {
                    "id": 15,
                    "name": "handphone"
                }
            },
            "review": 70,
            "after_discount": 806550
        },
        {
            "id": "6bf37c05-6c17-4572-a772-e47697455886",
            "createdAt": "2022-07-17T15:14:42.544",
            "image": "https://res.cloudinary.com/shonic/image/upload/w_300,h_300/v1657992857/handphone_17_v9fhrj.png",
            "name": "APPLE iPad Mini 6 2021 8.3\"",
            "price": 6700000,
            "qty": 21,
            "discount": 0,
            "rating": 3.82,
            "brand": {
                "id": 20,
                "name": "APPLE"
            },
            "category": {
                "categoryId": 16,
                "name": "smartphone",
                "category_parent": {
                    "id": 15,
                    "name": "handphone"
                }
            },
            "review": 78,
            "after_discount": 6700000
        },
        {
            "id": "aa2b8356-8a6d-44da-a61b-287d294bb66d",
            "createdAt": "2022-07-17T15:14:42.539",
            "image": "https://res.cloudinary.com/shonic/image/upload/w_300,h_300/v1657992810/handphone_16_t5qx6k.png",
            "name": "APPLE iPad Pro M1 Chip",
            "price": 11245000,
            "qty": 20,
            "discount": 30,
            "rating": 3.75,
            "brand": {
                "id": 20,
                "name": "APPLE"
            },
            "category": {
                "categoryId": 17,
                "name": "tablet",
                "category_parent": {
                    "id": 15,
                    "name": "handphone"
                }
            },
            "review": 70,
            "after_discount": 7871500
        },
        {
            "id": "03b96d87-0f3e-4f77-a608-fd9f18cfd171",
            "createdAt": "2022-07-17T15:14:42.535",
            "image": "https://res.cloudinary.com/shonic/image/upload/w_300,h_300/v1657992766/handphone_15_axu6dc.png",
            "name": "XIAOMI Redmi 9A",
            "price": 1299000,
            "qty": 14,
            "discount": 0,
            "rating": 3.43,
            "brand": {
                "id": 1,
                "name": "XIAOMI"
            },
            "category": {
                "categoryId": 16,
                "name": "smartphone",
                "category_parent": {
                    "id": 15,
                    "name": "handphone"
                }
            },
            "review": 68,
            "after_discount": 1299000
        },
        {
            "id": "32085a40-7f1e-49a1-8924-d1a17f49c7b6",
            "createdAt": "2022-07-17T15:14:42.53",
            "image": "https://res.cloudinary.com/shonic/image/upload/w_300,h_300/v1657992725/handphone_14_qzr4po.png",
            "name": "XIAOMI Redmi Note 11 Pro (8/128GB)",
            "price": 3799000,
            "qty": 31,
            "discount": 0,
            "rating": 3.77,
            "brand": {
                "id": 1,
                "name": "XIAOMI"
            },
            "category": {
                "categoryId": 16,
                "name": "smartphone",
                "category_parent": {
                    "id": 15,
                    "name": "handphone"
                }
            },
            "review": 71,
            "after_discount": 3799000
        },
        {
            "id": "17cbcb7a-bcc5-4f60-a8cb-40cb6267e041",
            "createdAt": "2022-07-17T15:14:42.526",
            "image": "https://res.cloudinary.com/shonic/image/upload/w_300,h_300/v1657992687/handphone_13_fwef7j.png",
            "name": "SAMSUNG Galaxy S22 Ultra 5G 12/512GB",
            "price": 20999000,
            "qty": 18,
            "discount": 25,
            "rating": 3.26,
            "brand": {
                "id": 3,
                "name": "SAMSUNG"
            },
            "category": {
                "categoryId": 16,
                "name": "smartphone",
                "category_parent": {
                    "id": 15,
                    "name": "handphone"
                }
            },
            "review": 79,
            "after_discount": 15749250
        },
        {
            "id": "3b513247-48bb-4d02-af7e-345bbc6ec6b6",
            "createdAt": "2022-07-17T15:14:42.521",
            "image": "https://res.cloudinary.com/shonic/image/upload/w_300,h_300/v1657992645/handphone_12_gggmlu.png",
            "name": "OPPO F9 6/128 gb",
            "price": 1169000,
            "qty": 20,
            "discount": 0,
            "rating": 3.56,
            "brand": {
                "id": 23,
                "name": "OPPO"
            },
            "category": {
                "categoryId": 16,
                "name": "smartphone",
                "category_parent": {
                    "id": 15,
                    "name": "handphone"
                }
            },
            "review": 53,
            "after_discount": 1169000
        },
        {
            "id": "498f2e94-1adc-454e-94c5-3ecc53c98555",
            "createdAt": "2022-07-17T15:14:42.515",
            "image": "https://res.cloudinary.com/shonic/image/upload/w_300,h_300/v1657993031/handphone_11_kizacb.png",
            "name": "OPPO A16 Smartphone 3GB/32GB",
            "price": 1599000,
            "qty": 21,
            "discount": 0,
            "rating": 4.0,
            "brand": {
                "id": 23,
                "name": "OPPO"
            },
            "category": {
                "categoryId": 16,
                "name": "smartphone",
                "category_parent": {
                    "id": 15,
                    "name": "handphone"
                }
            },
            "review": 51,
            "after_discount": 1599000
        }
    ],
    "error": null
}
"""
        let data = convertToDictionary(text: json)
         return data!
    }
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                let dataDict  = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
                print(dataDict)
                return dataDict
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}
